//=======================================================================================
//
//	メッシュベース
//
// 作成者：fujimura koichi	作成月：2013.11
//=======================================================================================
#include "mesh.h"

//---------------------------------------------------------------------------------------
// メッシュ初期化
//---------------------------------------------------------------------------------------
HRESULT InitMesh( LPDIRECT3DDEVICE9 pDevice, tMesh* mesh, TCHAR* xfile )
{
    LPD3DXBUFFER pD3DXMtrlBuffer = NULL;

	// Ｘファイルからメッシュデータを読み込む
	if (FAILED(D3DXLoadMeshFromX(xfile, D3DXMESH_SYSTEMMEM, pDevice, NULL,	// ここでＸファイルを指定
		&pD3DXMtrlBuffer, NULL, &mesh->dwNumMaterials, &mesh->pD3DMesh)))
	{
		MessageBox(NULL, _T("Xファイルの読み込みに失敗しました"), NULL, MB_OK);
		return E_FAIL;
	}

	// 法線が無い場合は強制的に追加
	if ((mesh->pD3DMesh->GetFVF() & D3DFVF_NORMAL) == 0)
	{
		LPD3DXMESH pMesh = mesh->pD3DMesh;
		pMesh->CloneMeshFVF(pMesh->GetOptions(), pMesh->GetFVF() | D3DFVF_NORMAL, pDevice, &mesh->pD3DMesh);
		SAFE_RELEASE(pMesh);
		D3DXComputeNormals(mesh->pD3DMesh, NULL);
	}

	D3DXMATERIAL* pD3DMaterials = (D3DXMATERIAL*)pD3DXMtrlBuffer->GetBufferPointer();
	mesh->pD3DMaterials = new D3DMATERIAL9[mesh->dwNumMaterials];
	mesh->pD3DTextures  = new LPDIRECT3DTEXTURE9[mesh->dwNumMaterials];
	for (DWORD i = 0; i < mesh->dwNumMaterials; i++)
	{ 
		mesh->pD3DMaterials[i] = pD3DMaterials[i].MatD3D;
		mesh->pD3DMaterials[i].Ambient = mesh->pD3DMaterials[i].Diffuse;
		mesh->pD3DTextures[i] = NULL;
		if (pD3DMaterials[i].pTextureFilename && pD3DMaterials[i].pTextureFilename[0])
		{
			// テクスチャファイルを読み込む
			if (FAILED(D3DXCreateTextureFromFileA(pDevice, pD3DMaterials[i].pTextureFilename, &mesh->pD3DTextures[i]))) {
				MessageBox(NULL, _T("テクスチャの読み込みに失敗しました"), NULL, MB_OK);
			}
		}
	}
	pD3DXMtrlBuffer->Release();

	return S_OK;
}

//---------------------------------------------------------------------------------------
// メッシュ解放
//---------------------------------------------------------------------------------------
void UninitMesh( tMesh* mesh )
{
	// テクスチャオブジェクトを解放
	for (DWORD i = 0; i < mesh->dwNumMaterials; i++)
	{
		SAFE_RELEASE(mesh->pD3DTextures[i]);
	}
	delete mesh->pD3DTextures;
	mesh->pD3DTextures = NULL;

	delete mesh->pD3DMaterials;

	SAFE_RELEASE(mesh->pD3DMesh);			// メッシュオブジェクトを解放
}

//---------------------------------------------------------------------------------------
// メッシュ更新
//---------------------------------------------------------------------------------------
void UpdateMesh( tMesh* mesh )
{
	// メッシュに対して共通して行われることだけ書く
}

//---------------------------------------------------------------------------------------
// メッシュ描画
//---------------------------------------------------------------------------------------
void DrawMesh( LPDIRECT3DDEVICE9 pDevice, tMesh* mesh )
{
	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);

	// 描画
	for( DWORD i = 0; i < mesh->dwNumMaterials; i++ )
	{
		pDevice->SetMaterial(&mesh->pD3DMaterials[i]);
		pDevice->SetTexture(0, mesh->pD3DTextures[i]);	// テクスチャを設定
		mesh->pD3DMesh->DrawSubset(i);							// 描画を実行
	}
}

//=======================================================================================
//	End of Files
//=======================================================================================
