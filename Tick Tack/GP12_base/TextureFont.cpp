//=============================================================================
//
// フォントテクスチャ生成処理 [textureFont.cpp]
//
//=============================================================================
#include "textureFont.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define FONTTEXTURE_MAXNUM	(20)

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
UINT GetFontCharCode( const TCHAR* CHAR );

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tFontTextureCache	g_FontTexture[FONTTEXTURE_MAXNUM];
//LPDIRECT3DDEVICE9	g_FontDrawDevice = NULL;

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT InitTextureFont(LPDIRECT3DDEVICE9 pDevice){
	for(int i=0; i<FONTTEXTURE_MAXNUM; i++){
		g_FontTexture[i].code = 0;
		g_FontTexture[i].pD3DTexture = NULL;
	}

	// main.hはあんまり汚したくないので、ここで保持
	// 描画系だけ分割する？
	//g_FontDrawDevice = pDevice;

	return S_OK;
}

//=============================================================================
// フォント作成処理
//=============================================================================
int CreateFontTexture(const TCHAR* CHAR){
	// 生成する文字のコード取得
	UINT code = GetFontCharCode( CHAR );

	for(int id=0; id<FONTTEXTURE_MAXNUM; id++){
		if( g_FontTexture[id].code == code ){
			// 既に生成済みである文字の場合は、処理中断
			return id;
		}

		if( g_FontTexture[id].pD3DTexture != NULL ){
			// 未使用のIDを探索
			continue;
		}

		HFONT hFont, oldFont;
		HDC hdc = GetDC(NULL);
		int fontsize = 260;
		int fontwidth = 400;

		// 生成するフォント情報の設定
		LOGFONT	lf = {
			fontsize,			// 縦幅
			0,					// 横幅(0指定で縦幅と同値)
			0,					// 角度
			0,					// 上と同じ値(使わない値)
			fontwidth,			// 太さ(0〜1000)	400:通常　700:太字
			0,					// 斜体
			0,					// 下線
			0,					// 打ち消し線
			SHIFTJIS_CHARSET,			// 文字セット
			OUT_TT_ONLY_PRECIS,			// 出力精度
			CLIP_DEFAULT_PRECIS,		// クリッピング精度
			PROOF_QUALITY,				// 出力品質
			DEFAULT_PITCH | FF_MODERN,	// ピッチとフォントファミリ
			_T("ＭＳ Ｐ明朝")			// 書体
		};

		// 生成用のフォントハンドルの取得
		hFont = CreateFontIndirect( &lf );
		if( hFont == NULL ){
			return -1;
		}

		// 生成用のフォントハンドルを設定
		oldFont = (HFONT)SelectObject( hdc, hFont );

		// フォントビットマップ情報の取得
		GLYPHMETRICS gm;	// ビットマップ情報取得用行列
		CONST MAT2 mat = {{0,1},{0,0},{0,0},{0,1}};	// 変換用行列(変換無し設定)
		// 必要なメモリブロックサイズの取得
		DWORD size = GetGlyphOutline(hdc, code, GGO_GRAY4_BITMAP, &gm,
										0, NULL,	// 0, NULL　設定で必要なメモリブロックサイズが戻り値になる
										&mat);
		// 必要量のメモリを確保
		// size　＝　横幅X（4バイト単位で確保）のサイズが縦幅Y分だけ並んでいる
		BYTE* pMono = new BYTE[size];
		// 改めてフォントビットマップ情報を取得する
		GetGlyphOutline(hdc, code, GGO_GRAY4_BITMAP, &gm, size, pMono, &mat);

		// デバイスコンテキストとフォントハンドルの開放処理
		SelectObject( hdc, oldFont );
		ReleaseDC( NULL, hdc );


	// テクスチャ作成
		int grad = 16;	// GGO_GRAY4_BITMAP（17階調ビットマップ）の階調最大値
		int texWidth = (gm.gmBlackBoxX + 3) / 4 * 4;	// 4バイト単位で確保されたメモリブロックと対応させる( gmBlackBoxXは余白を省いた値 )
		int texHeight = gm.gmBlackBoxY;
		//g_FontDrawDevice->CreateTexture( texWidth, texHeight, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &g_FontTexture[id].pD3DTexture, NULL );

		// テクスチャをロックして書き込み準備
		D3DLOCKED_RECT lockedRect;
		g_FontTexture[id].pD3DTexture->LockRect( 0, &lockedRect, NULL, 0);
		DWORD *pTexBuf = (DWORD*)lockedRect.pBits;	// テクスチャメモリへのポインタを取得

		for (int y = 0; y < texHeight; y++ ) {
				for (int x = 0; x < texWidth; x++ ) {
						DWORD alpha = pMono[y * texWidth + x] * 255 / grad;	// 階調数から0〜255のアルファ値へ変換
						pTexBuf[y * texWidth + x] = (alpha << 24) | 0x00ffffff;// ARGBへ変換し、書き込む
				}
		}

		// アンロック
		g_FontTexture[id].pD3DTexture->UnlockRect( 0 );
		// フォントビットマップ情報用のメモリを解放
		delete[] pMono;

		g_FontTexture[id].code = code;
		return id;
	}

	return -1;

}

//=============================================================================
// 終了処理
//=============================================================================
void UninitTextureFont(void){
	for(int i=0; i<FONTTEXTURE_MAXNUM; i++){
		SAFE_RELEASE( g_FontTexture[i].pD3DTexture );
	}

	//SAFE_RELEASE( g_FontDrawDevice );
}

//=============================================================================
// フォントテクスチャポインタ取得処理
//=============================================================================
LPDIRECT3DTEXTURE9 GetFontTexturePoint(const TCHAR* CHAR){
	int id = CreateFontTexture( CHAR );

	if( id == -1 ){
		// テクスチャ作成に失敗した場合
		return NULL;
	}

	return g_FontTexture[id].pD3DTexture;
}

//=============================================================================
// １文字の文字列から、文字コードの取得処理
//=============================================================================
UINT GetFontCharCode( const TCHAR* CHAR ){
	UINT code;

#if _UNICODE
	// unicodeの場合、文字コードはそのままUINT変換となる
	code = (UINT)*TEXT;
#else
	// マルチバイト文字の場合、
	if(IsDBCSLeadByte(*CHAR))
		// 2バイト文字のコードは、[1バイト目のコード][2バイト目のコード]
		// ※char型の1バイトコード二つを結合し、2バイトコードとして扱えるようにする
		code = (BYTE)CHAR[0]<<8 | (BYTE)CHAR[1];
	else
		// 1バイト文字のコードは1バイト目のUINT変換
		code = CHAR[0];
#endif

	return code;
}
