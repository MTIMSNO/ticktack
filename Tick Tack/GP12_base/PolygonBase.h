#ifndef ___POLYGON_BASE_H___
#define ___POLYGON_BASE_H___

#include "main.h"
#include "polygonAnim.h"

//*****************************************************************************
// 列挙型定義
//*****************************************************************************
enum{
	POLYGON_HANDLE_CENTER,
	POLYGON_HANDLE_MIDDLETOP,
	POLYGON_HANDLE_MAX
};

// ポリゴンを作成するために必要なデータ

// OBJデータ
typedef struct _tObjData
{
	LPDIRECT3DTEXTURE9		pD3DTexture;		// テクスチャへのポリゴン
	LPDIRECT3DVERTEXBUFFER9	pD3DVtxBuff;		// 頂点バッファインターフェースへのポインタ

	D3DXVECTOR3				pos;				// 移動量
	D3DXVECTOR2				size;				// ポリゴンのサイズ
	D3DXVECTOR2				sizeMax;			// 画像自体のサイズ
	D3DXVECTOR3				rot;				// 回転量
	float					fRadius;			// 中心から頂点への長さ
	float					fBaseAngle;			// 中心から頂点への角度
	float					fScale;				// 拡大率
	int						nAnimCount;			// アニメーションカウント
	int						nAnimPattern;		// アニメーションパターンナンバー
	int						nAnimNo;			// アニメナンバー
	bool					bAnim;				// アニメーション中かどうか
	bool					bExist;				// 表示するかどうか
	float					fAlpha;

	void*					pAnim;				// アニメーションデータ配列のポインタ(tAnimKeyData*型に変換すること)

	int						nType;				// タイプ・属性
	int						nStates;			// ステータス
	int						nHandle;			// 操作起点
}tObjData;

// 作成するための関数
// 引数(1:デバイス 2:OBJデータ 3:テクスチャ 4:初期位置)
HRESULT MakePolygon( LPDIRECT3DDEVICE9 pDevice, tObjData* obj, TCHAR* tex = NULL );
void SetVertexPolygon( tObjData* obj );										// ポリゴンの位置設定
//HRESULT ChangeTexture( LPDIRECT3DDEVICE9 pDevice, tObjData* obj, TCHAR* );	// テクスチャを変更する
HRESULT ChangeTexture( tObjData* obj, TCHAR* );	// テクスチャを変更する
void UninitPolygonAndTexture( tObjData* obj );								// 後片付け
void SetTexturePolygon( tObjData* obj );									// テクスチャ設定
void SetAnimationNo( tObjData* obj, int no );								// アニメナンバーを変更
bool IsPolygonAnimation( tObjData* obj, int no = -1 );						// アニメ中かどうか

// テクスチャアニメーションの進行
void Process_PolygonTexAnimation( tObjData* obj );
// 表示するテクスチャー範囲設定
void SetPolygonTextureRect( tObjData& obj, RECT rect );
// 各頂点の色設定
void SetPolygonDiffuse( tObjData* obj, D3DXCOLOR& color );
// 操作起点設定
void SetPolygonHanddle( tObjData* obj, int type );
// スクロール2D
void SetTextureScroll( tObjData* obj , float speed,int state);

// ビルボード用
HRESULT MakePolygon3D( LPDIRECT3DDEVICE9 pDevice, tObjData* obj, TCHAR* tex = NULL );
void SetVertexPolygon3D( tObjData* obj );									// ポリゴンの位置設定
void SetTexturePolygon3D( tObjData* obj );	// テクスチャ設定
// スクロール3D
void SetTextureScroll3D( tObjData* obj , float speed);


#endif