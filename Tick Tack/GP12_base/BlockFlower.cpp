#include "BlockFlower.h"
#include "Player.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PreStartBlockFlower(void);
void UpdateBlockFlower(void);
TCHAR* GetBlockFlower_TableFileName(void);

//*****************************************************************************
// 構造体定義
//*****************************************************************************

typedef struct _tBlockFlower
{
	// 各ブロックに対するカウントなど
	int			nCurrentI;
	int			nSaveI;

	// 各操作に対するフラグ
	bool		bFlower_Two_Wither;
	bool		bFlower_Two_Delete;

	bool		bFlower_Three_Wither;
	bool		bFlower_Three_Delete;

	bool		bFlower_Four_Wither;
	bool		bFlower_Four_Delete;

	bool		bFlower_Five_Wither;
	bool		bFlower_Five_Delete;

	bool		bFlower_Six_Wither;
	bool		bFlower_Six_Delete;

	bool		bFlower_Seven_Wither;
	bool		bFlower_Seven_Delete;
}tBlockFlower;

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*				g_Stage_BlockFlower[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];
tBlockFlower			g_tBlockFlower[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];

//=============================================================================
// 初期化処理
// ※起動時に一回だけやって欲しい処理
//=============================================================================
void InitBlockFlower(void){
	// ここで「StageBase.cpp」内で用意した、各マスのポリゴンへのポインタを取得する
	// ※二番目の引数は、「StageBase.h」『列挙型定義』に追加した、『STAGE_LAYER_〜〜』を入れる
	// 　このサンプルでは地形より奥に表示しているので、『STAGE_LAYER_BACK001』を設定してます
	LinkStage_CellArray( g_Stage_BlockFlower, STAGE_LAYER_FLOWER );

	// ここで「StageBase.cpp」から、
	// 「BlockFlower.cpp」内にある「PreStartBlockFlower」「UpdateBlockFlower」「GetBlockFlower_TableFileName」関数にアクセスできるようにする
	// ※関数名を変更した場合は、ここの引数名も変更する
	// 　レイヤー設定は、上と同じものを使う
	LinkStage_PreStartFunc( PreStartBlockFlower, STAGE_LAYER_FLOWER );
	LinkStage_UpdateFunc( UpdateBlockFlower, STAGE_LAYER_FLOWER );
	LinkStage_TableFunc( GetBlockFlower_TableFileName, STAGE_LAYER_FLOWER );

	/*
	※ 初期化処理をPreStartに移動
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
	{
		g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI = 0; 
		g_tBlockFlower[i][j].bFlower_Two_Wither = g_tBlockFlower[i][j].bFlower_Two_Delete =
		g_tBlockFlower[i][j].bFlower_Three_Wither = g_tBlockFlower[i][j].bFlower_Three_Delete =
		g_tBlockFlower[i][j].bFlower_Four_Wither = g_tBlockFlower[i][j].bFlower_Four_Delete =
		g_tBlockFlower[i][j].bFlower_Five_Wither = g_tBlockFlower[i][j].bFlower_Five_Delete = 
		g_tBlockFlower[i][j].bFlower_Six_Wither = g_tBlockFlower[i][j].bFlower_Six_Delete = 
		g_tBlockFlower[i][j].bFlower_Seven_Wither = g_tBlockFlower[i][j].bFlower_Seven_Delete = false;
	}
	*/
}

//=============================================================================
// ステージ開始時の処理
// ※各ステージの開始時に毎回やって欲しい処理
//=============================================================================
void PreStartBlockFlower(void){
	// ステージが開始される毎に、花の成長状態を初期化する
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
	{
		g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI = 0; 
		g_tBlockFlower[i][j].bFlower_Two_Wither = g_tBlockFlower[i][j].bFlower_Two_Delete =
		g_tBlockFlower[i][j].bFlower_Three_Wither = g_tBlockFlower[i][j].bFlower_Three_Delete =
		g_tBlockFlower[i][j].bFlower_Four_Wither = g_tBlockFlower[i][j].bFlower_Four_Delete =
		g_tBlockFlower[i][j].bFlower_Five_Wither = g_tBlockFlower[i][j].bFlower_Five_Delete = 
		g_tBlockFlower[i][j].bFlower_Six_Wither = g_tBlockFlower[i][j].bFlower_Six_Delete = 
		g_tBlockFlower[i][j].bFlower_Seven_Wither = g_tBlockFlower[i][j].bFlower_Seven_Delete = false;
	}
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockFlower(void)
{
	if ( !(GetPlayerNumber() == NAME_TACK) )	return;

	static int nFrameCount = 0;
	nFrameCount++;

	if ( nFrameCount % 45 == 0 )
	{
		for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
		{
			for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
			{
				// あたり判定作成
				CreateCollision(&g_Stage_BlockFlower[i][j]->collision,g_Stage_BlockFlower[i][j]->obj.pos,COLLISION_TYPE_CIRCLE,(float)STAGE_CELL_HEIGHT/25);
				switch ( g_Stage_BlockFlower[i][j]->type )
				{
				case FLOWER_TWO:
					if (!(g_tBlockFlower[i][j].bFlower_Two_Wither) && !(g_tBlockFlower[i][j].bFlower_Two_Delete))
					{
						g_tBlockFlower[i][j].nCurrentI++;
						if (g_tBlockFlower[i][j].nCurrentI >= 2)
						{
							g_tBlockFlower[i][j].bFlower_Two_Wither = true;
							g_tBlockFlower[i][j].nSaveI = g_tBlockFlower[i][j].nCurrentI;
						}
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_ONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
					}
					else if (g_tBlockFlower[i][j].bFlower_Two_Wither && !(g_tBlockFlower[i][j].bFlower_Two_Delete))
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_WITHER;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Two_Delete = true;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI;
						}
					}
					else if (g_tBlockFlower[i][j].bFlower_Two_Wither && g_tBlockFlower[i][j].bFlower_Two_Delete)
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_NONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Two_Wither = false;
							g_tBlockFlower[i][j].bFlower_Two_Delete = false;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI = 0;
						}
					}
					break;

				case FLOWER_THREE:
					if (!(g_tBlockFlower[i][j].bFlower_Three_Wither) && !(g_tBlockFlower[i][j].bFlower_Three_Delete))
					{
						g_tBlockFlower[i][j].nCurrentI++;
						if (g_tBlockFlower[i][j].nCurrentI >= 3)
						{
							g_tBlockFlower[i][j].bFlower_Three_Wither = true;
							g_tBlockFlower[i][j].nSaveI = g_tBlockFlower[i][j].nCurrentI;
						}

						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_ONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
					}
					else if (g_tBlockFlower[i][j].bFlower_Three_Wither && !(g_tBlockFlower[i][j].bFlower_Three_Delete))
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_WITHER;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Three_Delete = true;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI;
						}
					}
					else if (g_tBlockFlower[i][j].bFlower_Three_Wither && g_tBlockFlower[i][j].bFlower_Three_Delete)
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_NONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Three_Wither = false;
							g_tBlockFlower[i][j].bFlower_Three_Delete = false;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI = 0;
						}
					}
					break;

				case FLOWER_FOUR:
					if (!(g_tBlockFlower[i][j].bFlower_Four_Wither) && !(g_tBlockFlower[i][j].bFlower_Four_Delete))
					{
						g_tBlockFlower[i][j].nCurrentI++;
						if (g_tBlockFlower[i][j].nCurrentI >= 4)
						{
							g_tBlockFlower[i][j].bFlower_Four_Wither = true;
							g_tBlockFlower[i][j].nSaveI = g_tBlockFlower[i][j].nCurrentI;
						}

						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_ONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
					}
					else if (g_tBlockFlower[i][j].bFlower_Four_Wither && !(g_tBlockFlower[i][j].bFlower_Four_Delete))
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_WITHER;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Four_Delete = true;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI;
						}
					}
					else if (g_tBlockFlower[i][j].bFlower_Four_Wither && g_tBlockFlower[i][j].bFlower_Four_Delete)
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_NONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Four_Wither = false;
							g_tBlockFlower[i][j].bFlower_Four_Delete = false;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI = 0;
						}
					}
					break;

				case FLOWER_FIVE:
					if (!(g_tBlockFlower[i][j].bFlower_Five_Wither) && !(g_tBlockFlower[i][j].bFlower_Five_Delete))
					{
						g_tBlockFlower[i][j].nCurrentI++;
						if (g_tBlockFlower[i][j].nCurrentI >= 5)
						{
							g_tBlockFlower[i][j].bFlower_Five_Wither = true;
							g_tBlockFlower[i][j].nSaveI = g_tBlockFlower[i][j].nCurrentI;
						}

						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_ONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
					}
					else if (g_tBlockFlower[i][j].bFlower_Five_Wither && !(g_tBlockFlower[i][j].bFlower_Five_Delete))
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_WITHER;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Five_Delete = true;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI;
						}
					}
					else if (g_tBlockFlower[i][j].bFlower_Five_Wither && g_tBlockFlower[i][j].bFlower_Five_Delete)
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_NONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Five_Wither = false;
							g_tBlockFlower[i][j].bFlower_Five_Delete = false;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI = 0;
						}
					}
					break;

				case FLOWER_SIX:
					if (!(g_tBlockFlower[i][j].bFlower_Six_Wither) && !(g_tBlockFlower[i][j].bFlower_Six_Delete))
					{
						g_tBlockFlower[i][j].nCurrentI++;
						if (g_tBlockFlower[i][j].nCurrentI >= 6)
						{
							g_tBlockFlower[i][j].bFlower_Six_Wither = true;
							g_tBlockFlower[i][j].nSaveI = g_tBlockFlower[i][j].nCurrentI;
						}

						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_ONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
					}
					else if (g_tBlockFlower[i][j].bFlower_Six_Wither && !(g_tBlockFlower[i][j].bFlower_Six_Delete))
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_WITHER;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Six_Delete = true;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI;
						}
					}
					else if (g_tBlockFlower[i][j].bFlower_Six_Wither && g_tBlockFlower[i][j].bFlower_Six_Delete)
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_NONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Six_Wither = false;
							g_tBlockFlower[i][j].bFlower_Six_Delete = false;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI = 0;
						}
					}
					break;

				case FLOWER_SEVEN:
					if (!(g_tBlockFlower[i][j].bFlower_Seven_Wither) && !(g_tBlockFlower[i][j].bFlower_Seven_Delete))
					{
						g_tBlockFlower[i][j].nCurrentI++;
						if (g_tBlockFlower[i][j].nCurrentI >= 7)
						{
							g_tBlockFlower[i][j].bFlower_Seven_Wither = true;
							g_tBlockFlower[i][j].nSaveI = g_tBlockFlower[i][j].nCurrentI;
						}

						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_ONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
					}
					else if (g_tBlockFlower[i][j].bFlower_Seven_Wither && !(g_tBlockFlower[i][j].bFlower_Seven_Delete))
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_WITHER;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Seven_Delete = true;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI;
						}
					}
					else if (g_tBlockFlower[i][j].bFlower_Seven_Wither && g_tBlockFlower[i][j].bFlower_Seven_Delete)
					{
						g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type = FLOWER_NONE;
						SetStageCellType(STAGE_LAYER_FLOWER,i-g_tBlockFlower[i][j].nCurrentI,j,g_Stage_BlockFlower[i-g_tBlockFlower[i][j].nCurrentI][j]->type);
						g_tBlockFlower[i][j].nCurrentI--;
						if (g_tBlockFlower[i][j].nCurrentI <= 0)
						{
							g_tBlockFlower[i][j].bFlower_Seven_Wither = false;
							g_tBlockFlower[i][j].bFlower_Seven_Delete = false;
							g_tBlockFlower[i][j].nCurrentI = g_tBlockFlower[i][j].nSaveI = 0;
						}
					}
					break;

				}
			}
		}
	}
}

//=============================================================================
// 配置データファイル名取得
// ※「BlockFlower」と「BLOCKSAMPLE」の部分を、変更してね
//=============================================================================
TCHAR* GetBlockFlower_TableFileName(void){
	return BLOCKFLOWER_CELLTABLE_FILENAME;
}

//=============================================================================
// バルブとのあたり判定
//=============================================================================
bool CheckHitFlower(const tCollisionData& DATA )
{
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	{
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			if (g_Stage_BlockFlower[i][j]->type == FLOWER_NONE || g_Stage_BlockFlower[i][j]->type == FLOWER_WITHER)
				continue;

			// 花があるとき
			if ( CheckCollision(DATA,g_Stage_BlockFlower[i][j]->collision) )
			{
				return true;
			}
		}
	}
	return false;
}
