#include "BlockMove.h"
#include "Player.h"
#include "Data.h"
#include "BlockGoal.h"
//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PrestartBlockMove(void);
void UpdateBlockMove(void);
TCHAR* GetBlockMove_TableFileName(void);
void SetMoveBlock(int fromMax_y , int fromMax_x , int value , int UporDown );
bool CheckBlockMove(tCollisionData& DATA,tCollisionData& DATA2 );
static int frame = 10;
//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*		g_Stage_BlockMove[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];


//tStageCell*		g_Stage_BeforeBlockMove[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];
//=============================================================================
// 初期化処理
//=============================================================================
void InitBlockMove(void){
	LinkStage_PreStartFunc(PrestartBlockMove , STAGE_LAYER_BLOCK_MOVE);

	LinkStage_CellArray( g_Stage_BlockMove, STAGE_LAYER_BLOCK_MOVE );

	LinkStage_UpdateFunc( UpdateBlockMove, STAGE_LAYER_BLOCK_MOVE );

	LinkStage_TableFunc( GetBlockMove_TableFileName, STAGE_LAYER_BLOCK_MOVE );

}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockMove(void){

	//移動ブロック配置
	//変数1,2:移動させるブロックを指定
	//変数3  :移動フレームを指定　* (移動させる距離によって変更する)
	//変数4  :上移動 = 1,下移動 = 2

	// csvファイル参照

	if(GetData(DATA_SELECTMAIN) == 1)
	{
		switch(GetData(DATA_SELECTSUB))
		{
		case 1:
			//移動ブロック�@
			SetMoveBlock(11,39,530,1);
			SetMoveBlock(11,40,530,1);
			SetMoveBlock(11,41,530,1);
			break;
		case 2:
			//移動ブロック�@
			SetMoveBlock(11,32,720,1);
			SetMoveBlock(11,33,720,1);
			SetMoveBlock(11,34,720,1);
			//移動ブロック�A
			SetMoveBlock(12,65,420,1);
			SetMoveBlock(12,66,420,1);
			SetMoveBlock(12,67,420,1);
			//移動ブロック�B
			SetMoveBlock(9,83,420,1);
			SetMoveBlock(9,84,420,1);
			SetMoveBlock(9,85,420,1);
			//移動ブロック�C
			SetMoveBlock(9,91,420,1);
			SetMoveBlock(9,92,420,1);
			SetMoveBlock(9,93,420,1);
			break;
		case 3:
			//移動ブロック�@
			SetMoveBlock(9,24,500,2);
			SetMoveBlock(9,25,500,2);
			SetMoveBlock(9,26,500,2);
			//移動ブロック�A
			SetMoveBlock(9,69,500,2);
			SetMoveBlock(9,70,500,2);
			break;
		}
	}
	if(GetData(DATA_SELECTMAIN) == 2)
	{
		switch(GetData(DATA_SELECTSUB))
		{
		case 1:
			//移動ブロック�@
			SetMoveBlock(11,72,600,1);
			SetMoveBlock(11,73,600,1);
			SetMoveBlock(11,74,600,1);
			//移動ブロック�A
			SetMoveBlock(11,94,600,1);
			SetMoveBlock(11,95,600,1);
			SetMoveBlock(11,96,600,1);
			break;
		case 2:
			//移動ブロック�@
			SetMoveBlock(10,28,500,1);
			SetMoveBlock(10,29,500,1);
			SetMoveBlock(10,30,500,1);
			//移動ブロック�A
			SetMoveBlock(10,33,500,1);
			SetMoveBlock(10,34,500,1);
			SetMoveBlock(10,35,500,1);
			//移動ブロック�B
			SetMoveBlock(11,38,500,1);
			SetMoveBlock(11,39,500,1);
			SetMoveBlock(11,40,500,1);
			//移動ブロック�C
			SetMoveBlock(11,43,500,1);
			SetMoveBlock(11,44,500,1);
			SetMoveBlock(11,45,500,1);
			//移動ブロック�D
			SetMoveBlock(8,48,500,1);
			SetMoveBlock(8,49,500,1);
			SetMoveBlock(8,50,500,1);
			break;
		case 3:
			break;
		}
	}
	if(GetData(DATA_SELECTMAIN) == 3)
	{
		switch(GetData(DATA_SELECTSUB))
		{
		case 1:
			//移動ブロック�@
			SetMoveBlock(7,64,400,2);
			SetMoveBlock(7,65,400,2);
			SetMoveBlock(7,66,400,2);
			//移動ブロック�A
			SetMoveBlock(7,70,600,2);
			SetMoveBlock(7,71,600,2);
			SetMoveBlock(7,72,600,2);
			break;
		case 2:
			//移動ブロック�@
			SetMoveBlock(11,42,530,1);
			SetMoveBlock(11,43,530,1);
			SetMoveBlock(11,44,530,1);
			//移動ブロック�A
			SetMoveBlock(11,66,530,1);
			SetMoveBlock(11,67,530,1);
			SetMoveBlock(11,68,530,1);
			break;
		case 3:
			//移動ブロック�@
			SetMoveBlock(11,67,500,1);
			SetMoveBlock(11,68,500,1);
			SetMoveBlock(11,69,500,1);
			break;
		}
	}
	if(GetData(DATA_SELECTMAIN) == 4)
	{
		switch(GetData(DATA_SELECTSUB))
		{
		case 1:
			//移動ブロック�@
			SetMoveBlock(11,29,500,1);
			SetMoveBlock(11,30,500,1);
			SetMoveBlock(11,31,500,1);
			SetMoveBlock(11,32,500,1);
			//移動ブロック�A
			SetMoveBlock(8,66,500,1);
			SetMoveBlock(8,67,500,1);
			SetMoveBlock(8,68,500,1);
			//移動ブロック�B
			SetMoveBlock(7,69,450,1);
			SetMoveBlock(7,70,450,1);
			SetMoveBlock(7,71,450,1);
			//移動ブロック�C
			SetMoveBlock(9,72,630,1);
			SetMoveBlock(9,73,630,1);
			SetMoveBlock(9,74,630,1);
			break;
		case 2:
			//移動ブロック�@
			SetMoveBlock(12,61,500,1);
			SetMoveBlock(12,62,500,1);
			SetMoveBlock(12,63,500,1);
			break;
		case 3:
			//移動ブロック�@
			SetMoveBlock(11,35,520,1);
			SetMoveBlock(11,36,520,1);
			SetMoveBlock(11,37,520,1);
			//移動ブロック�A
			SetMoveBlock(12,56,500,1);
			SetMoveBlock(12,57,500,1);
			SetMoveBlock(12,58,500,1);
			break;
		}
	}

	int tmpPNum = GetPlayerNumber();

	if(tmpPNum == NAME_TACK){
		frame++;
	}

	// ※不要っぽい処理なので消去
	/*
	for(int i = 0; i > STAGE_CELL_ROW_NUM;i++)
	{
		for(int j = 0; j > STAGE_CELL_COLUMN_NUM;j++)
		{
			if(g_Stage_BlockMove[i][j]->type == 0) continue;

			CreateCollision(&g_Stage_BlockMove[i][j]->collision,g_Stage_BlockMove[i][j]->obj.pos,COLLISION_TYPE_BOX,
				(float)STAGE_CELL_WIDTH,(float)STAGE_CELL_HEIGHT);

			SetVertexPolygon(&g_Stage_BlockMove[i][j]->obj);
		}
	}*/
}
//=============================================================================
// ステージ開始時の処理
// ※各ステージの開始時に毎回やって欲しい処理
//=============================================================================
void PrestartBlockMove(void){
	frame = 10;
}
//=============================================================================
// 配置データファイル名取得
//=============================================================================
TCHAR* GetBlockMove_TableFileName(void){
	return BLOCKMOVE_CELLTABLE_FILENAME;
}
//=============================================================================
//ブロック移動処理
//=============================================================================
void SetMoveBlock(int fromMax_y , int fromMax_x , int value , int UporDown ){

	int tmpPNum = GetPlayerNumber();

	D3DXVECTOR3 tmpfrom = g_Stage_BlockMove[fromMax_y][fromMax_x]->obj.pos;

	//float diff_y = tmpto.y - tmpfrom.y;

	//float MoveSpeed = diff_y / frame;

	if(tmpPNum == NAME_TACK)
	{

		if(UporDown == 2){

			g_Stage_BlockMove[fromMax_y][fromMax_x]->obj.pos.y += 1.0f * ((frame%value >= value/2)?-1:1);
			g_Stage_BlockMove[fromMax_y+1][fromMax_x]->obj.pos.y += 1.0f * ((frame%value >= value/2)?-1:1);
		}

		if(UporDown == 1){

			g_Stage_BlockMove[fromMax_y][fromMax_x]->obj.pos.y -= 1.0f * ((frame%value >= value/2)?-1:1);
			g_Stage_BlockMove[fromMax_y+1][fromMax_x]->obj.pos.y -= 1.0f * ((frame%value >= value/2)?-1:1);

		}

		SetVertexPolygon(&g_Stage_BlockMove[fromMax_y][fromMax_x]->obj);
		SetVertexPolygon(&g_Stage_BlockMove[fromMax_y+1][fromMax_x]->obj);

	}

	// ※ループ変数(i,j)が処理に関わって無いので、for文を削除
	// 　CheckBlockMoveも同様なので、CheckCollisionに変更
	if(CheckCollision(GetPlayerCollision(tmpPNum),g_Stage_BlockMove[fromMax_y][fromMax_x]->collision)){
		D3DXVECTOR3 tmpPos = D3DXVECTOR3(GetPlayerPos(tmpPNum).x,g_Stage_BlockMove[fromMax_y][fromMax_x]->obj.pos.y - STAGE_CELL_HEIGHT - 10.0f,0.0f);
		D3DXVECTOR3 tmpPos2 = D3DXVECTOR3(GetPlayerPos(!tmpPNum).x,g_Stage_BlockMove[fromMax_y][fromMax_x]->obj.pos.y - STAGE_CELL_HEIGHT - 10.0f,0.0f);
		SetPlayerPos(tmpPNum,tmpPos);
		SetPlayerPos(!tmpPNum,tmpPos2);
	}
	/*
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	{
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			if(g_Stage_BlockMove[i][j]->type == 0) continue;

			if(CheckBlockMove(GetPlayerCollision(tmpPNum),g_Stage_BlockMove[fromMax_y][fromMax_x]->collision)){

					D3DXVECTOR3 tmpPos = D3DXVECTOR3(GetPlayerPos(tmpPNum).x,g_Stage_BlockMove[fromMax_y][fromMax_x]->obj.pos.y - STAGE_CELL_HEIGHT - 10.0f,0.0f);

					D3DXVECTOR3 tmpPos2 = D3DXVECTOR3(GetPlayerPos(!tmpPNum).x,g_Stage_BlockMove[fromMax_y][fromMax_x]->obj.pos.y - STAGE_CELL_HEIGHT - 10.0f,0.0f);

					SetPlayerPos(tmpPNum,tmpPos);
					
					SetPlayerPos(!tmpPNum,tmpPos2);

				}

			if(CheckBlockMove(GetPlayerCollision(tmpPNum),g_Stage_BlockMove[fromMax_y+1][fromMax_y]->collision)){

					D3DXVECTOR3 tmpPos = D3DXVECTOR3(GetPlayerPos(tmpPNum).x,GetPlayerBeforePos(tmpPNum).y,0.0f);

					D3DXVECTOR3 tmpPos2 = D3DXVECTOR3(GetPlayerPos(!tmpPNum).x,GetPlayerBeforePos(!tmpPNum).y,0.0f);

					SetPlayerPos(tmpPNum,tmpPos);
					
					SetPlayerPos(!tmpPNum,tmpPos2);
				}
		}
	}*/
}

//=============================================================================
//1フレーム前移動ブロック中心座標取得
//=============================================================================
/*tStageCell GetBeforeBlockMove(int layer,int i,int j){
	return	g_Stage_BeforeBlockMove[layer][i][j];
}*/

bool CheckBlockMove(tCollisionData& DATA,tCollisionData& DATA2 ){

	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	{
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			if (g_Stage_BlockMove[i][j]->type == 0 ) continue;

			if(CheckCollision(DATA,DATA2))
			{
				return true;
			}
		}
	}
	return false;
}