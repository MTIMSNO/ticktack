//===================================================================================
//
// ステージ選択
//
//===================================================================================
#pragma once

#include "main.h"
#include "input.h"
#include "StageBase.h"
#include "TextureFont.h"
#include "Data.h"
#include "Sound.h"

#define MAX_SELECTOBJ		(14)

//-----------------------------------------------------------------------------------
// 構造体定義
//-----------------------------------------------------------------------------------
typedef struct _tSelectStage
{
	int			MainStageNum;
	int			SubStageNum;
	tObjData	tSelectObject[MAX_SELECTOBJ];

}tSelectStage;

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
void InitSelectStage(LPDIRECT3DDEVICE9 pDevice);
void UpdateSelectStage(void);
void DrawSelectStage(LPDIRECT3DDEVICE9 pDevice);

#ifdef _DEBUG
void DrawSelectStageInfo(void);
#endif