//=============================================================================
//=============================================================================

#include "PipeBase.h"
#include "Pipe_CellData.h"
#include "PolygonBase.h"
#include "input.h"
#include "Player.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define _DEBUG_

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void SetPipeCell_StageNum( int main_stage, int sub_stage );
bool CheckHitBB_A ( D3DXVECTOR3 pos1, D3DXVECTOR2 size1, D3DXVECTOR3 pos2, D3DXVECTOR2 size2 );

void ChangeArrayContent(int i,int j);

bool ClearDecision(void);

//*****************************************************************************
// 構造体定義
//*****************************************************************************
typedef struct _tPipeCell_Block{
	unsigned char	type;
	tObjData		obj;
	tCollisionData	collision;
}tPipeCell_Block;

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tPipeCell_Block	g_PipeCell_Block[PIPE_CELL_ROW][PIPE_CELL_COLUMN];

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT InitStagePipe(LPDIRECT3DDEVICE9 pDevice)
{
	for(int i=0; i<PIPE_CELL_ROW; i++){
		for(int j=0; j<PIPE_CELL_COLUMN; j++){
			// セル配置データの初期化
			g_PipeCell_Block[i][j].type = PIPE_NONE;
			tObjData&		tmpObj = g_PipeCell_Block[i][j].obj;
			tCollisionData&	tmpCol = g_PipeCell_Block[i][j].collision;

			tmpObj.pos = D3DXVECTOR3( PIPE_CELL_SIZE_X * j, PIPE_CELL_SIZE_Y * i, 0.0f );
			tmpObj.rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			tmpObj.fScale = 1.0f;
			tmpObj.size = D3DXVECTOR2( PIPE_CELL_SIZE_X, PIPE_CELL_SIZE_Y );
			tmpObj.pAnim = NULL;

			
			CreateCollision(
				&g_PipeCell_Block[i][j].collision,
				tmpObj.pos, COLLISION_TYPE_BOX,
				PIPE_CELL_SIZE_X/2, PIPE_CELL_SIZE_Y/2, 0.0f
				);
				
			MakePolygon( pDevice, &tmpObj, PIPE_CELL_TEXTURE );
			tmpObj.bExist = false;

			SetPipe_PipeNum( 1, 1 );
		}
	}

	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void UninitStagePipe(void)
{
	for(int i=0; i<PIPE_CELL_ROW; i++){
		for(int j=0; j<PIPE_CELL_COLUMN; j++){
			UninitPolygonAndTexture( &g_PipeCell_Block[i][j].obj );
		}
	}
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateStagePipe(void)
{
	for(int i=0; i<PIPE_CELL_ROW; i++){
		for(int j=0; j<PIPE_CELL_COLUMN; j++){
			SetVertexPolygon( &g_PipeCell_Block[i][j].obj );
			SetTexturePolygon( &g_PipeCell_Block[i][j].obj );
		}
	}

	// クリア判定
	ClearDecision();

	// ボタンを押すとブロック配置が変化します
	if ( GetKeyboardPress(DIK_A) ){
		SetPipe_PipeNum( 1, 1 );
	}
	else if ( GetKeyboardPress(DIK_S) ){
		SetPipe_PipeNum( 1, 2 );
	}
	else if ( GetKeyboardPress(DIK_D) ){
		SetPipe_PipeNum( 1, 3 );
	}
}

//=============================================================================
// 描画処理
//=============================================================================
void DrawStagePipe(LPDIRECT3DDEVICE9 pDevice)
{
	for(int i=0; i<PIPE_CELL_ROW; i++){
		for(int j=0; j<PIPE_CELL_COLUMN; j++){
			tObjData& tmpObj = g_PipeCell_Block[i][j].obj;
			if ( tmpObj.bExist == false ){
				continue;
			}

			// 頂点バッファをデバイスのデータストリームにバインド
			pDevice->SetStreamSource( 0, tmpObj.pD3DVtxBuff, 0, sizeof(VERTEX_2D) );
			// 頂点フォーマットの設定
			pDevice->SetFVF(FVF_VERTEX_2D);
			// テクスチャの設定
			pDevice->SetTexture( 0, tmpObj.pD3DTexture );
			// ポリゴンの描画
			pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
		}
	}
}

//=============================================================================
// 各ステージごとの初期設定
//=============================================================================
void SetPipe_PipeNum( int main_stage, int sub_stage ){
	// データ範囲外を操作しないようにチェック
	if( ( main_stage > PIPE_NUM_MAIN ) ||
		( main_stage <= 0 ) ||
		( sub_stage  > PIPE_NUM_SUB ) ||
		( sub_stage  <= 0 ) ){
		return;
	}

	SetPipeCell_StageNum( main_stage, sub_stage );
}

//=============================================================================
// 各ステージごとのセル配置データを反映
//=============================================================================
void SetPipeCell_StageNum( int main_stage, int sub_stage ){
	// 配列添え字として扱う為に1減らす
	main_stage--;
	sub_stage--;

	for(int i=0; i<PIPE_CELL_ROW; i++){
		for(int j=0; j<PIPE_CELL_COLUMN; j++){
			SetPipeCellType( i, j, c_PipeData[main_stage][sub_stage][ i * PIPE_CELL_COLUMN + j ]);
		}
	}
}

//=============================================================================
// チップ設定処理
//=============================================================================
void SetPipeCellType( int row, int column, int type ){
	// データ範囲外を操作しないようにチェック
	if( ( row >= PIPE_CELL_ROW ) ||
		( column >= PIPE_CELL_COLUMN ) ||
		( type < PIPE_NONE ) ||
		( type >= PIPE_MAX ) ){
		return;
	}

	// 新しいセルデータを反映し、ブロックがある場合はテクスチャ変更
	if( ( g_PipeCell_Block[row][column].type = type ) > PIPE_NONE ){
		SetPolygonTextureRect( g_PipeCell_Block[row][column].obj, c_Pipe_CellData[ type ] );
		g_PipeCell_Block[row][column].obj.bExist = true;
	}
	else{
		g_PipeCell_Block[row][column].obj.bExist = false;
	}
}


//=============================================================================
// 地形との当たり判定処理
//=============================================================================
bool CheckHit_PipeCell( const tCollisionData& DATA ){
	for(int i=0; i<PIPE_CELL_ROW; i++){
		for(int j=0; j<PIPE_CELL_COLUMN; j++){
			if(!(g_PipeCell_Block[i][j].type == PIPE_LENGTH_VALVE || g_PipeCell_Block[i][j].type == PIPE_WIGHT_VALVE)){
				continue;
			}

			// バルブがあるとき
			if( CheckCollision( DATA, g_PipeCell_Block[i][j].collision ) ){
				ChangeArrayContent(i,j);
				return true;
			}
		}
	}
	return false;
}

//=============================================================================
// 配列の内容変更
//=============================================================================
void ChangeArrayContent(int i,int j)
{
	// ブロックが違うとき
	if ( g_PipeCell_Block[i][j].type < PIPE_LENGTH_VALVE )
		return;

	// バルブのときだけ
	// その方向の操作済みに変更
	if ( GetPlayerFlag(0))
	switch( g_PipeCell_Block[i][j].type )
	{
	case PIPE_LENGTH_VALVE:
		g_PipeCell_Block[i][j].type = PIPE_LENGTH_SETTLED_VALVE;
		SetPolygonTextureRect(g_PipeCell_Block[i][j].obj,c_Pipe_CellData[g_PipeCell_Block[i][j].type]);
		break;

	case PIPE_WIGHT_VALVE:
		g_PipeCell_Block[i][j].type = PIPE_WIGHT_SETTLED_VALVE;
		SetPolygonTextureRect(g_PipeCell_Block[i][j].obj,c_Pipe_CellData[g_PipeCell_Block[i][j].type]);
		break;
	}
}

//=============================================================================
// 終了判定
//=============================================================================
bool ClearDecision(void)
{
	static int count = 0;
	for ( int i = 0; i < PIPE_CELL_ROW; i++ ){
		for ( int j = 0; j < PIPE_CELL_COLUMN; j++ ){
			// バルブ(済み)が１２個あるかどうか判定
			if ( g_PipeCell_Block[i][j].type == PIPE_LENGTH_SETTLED_VALVE ||
				 g_PipeCell_Block[i][j].type == PIPE_WIGHT_SETTLED_VALVE )
			{
				count++;
				if ( count >= PIPE_CLEAR_COUNT )
					return true;
			}
		}
	}

	// １２個に満たない場合には続行
	return false;

}

//=============================================================================
// パイプの中心座標の取得
//=============================================================================
D3DXVECTOR3 GetPipePos(int x,int y)
{
	return g_PipeCell_Block[y][x].obj.pos;
}

//=============================================================================
// パイプの中心座標の設定
//=============================================================================
void SetPipePos(int x, int y,D3DXVECTOR3 pos)
{
	 g_PipeCell_Block[y][x].obj.pos = pos;
}
//=============================================================================
// スクロール処理
//=============================================================================
/*void SetPipe_Scroll(void){
	D3DXVECTOR3	tmp_pos, pos_now = GetPlayerPos(0), pos_pre = GetPlayerBeforePos(0),
		Vec_Move = pos_now - pos_pre;

	if( D3DXVec3LengthSq( &Vec_Move ) > 0 ){
		SetPipePos( 0,0, pos_now - Vec_Move );

		for(int i=0; i<PIPE_CELL_ROW; i++){
			for(int j=0; j<PIPE_CELL_COLUMN; j++){
				g_PipeCell_Block[i][j].obj.pos -= Vec_Move;

				SetVertexPolygon( &g_PipeCell_Block[i][j].obj );
			}
		}
	}
}*/