//=============================================================================
//
// ステージ処理 [StageBase.h]
//
//=============================================================================

#ifndef ___STAGEPIPE_BASE_H___
#define ___STAGEPIPE_BASE_H___

#include "main.h"
#include "CollisionBase.h"

//-----------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------
#define PIPE_NUM_MAIN	(4)
#define PIPE_NUM_SUB	(3)
#define PIPE_CLEAR_COUNT	(12)

//-----------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------
HRESULT InitStagePipe(LPDIRECT3DDEVICE9 pDevice);
void UninitStagePipe(void);
void UpdateStagePipe(void);
void DrawStagePipe(LPDIRECT3DDEVICE9 pDevice);

// 任意マスステージセルの設定
void SetPipeCellType( int row, int column, int type );

// 任意ステージの初期設定
// ※1-1から開始、STAGE_NUM_MAIN-STAGE_NUM_SUBまで
void SetPipe_PipeNum( int main_stage, int sub_stage );

// パイプとの当たり判定
bool CheckHit_PipeCell( const tCollisionData& DATA );

D3DXVECTOR3 GetPipePos(int x,int y);
void SetPipePos(int x, int y,D3DXVECTOR3 pos);

#endif