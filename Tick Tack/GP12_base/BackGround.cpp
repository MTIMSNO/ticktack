//===================================================================================
// 背景
//===================================================================================

#include "BackGround.h"
#include "PolygonBase.h"
#include "polygonAnim.h"
#include "Player.h"
#include "Data.h"

//-----------------------------------------------------------------------------------
//　プロトタイプ宣言
//-----------------------------------------------------------------------------------

int TypeDesignation(int num);
void TypeSet(int i);

//-----------------------------------------------------------------------------------
//　構造体定義
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

tObjData			g_tBackGround[MAX_BACKGROUND];

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitBackGround(LPDIRECT3DDEVICE9 pDevice)
{
	for ( int i = 0; i < MAX_BACKGROUND; i++ )
	{
		// 各設定
		g_tBackGround[i].rot = D3DXVECTOR3(0.0f,0.0f,0.0f);
		g_tBackGround[i].fScale = 1.0f;
		g_tBackGround[i].bExist = true;
		g_tBackGround[i].nType = TypeDesignation(i);
		g_tBackGround[i].sizeMax = c_sizeBackGround00;
		g_tBackGround[i].pAnim = c_animBackGround00;

		// 各タイプごと設定
		TypeSet(i);

		// ポリゴンの作成
		MakePolygon3D(pDevice,&g_tBackGround[i],TEXTURE_BACKGROUND);
	}

	return S_OK;
}
//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitBackGround(void)
{
	for ( int i = 0; i < MAX_BACKGROUND; i++ )	UninitPolygonAndTexture(&g_tBackGround[i]);
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateBackGround(void)
{
	switch(GetData(DATA_MAINTRANSITION))
	{
	case STATE_SELECTMENU:
		for(int i = 0; i < MAX_BACKGROUND; i++)
		{
			if(g_tBackGround[i].nStates != STAGE_FOREST)
			{
				g_tBackGround[i].bExist = false;
				return;
			}

			// ビルボード設定
			g_tBackGround[i].rot.y = -atan2f(-1000.0f - 0.0f,0.0f - 0.0f);
			g_tBackGround[i].bExist = true;
			SetVertexPolygon3D(&g_tBackGround[i]);
			SetAnimationNo(&g_tBackGround[i],i);
			// スクロール設定
			if (g_tBackGround[i].nType != TYPE_FOURTH)
			SetTextureScroll3D(&g_tBackGround[i],GetPlayerSpeed().x/5000.0f);
			else 
			SetTextureScroll3D(&g_tBackGround[i],0.0f);
		}
		break;

	case STATE_GAME:
		for ( int i = 0; i < MAX_BACKGROUND; i++ )
		{
			if (g_tBackGround[i].nStates != STAGE_FOREST)
			{
				g_tBackGround[i].bExist = false;
				return;
			}

			// 表示設定
			g_tBackGround[i].bExist = true;
			// ビルボード設定
			g_tBackGround[i].rot.y = -atan2f(-1000.0f - 0.0f,0.0f - 0.0f);
			// ポリゴン座標のセット
			SetVertexPolygon3D(&g_tBackGround[i]);
			// アニメーションナンバー
			SetAnimationNo(&g_tBackGround[i],i);
			// スクロール設定
			if (g_tBackGround[i].nType != TYPE_FOURTH)
			SetTextureScroll3D(&g_tBackGround[i],GetPlayerSpeed().x/5000.0f);
			else 
			SetTextureScroll3D(&g_tBackGround[i],0.0f);
		}
		break;
	}
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawBackGround(LPDIRECT3DDEVICE9 pDevice)
{
	//透明色部分を正しく表示する設定
	pDevice->SetRenderState ( D3DRS_ALPHAREF , 0x00000080 );

	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);

	for ( int i = 0; i < MAX_BACKGROUND; i++ )
	{
		if( g_tBackGround[i].bExist != true ){
			// D3DRS_ALPHAREFの設定を戻してほしいので、最後のリセット処理まで処理を継続する
			// ※returnだと処理が中断されてしまう
			continue;
		}

		// 頂点バッファをデバイスのデータストリームにバインド
		pDevice->SetStreamSource( 0, g_tBackGround[i].pD3DVtxBuff, 0, sizeof(VERTEX_3D) );

		// ワールドトランスフォーム（絶対座標変換）
		D3DXMATRIXA16 matWorld, matRY, matTran;
		tObjData& to = g_tBackGround[i];

		D3DXMatrixIdentity( &matWorld );									//初期化
		D3DXMatrixRotationY( &matRY, to.rot.y );							//ローテーションY
		D3DXMatrixTranslation( &matTran, to.pos.x, to.pos.y, to.pos.z );	//平行移動

		D3DXMatrixMultiply( &matWorld, &matWorld, &matRY );
		D3DXMatrixMultiply( &matWorld, &matWorld, &matTran );
		pDevice->SetTransform( D3DTS_WORLD, &matWorld );

		// テクスチャの設定
		pDevice->SetTexture( 0, g_tBackGround[i].pD3DTexture );
	
		// ポリゴンの描画
		pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
	}

	// 2Dポリゴンの描画に影響するので描画が終わったら設定を元に戻して！
	pDevice->SetRenderState ( D3DRS_ALPHAREF , 0x00000000 );
}

//-----------------------------------------------------------------------------------
// タイプ指定
//-----------------------------------------------------------------------------------
int TypeDesignation(int num)
{
	switch ( num )
	{
		// 森
	case 0:
		g_tBackGround[num].nStates = STAGE_FOREST;
		return TYPE_FOURTH;

		// 火山
	case 1:
		g_tBackGround[num].nStates = STAGE_VOLCANO;
		return TYPE_FIRST;
	case 2:
		g_tBackGround[num].nStates = STAGE_VOLCANO;
		return TYPE_SECOND;
	case 3:
		g_tBackGround[num].nStates = STAGE_VOLCANO;
		return TYPE_THIRD;
	case 4:
		g_tBackGround[num].nStates = STAGE_VOLCANO;
		return TYPE_FOURTH;

		// 海
	case 5:
		g_tBackGround[num].nStates = STAGE_SEA;
		return TYPE_FIRST;
	case 6:
		g_tBackGround[num].nStates = STAGE_SEA;
		return TYPE_SECOND;
	case 7:
		g_tBackGround[num].nStates = STAGE_SEA;
		return TYPE_FOURTH;
	}
	return -1;
}

//-----------------------------------------------------------------------------------
// タイプごとの指定
//-----------------------------------------------------------------------------------
void TypeSet(int i)
{
	switch ( g_tBackGround[i].nType )
		{
		case TYPE_FIRST:
			g_tBackGround[i].pos = D3DXVECTOR3(0.0f,100.0f,-320.0f);
			g_tBackGround[i].size = D3DXVECTOR2((float)TEXTURE_SIZE_X,(float)TEXTURE_SIZE_Y);
			break;

		case TYPE_SECOND:
			g_tBackGround[i].pos = D3DXVECTOR3(0.0f,100.0f,-280.0f);
			g_tBackGround[i].size = D3DXVECTOR2((float)TEXTURE_SIZE_X,(float)TEXTURE_SIZE_Y);
			break;

		case TYPE_THIRD:
			g_tBackGround[i].pos = D3DXVECTOR3(0.0f,0.0f,-240.0f);
			g_tBackGround[i].size = D3DXVECTOR2((float)TEXTURE_BG_SIZE_X,(float)TEXTURE_BG_SIZE_Y);
			break;

		case TYPE_FOURTH:
			g_tBackGround[i].pos = D3DXVECTOR3(0.0f,0.0f,-200.0f);
			g_tBackGround[i].size = D3DXVECTOR2((float)TEXTURE_BG_SIZE_X,(float)TEXTURE_BG_SIZE_Y);
			break;
		}
}