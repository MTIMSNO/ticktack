//=============================================================================
//
// プレイヤー処理 [Player.h]
//
//=============================================================================
#pragma once

#include "main.h"
#include "CollisionBase.h"
#include "PolygonBase.h"
#include "Sound.h"
//-----------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------

#define PLAYER_SIZE_X			(75)
#define PLAYER_SIZE_Y			(75)
#define MAX_PLAYER				(2)

#define PLAYER_SPEED_X			(0.3f)
#define PLAYER_SPEED_RESIST_X	(0.2f)
#define PLAYER_SPEED_GRAVITY_Y	(0.8f)
#define PLAYER_SPEED_MAX_X		(3.0f)
#define PLAYER_SPEED_MAX_Y		(8.0f)
#define PLAYER_SPEED_MAX_UP		(14.0f)

//-----------------------------------------------------------------------------
// 列挙型定義
//-----------------------------------------------------------------------------

enum ACTIONFLAG
{
	ACT_VALVE,

	MAX_ACT,
};

//-----------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------
// 初期化処理
HRESULT InitPlayer(LPDIRECT3DDEVICE9 pDevice);
// 終了処理
void UninitPlayer(void);
// 更新処理
void UpdatePlayer(void);
// 描画処理
void DrawPlayer(LPDIRECT3DDEVICE9 pDevice);


//D3DXVECTOR3 GetPlayerPos(void);
D3DXVECTOR3 GetPlayerPos(int num);

//D3DXVECTOR3 GetPlayerPos(void);
D3DXVECTOR3 GetPlayerPos(int num);

// プレイヤーの中心座標の取得
D3DXVECTOR3 GetPlayerPos(int num);

// プレイヤーの移動フラグの取得
bool GetPlayerFlagIsRight(void);
bool GetPlayerFlagIsLeft(void);
bool GetFlagIsStop(void);

void DrawPlayerPos(void);


//*************************************************

// 現在選択中のプレイヤーナンバーの取得
int GetPlayerNumber(void);
// プレイヤーの中心座標の取得
D3DXVECTOR3 GetPlayerPos(int num);
// プレイヤーの移動スピードの取得
D3DXVECTOR2 GetPlayerSpeed(void);

// 1フレーム前のプレイヤーの位置の取得
D3DXVECTOR3 GetPlayerBeforePos(int num);
// プレイヤーのtCollisionDataの取得
tCollisionData GetPlayerCollision(int num);
// プレイヤー座標の設定
void SetPlayerPos(int num, D3DXVECTOR3 pos);


void DrawPlayerPos(void);


// 各フラグの取得
bool GetPlayerFlag(int FlagNum);
//*************************************************
#ifdef _DEBUG
// プレイヤー情報の表示
//void DrawPlayerInfo(void);
#endif

// プレイヤーの移動スピード加算
void AddPlayerSpeed( D3DXVECTOR3 speed );

// プレイヤーの移動スピード設定
void SetPlayerSpeed( D3DXVECTOR3 speed );

// プレイヤーの重力方向速度ベクトル消去
void ErazePlayerSpeed_Gravity(void);

// プレイヤーが切り替わったか？
bool GetPlayerChange(void);

// 再初期化
void ReInitPlayer(void);
