
//===================================================================================
//	フェードイン/フェードアウト
//===================================================================================

#include "Fade.h"
#include "PolygonBase.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define FADE_SPEED_DEFAULT	(0.01f)

//*****************************************************************************
// 列挙型定義
//*****************************************************************************
enum{
	FADE_STATUS_OUT,		// フェードアウト中
	FADE_STATUS_COMPLETE,	// フェードアウト完了
	FADE_STATUS_IN,			// フェードイン中
	FADE_STATUS_NONE,		// 何も無しの状態（フェードイン完了）
	FADE_STATUS_MAX
};

//*****************************************************************************
// 構造体定義
//*****************************************************************************
typedef struct _tFadeData{
	int			status;
	bool		OnFlash;
	tObjData	obj;

	float		power;
	float		alpha;
	float		speed;
	D3DXCOLOR	color;
}tFadeData;

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void Process_SetFade( tFadeData& data, int frame );
void Process_SetFadePolygonColor(void);

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tFadeData	g_FadeData;

//=============================================================================
// 初期化処理
//=============================================================================
void InitFade( LPDIRECT3DDEVICE9 pDevice ){
	// フェード管理データの初期化
	tFadeData&	tmpData = g_FadeData;
	tmpData.status = FADE_STATUS_NONE;
	tmpData.OnFlash = false;
	tmpData.power = 1.0f;
	tmpData.alpha = 0.0f;
	tmpData.speed = FADE_SPEED_DEFAULT;
	tmpData.color = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 0.0f);

	// フェード用ポリゴンの作成
	tObjData&	tmpObj = tmpData.obj;
	tmpObj.pos = D3DXVECTOR3( SCREEN_CENTER_X, SCREEN_CENTER_Y, -10.0f );
	tmpObj.rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	tmpObj.fScale = 1.0f;
	tmpObj.size = D3DXVECTOR2( SCREEN_WIDTH, SCREEN_HEIGHT );
	tmpObj.pAnim = NULL;
	MakePolygon( pDevice, &tmpObj, NULL );
	tmpObj.bExist = false;
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateFade(void){
	tFadeData& data = g_FadeData;

	switch( data.status ){
	case FADE_STATUS_NONE:
		// フェード設定が無い場合は中断
		return;
		break;
	case FADE_STATUS_OUT:
		// フェードアウト状態の処理
		data.alpha += data.speed;
		if( data.alpha > data.power ){
			data.alpha = data.power;
			data.status = (data.OnFlash)? FADE_STATUS_IN: FADE_STATUS_COMPLETE;
		}
		break;
	case FADE_STATUS_IN:
		// フェードイン状態の処理
		data.alpha -= data.speed;
		if( data.alpha < 0.0f ){
			data.alpha = 0.0f;
			data.status = FADE_STATUS_NONE;
			// フェードインが完了したら描画を切る
			data.obj.bExist = false;
		}
		break;
	default:
		break;
	}

	// アルファ値の変化を反映させる
	Process_SetFadePolygonColor();
}

//=============================================================================
// 終了処理
//=============================================================================
void UninitFade(void){
	UninitPolygonAndTexture( &g_FadeData.obj );
}

//=============================================================================
// 描画処理
//=============================================================================
void DrawFade(LPDIRECT3DDEVICE9 pDevice){
	if( g_FadeData.obj.bExist ){
		// 頂点フォーマットの設定
		pDevice->SetFVF(FVF_VERTEX_2D);
		// テクスチャの設定
		pDevice->SetTexture( 0, NULL );
		// 頂点バッファをデバイスのデータストリームにバインド
		pDevice->SetStreamSource( 0, g_FadeData.obj.pD3DVtxBuff, 0, sizeof(VERTEX_2D) );
		// ポリゴンの描画
		pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
	}
}

//=============================================================================
// フェードイン・アウトの処理
//=============================================================================
//void SetFade( int flag, int frame, D3DXCOLOR& color, float power ){
bool SetFade( int flag, int frame, D3DXCOLOR& color, float power ){
	tFadeData& data = g_FadeData;

	switch( flag ){
	case FADE_IN:
		if( data.status != FADE_STATUS_COMPLETE ){
			// フェードアウトが完了していなければ中断
			return false;
		}

		// 指定フレーム数で完了するフェードイン処理を開始する
		Process_SetFade( data, frame );
		data.status = FADE_STATUS_IN;
		break;
	case FADE_OUT:
		if( data.status != FADE_STATUS_NONE ){
			// フェードインが完了していなければ中断
			return false;
		}

		// 指定フレーム数で完了するフェードアウト処理を開始する
		Process_SetFade( data, frame );
		data.status = FADE_STATUS_OUT;
		data.color = color;
		break;
	case FADE_FLASH:
		if( data.status != FADE_STATUS_NONE ){
			// フェードインが完了していなければ中断
			return false;
		}
		// フラッシュ強度設定
		data.power = (power > 1.0f)? 1.0f: power;
		// 設定フレーム数で完了するようにスピードセット
		data.speed = (frame <= 0)? FADE_SPEED_DEFAULT: (2 * data.power / frame);

		data.color = color;
		data.OnFlash = true;
		data.status = FADE_STATUS_OUT;
		break;
	default:
		break;
	}

	// 新しい色へ更新
	Process_SetFadePolygonColor();
	// フェード処理が開始されるので描画を復活
	data.obj.bExist = true;

	return true;
}

//=============================================================================
// フェード開始設定の処理
//=============================================================================
void Process_SetFade( tFadeData& data, int frame ){
	// フラッシュフラグ・強度のリセット
	data.OnFlash = false;
	data.power = 1.0f;
	// 設定フレーム数で完了するようにスピードセット
	data.speed = (frame <= 0)? FADE_SPEED_DEFAULT: (data.power / frame);
}

//=============================================================================
// 現在の色・アルファを反映させる
//=============================================================================
void Process_SetFadePolygonColor(void){
	tFadeData& data = g_FadeData;

	D3DXCOLOR color = data.color;
	color.a = data.alpha;
	SetPolygonDiffuse( &data.obj, color );
}

//=============================================================================
// フェードアウト完了した？
//=============================================================================
bool GetFadeOutComplete(void){
	return ( g_FadeData.status == FADE_STATUS_COMPLETE );
}