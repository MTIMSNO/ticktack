#include "BlockBird.h"
#include "Player.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PreStartBlockBird(void);
void UpdateBlockBird(void);
TCHAR* GetBlockBird_TableFileName(void);
void IsHitBird(void);
void MoveBird(void);
void BalloonAnime(void);
void SetVertexBalloonPolygon( tObjData* obj );

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*		g_Stage_BlockBird[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];
D3DXVECTOR3		g_dMoveSpeed;
bool			g_bMoveBird;

int				g_nSaveI;
int				g_nSaveJ;

//=============================================================================
// 初期化処理
//=============================================================================
void InitBlockBird(LPDIRECT3DDEVICE9 pDevice){
	// ここで「StageBase.cpp」内で用意した、各マスのポリゴンへのポインタを取得する
	// ※二番目の引数は、「StageBase.h」『列挙型定義』に追加した、『STAGE_LAYER_〜〜』を入れる
	// 　このサンプルでは地形より奥に表示しているので、『STAGE_LAYER_BACK001』を設定してます
	LinkStage_CellArray( g_Stage_BlockBird, STAGE_LAYER_BIRD );

	// ここで「StageBase.cpp」から、
	// 「BlockBird.cpp」内にある「PreStartBlockBird」「UpdateBlockBird」「GetBlockBird_TableFileName」関数にアクセスできるようにする
	// ※関数名を変更した場合は、ここの引数名も変更する
	// 　レイヤー設定は、上と同じものを使う
	LinkStage_PreStartFunc( PreStartBlockBird, STAGE_LAYER_BIRD );
	LinkStage_UpdateFunc( UpdateBlockBird, STAGE_LAYER_BIRD );
	LinkStage_TableFunc( GetBlockBird_TableFileName, STAGE_LAYER_BIRD );
	// アニメーション
	SetStageLayerAnimation( STAGE_LAYER_BIRD, c_sizeBird00, c_animBird00, "Bird02.png" );

	g_bMoveBird = false;
	g_dMoveSpeed = D3DXVECTOR3(0.05f,-0.025f,0.0f);
	g_nSaveI = g_nSaveJ = 0;

}

//=============================================================================
// ステージ開始時の処理
// ※各ステージの開始時に毎回やって欲しい処理
//=============================================================================
void PreStartBlockBird(void){
	for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
		for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
			SetAnimationNo( &g_Stage_BlockBird[i][j]->obj, g_Stage_BlockBird[i][j]->type);
			if(g_Stage_BlockBird[i][j]->type != BIRD_NONE)	g_Stage_BlockBird[i][j]->obj.bExist = true;

		}
	}
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockBird(void)
{
	// 鳥と当たっているときの処理
	IsHitBird();
	// 鳥の移動
	MoveBird();
	// 吹き出しのアニメーション
	BalloonAnime();

}

//=============================================================================
// 配置データファイル名取得
// ※「BlockBird」と「BLOCKSAMPLE」の部分を、変更してね
//=============================================================================
TCHAR* GetBlockBird_TableFileName(void){
	return BLOCKBIRD_CELLTABLE_FILENAME;
}

//=============================================================================
// 鳥とのあたり判定
//=============================================================================
bool CheckHitBird(const tCollisionData& DATA )
{
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	{
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			if (g_Stage_BlockBird[i][j]->type != BIRD_ON )	continue;

			// 鳥と当たっているとき
			if ( CheckCollision(DATA,g_Stage_BlockBird[i][j]->collision) )
			{
				g_nSaveI = i;
				g_nSaveJ = j;
				return true;
			}
		}
	}
	return false;
}

//=============================================================================
// 鳥とプレイヤーの処理
//=============================================================================
void IsHitBird(void)
{
	// チックと鳥が当たっているとき
	if (GetPlayerNumber() == NAME_TICK && CheckHitBird(GetPlayerCollision(GetPlayerNumber())))
	{
		// 前のフレームの位置へ戻す
		SetPlayerPos(GetPlayerNumber(),GetPlayerBeforePos(GetPlayerNumber()));
		// アニメーションナンバーの変更
		SetAnimationNo(&g_Stage_BlockBird[g_nSaveI][g_nSaveJ]->obj,1);
		g_bMoveBird = false;
	}
	// タックと鳥が当たっているとき
	else if (GetPlayerNumber() == NAME_TACK && CheckHitBird(GetPlayerCollision(GetPlayerNumber())))
	{
		// 吹き出しを非表示に
		g_Stage_BlockBird[g_nSaveI-2][g_nSaveJ]->obj.bExist = false;

		// アニメーションナンバーの変更
		SetAnimationNo(&g_Stage_BlockBird[g_nSaveI][g_nSaveJ]->obj,0);
		g_bMoveBird = true;
	}
}

//=============================================================================
// 鳥の移動
//=============================================================================
void MoveBird(void)
{
	if ( g_bMoveBird != true ) return;

	// 移動処理
	g_dMoveSpeed += g_dMoveSpeed;
	if ( g_dMoveSpeed.x >= 10.0f )	g_dMoveSpeed.x = 10.0f;
	if ( g_dMoveSpeed.y <= -5.0f )	g_dMoveSpeed.y = -5.0f;
	g_Stage_BlockBird[g_nSaveI][g_nSaveJ]->obj.pos += g_dMoveSpeed;

	// アニメーション
	if( g_Stage_BlockBird[g_nSaveI][g_nSaveJ]->obj.nAnimNo > -1 )
		Process_PolygonTexAnimation( &g_Stage_BlockBird[g_nSaveI][g_nSaveJ]->obj );
	
	SetVertexPolygon(&g_Stage_BlockBird[g_nSaveI][g_nSaveJ]->obj);

	// プレイヤーから一定の距離離れた場合
	if ( g_Stage_BlockBird[g_nSaveI][g_nSaveJ]->obj.pos.x - GetPlayerPos(GetPlayerNumber()).x >= SCREEN_WIDTH ||
		 g_Stage_BlockBird[g_nSaveI][g_nSaveJ]->obj.pos.y - GetPlayerPos(GetPlayerNumber()).y >= SCREEN_HEIGHT )
	{
		// 移動処理をfalseに
		g_bMoveBird = false;
		// スピードを初期化
		g_dMoveSpeed = D3DXVECTOR3(0.05f,-0.25f,0.0f);
		g_Stage_BlockBird[g_nSaveI][g_nSaveJ]->obj.bExist = false;
	}
}

//=============================================================================
// 吹き出しのアニメーション
//=============================================================================
void BalloonAnime(void)
{
	// アニメーション
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	{
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			if( g_Stage_BlockBird[i][j]->type == BALLOON)
			{
				g_Stage_BlockBird[i][j]->obj.size = D3DXVECTOR2((float)BALLOON_SIZE_X,(float)BALLOON_SIZE_Y);
				SetVertexBalloonPolygon(&g_Stage_BlockBird[i][j]->obj);
				Process_PolygonTexAnimation(&g_Stage_BlockBird[i][j]->obj);
			}
		}
	}
}

//=============================================================================
// 頂点データの設定
//=============================================================================
void SetVertexBalloonPolygon( tObjData* obj )
{
	VERTEX_2D* pVtx;

	// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
	obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

	// 基本値を設定
	obj->fRadius = sqrtf(obj->size.x * obj->size.x + obj->size.y * obj->size.y) / 2.0f;
	obj->fBaseAngle = atan2f(obj->size.x, obj->size.y);

	// 頂点座標の設定
	pVtx[0].vtx.x = obj->pos.x - sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[0].vtx.y = obj->pos.y - cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[0].vtx.z = 0.0f;
	pVtx[1].vtx.x = obj->pos.x + sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[1].vtx.y = obj->pos.y - cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[1].vtx.z = 0.0f;
	pVtx[2].vtx.x = obj->pos.x - sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[2].vtx.y = obj->pos.y + cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[2].vtx.z = 0.0f;
	pVtx[3].vtx.x = obj->pos.x + sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[3].vtx.y = obj->pos.y + cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[3].vtx.z = 0.0f;
	

	// 表示ずれ対策（これを行っても、テクスチャサイズの問題でずれる可能性はある）
	for( int i = 0; i <= 3; i++ )
	{
		pVtx[i].vtx.x -= 0.5f;
		pVtx[i].vtx.y -= 0.5f;
		D3DXCOLOR temp = pVtx[i].diffuse;
		temp.a = obj->fAlpha;
		pVtx[i].diffuse = temp;
	}

	// 頂点データをアンロックする
	obj->pD3DVtxBuff->Unlock();
}