//===================================================================================
// 背景
//===================================================================================

#include "main.h"

//-----------------------------------------------------------------------------------
//　マクロ定義
//-----------------------------------------------------------------------------------

#define TEXTURE_BACKGROUND		_T("BackGround.png")
#define TEXTURE_SIZE_X			(800)
#define TEXTURE_SIZE_Y			(400)
#define TEXTURE_BG_SIZE_X		(1600)
#define TEXTURE_BG_SIZE_Y		(720)
#define MAX_FOREST_BG			(1)
#define MAX_VOLCANO_BG			(4)
#define MAX_SEA_BG				(3)
#define MAX_BACKGROUND			(MAX_FOREST_BG+MAX_VOLCANO_BG+MAX_SEA_BG)

//-----------------------------------------------------------------------------------
//　列挙型
//-----------------------------------------------------------------------------------

enum BackGroundType
{
	TYPE_FIRST,			// 一番目に描画
	TYPE_SECOND,		// 二番目に描画
	TYPE_THIRD,			// 三番目に描画
	TYPE_FOURTH			// 四番目に描画
};

//-----------------------------------------------------------------------------------
//　プロトタイプ宣言
//-----------------------------------------------------------------------------------
HRESULT InitBackGround(LPDIRECT3DDEVICE9 pDevice);
void UninitBackGround(void);
void UpdateBackGround(void);
void DrawBackGround(LPDIRECT3DDEVICE9 pDevice);
