#ifndef ___POLYGONANIM_H___
#define ___POLYGONANIM_H___

// ポリゴンのアニメーション用データ
// 2013.07.23

#include "main.h"

// 共通設定
typedef struct _tAnimKeyData
{
	int		frame;	// 変更フレーム
	RECT	pos;	// テクスチャ座標
	int		flag;	// フラグ(ループなど)
}tAnimKeyData;

#define ANIM_FLAG_NONE	(0x00)	// フラグなし
#define ANIM_FLAG_LOOP	(0x01)	// ループする（先頭へ）
#define ANIM_FLAG_STOP	(0x02)	// 停止する

// TickTack
extern const D3DXVECTOR2 c_sizePlayer00;
extern const tAnimKeyData* c_animPlayer00[];
extern const D3DXVECTOR2 c_sizePlayer01;
extern const tAnimKeyData* c_animPlayer01[];

// 背景

extern const D3DXVECTOR2 c_sizeBackGround00;
extern const tAnimKeyData* c_animBackGround00[];

// 風
extern const D3DXVECTOR2 c_sizeWind00;
extern const tAnimKeyData* c_animWind00[];

// ポーズ画面
extern const D3DXVECTOR2 c_sizePauseMenu00;
extern const tAnimKeyData* c_animPauseMenu00[];

// クリアーメニュー画面
extern const D3DXVECTOR2 c_sizeClearMenu00;
extern const tAnimKeyData* c_animClearMenu00[];

// タイトル
extern const D3DXVECTOR2 c_sizeTitle00;
extern const tAnimKeyData* c_animTitle00[];

// 鳥
extern const D3DXVECTOR2 c_sizeBird00;
extern const tAnimKeyData* c_animBird00[];

// セレクト画面
extern const D3DXVECTOR2 c_sizeSelect00;
extern const tAnimKeyData* c_animSelect00[];


enum	TickTack
{
	ANIM_MOVE_RIGHT,
	ANIM_MOVE_LEFT,
	ANIM_MOVE_UP,

	// ギミックに対する
	ANIM_GIM_PIPE,
	ANIM_GIM_PIPEWAIT,
};

enum PauseMenu
{
	PAUSE_BG,
	PAUSE_FRAME,
	PAUSE_RETIRE,
	PAUSE_BACK,
	PAUSE_SET_VALVE,
	PAUSE_VALVE,
	PAUSE_NOT_SET_VALVE,
};

enum ClearMenu
{
	CLEAR_BG,
	CLEAR_LOGO,
	CLEAR_SELECT,
	CLEAR_NEXT,
};


#endif