//=============================================================================
//
// テクスチャー読み込み処理
//
//=============================================================================
#include "TextureBase.h"
//#include "TextureFont.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tTextureCache	g_TextureCache[TEXTURE_MAXNUM];

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT InitTexture( LPDIRECT3DDEVICE9 pDevice ){
	//InitTextureFont( pDevice );

	int file_num = 0;
	HRESULT	hr;
	std::string dir_path = TEXTURE_DIRECTORY_PATH;		// 画像ファイルのフォルダ位置を取得
	std::string find_path = dir_path + _T("*");			// 探索用の任意文字列付加
	std::string file_name;
	
	WIN32_FIND_DATA	w32_FindData;	// 探索したファイルのデータ

	// オープン処理
	HANDLE	hFind = FindFirstFile( find_path.c_str(), &w32_FindData );
	
	while(1){
		// 探索したファイルの種類を判定
		if( w32_FindData.dwFileAttributes != FILE_ATTRIBUTE_DIRECTORY ){
			// ディレクトリでない場合
			file_name = w32_FindData.cFileName;

			// 文字列".png"の検索、拡張子判定
			if( file_name.find( _T(".png"), 0 ) != std::string::npos ){
				// ".png"が含まれている場合
				// テクスチャの読み込み
				hr = D3DXCreateTextureFromFile(
						pDevice,							// デバイスのポインタ
						( dir_path + file_name ).c_str(),	// ファイルパス
						&g_TextureCache[file_num].pD3DTexture		// 読み込むメモリのポインタ
						);

				if(FAILED(hr)){
					MessageBox(NULL,
						_T("テクスチャ読み込みエラー"),
						_T("error"),MB_OK);

					FindClose( hFind );
					return E_FAIL;
				}

				// アクセス用テクスチャファイル名の設定
				g_TextureCache[file_num].file_name = file_name;

				// ロードしたファイル数加算
				if( ++file_num >= TEXTURE_MAXNUM ){
					MessageBox(NULL,
						_T("テクスチャロード数が上限を超えています"),
						_T("warning"),MB_OK);
					break;
				}
			}
		}

		// ファイルが存在する間ループ
		if( !FindNextFile( hFind, &w32_FindData ) ){
			// 余った分はNULL
			for(int i = file_num + 1; i<TEXTURE_MAXNUM; i++){
				g_TextureCache[i].pD3DTexture = NULL;
			}
			break;
		}
	}

	// クローズ処理
	FindClose( hFind );

	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void UninitTexture(void){
	for( int i=0; i<TEXTURE_MAXNUM; i++ ){
		SAFE_RELEASE( g_TextureCache[i].pD3DTexture );
	}

	//UninitTextureFont();
}

//=============================================================================
// ロード済みテクスチャへのポインタ取得
//=============================================================================
LPDIRECT3DTEXTURE9 GetTexturePoint( const TCHAR* file_name ){
	for( int i=0; i<TEXTURE_MAXNUM; i++ ){
		tTextureCache& cache = g_TextureCache[i];

		if( cache.file_name == file_name ){
			return cache.pD3DTexture;
		}
	}

	return NULL;

	// 指定されたファイルが見つからなかった場合は、フォントテクスチャの指定として扱う
	//return GetFontTexturePoint( file_name );
}