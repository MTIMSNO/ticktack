//=============================================================================
//
// ベースプログラム [main.cpp]
//
//=============================================================================
#include "main.h"
#include "input.h"
#include "Fade.h"
#include "Player.h"
#include "StageBase.h"
#include "BlockLand.h"
#include "BlockWind.h"
#include "BlockRetry.h"
//#include "BlockPipe.h"
#include "BlockFlower.h"
#include "BlockVine.h"
#include "BlockTree.h"
#include "BlockGoal.h"
#include "BlockSpiderNet.h"
#include "BlockCheckPoint.h"
#include "BlockBird.h"
#include "TextureBase.h"
#include "BackGround.h"
#include "GimmickData.h"
#include "BlockMove.h"
#include "PauseMenu.h"
#include "ClearMenu.h"
#include "Data.h"
#include "Sound.h"
#include "Title.h"

#include "SelectStage.h"
//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define CLASS_NAME			_T("AppClass")				// ウインドウのクラス名
#define WINDOW_NAME			_T("チック タック")			// ウインドウのキャプション名

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HRESULT Init(HWND hWnd, BOOL bWindow);
void Uninit(void);
void Update(void);
void Draw(void);

// あたり判定
void CheckHit(void);
// あたり判定(BB)
bool CheckHitBB ( D3DXVECTOR3 pos1, D3DXVECTOR2 size1, D3DXVECTOR3 pos2, D3DXVECTOR2 size2 );


#ifdef _DEBUG
void DrawFPS(void);
#endif

//*****************************************************************************
// グローバル変数:
//*****************************************************************************
LPDIRECT3D9				g_pD3D = NULL;				// Direct3Dオブジェクト
LPDIRECT3DDEVICE9		g_pD3DDevice = NULL;		// Deviceオブジェクト(描画に必要)

#ifdef _DEBUG
LPD3DXFONT				g_pD3DXFont = NULL;			// フォントへのポインタ
int						g_nCountFPS;				// FPSカウンタ
#endif

//=============================================================================
// メイン関数
//=============================================================================
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);	// 無くても良いけど、警告が出る（未使用宣言）
	UNREFERENCED_PARAMETER(lpCmdLine);		// 無くても良いけど、警告が出る（未使用宣言）

	// 時間計測用
	DWORD dwExecLastTime;
	DWORD dwFPSLastTime;
	DWORD dwCurrentTime;
	DWORD dwFrameCount;

	WNDCLASSEX	wcex = {
		sizeof(WNDCLASSEX),
		CS_CLASSDC,
		WndProc,
		0,
		0,
		hInstance,
		NULL,
		LoadCursor(NULL, IDC_ARROW),
		(HBRUSH)(COLOR_WINDOW+1),
		NULL,
		CLASS_NAME,
		NULL
	};
	HWND		hWnd;
	MSG			msg;
	
	// ウィンドウクラスの登録
	RegisterClassEx(&wcex);

	// ウィンドウの作成
	hWnd = CreateWindow(CLASS_NAME,
						WINDOW_NAME,
						WS_OVERLAPPEDWINDOW,
						CW_USEDEFAULT,																		// ウィンドウの左座標
						CW_USEDEFAULT,																		// ウィンドウの上座標
						SCREEN_WIDTH + GetSystemMetrics(SM_CXDLGFRAME)*2,									// ウィンドウ横幅
						SCREEN_HEIGHT + GetSystemMetrics(SM_CXDLGFRAME)*2+GetSystemMetrics(SM_CYCAPTION),	// ウィンドウ縦幅
						NULL,
						NULL,
						hInstance,
						NULL);

	// DirectXの初期化(ウィンドウを作成してから行う)
	if(FAILED(Init(hWnd, true)))
	{
		return -1;
	}

	// 入力処理の初期化
	InitInput(hInstance, hWnd);

	// フレームカウント初期化
	timeBeginPeriod( 1 );	// 分解能を設定
	dwExecLastTime =
	dwFPSLastTime =	timeGetTime();	// システム時刻をミリ秒単位で取得
	dwCurrentTime =
	dwFrameCount =	0;

	// ウインドウの表示(Init()の後に呼ばないと駄目)
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	
	// メッセージループ
	while(1)
	{
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
			{// PostQuitMessage()が呼ばれたらループ終了
				break;
			}
			else
			{
				// メッセージの翻訳とディスパッチ
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
        }
		else
		{
			dwCurrentTime = timeGetTime();					// システム時刻を取得

			if( ( dwCurrentTime - dwFPSLastTime ) >= 500 )	// 0.5秒ごとに実行
			{
#ifdef _DEBUG
				g_nCountFPS = ( dwFrameCount * 1000 ) / ( dwCurrentTime - dwFPSLastTime );	// FPSを測定
#endif
				dwFPSLastTime = dwCurrentTime;				// FPSを測定した時刻を保存
				dwFrameCount = 0;							// カウントをクリア
			}

			if( ( dwCurrentTime - dwExecLastTime ) >= (1000/60) )	// 1/60秒ごとに実行
			{
				dwExecLastTime = dwCurrentTime;	// 処理した時刻を保存

				// 更新処理
				Update();

				// 描画処理
				Draw();

				dwFrameCount++;		// 処理回数のカウントを加算
			}
		}
	}
	
	// ウィンドウクラスの登録を解除
	UnregisterClass(CLASS_NAME, wcex.hInstance);

	// 終了処理
	Uninit();

	// 入力処理の終了処理
	UninitInput();

	return (int)msg.wParam;
}

//=============================================================================
// プロシージャ
//=============================================================================
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch( message )
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch(wParam)
		{
		case VK_ESCAPE:					// [ESC]キーが押された
			DestroyWindow(hWnd);		// ウィンドウを破棄するよう指示する
			break;
		}
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT Init(HWND hWnd, BOOL bWindow)
{
	D3DPRESENT_PARAMETERS d3dpp;
    D3DDISPLAYMODE d3ddm;

	// Direct3Dオブジェクトの作成
	g_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	if(g_pD3D == NULL)
	{
		return E_FAIL;
	}

	// 現在のディスプレイモードを取得
    if(FAILED(g_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)))
	{
		return E_FAIL;
	}

	// デバイスのプレゼンテーションパラメータの設定
	ZeroMemory(&d3dpp, sizeof(d3dpp));							// ワークをゼロクリア
	d3dpp.BackBufferCount			= 1;						// バックバッファの数
	d3dpp.BackBufferWidth			= SCREEN_WIDTH;				// ゲーム画面サイズ(幅)
	d3dpp.BackBufferHeight			= SCREEN_HEIGHT;			// ゲーム画面サイズ(高さ)
	d3dpp.BackBufferFormat			= D3DFMT_UNKNOWN;			// バックバッファのフォーマットは現在設定されているものを使う
	d3dpp.SwapEffect				= D3DSWAPEFFECT_DISCARD;	// 映像信号に同期してフリップする
	d3dpp.Windowed					= bWindow;					// ウィンドウモード
	d3dpp.EnableAutoDepthStencil	= TRUE;						// デプスバッファ（Ｚバッファ）とステンシルバッファを作成
	d3dpp.AutoDepthStencilFormat	= D3DFMT_D24S8;				// デプスバッファとして24bit、ステンシルバッファとして8bitを使う
	d3dpp.BackBufferFormat			= d3ddm.Format;				// カラーモードの指定

	if(bWindow)
	{// ウィンドウモード
		d3dpp.BackBufferFormat           = D3DFMT_UNKNOWN;					// バックバッファ
		d3dpp.FullScreen_RefreshRateInHz = 0;								// リフレッシュレート
		d3dpp.PresentationInterval       = D3DPRESENT_INTERVAL_IMMEDIATE;	// インターバル
	}
	else
	{// フルスクリーンモード
		d3dpp.BackBufferFormat           = D3DFMT_R5G6B5;					// バックバッファ
		d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;			// リフレッシュレート
		d3dpp.PresentationInterval       = D3DPRESENT_INTERVAL_DEFAULT;		// インターバル
	}

	// デバイスの生成
	// ディスプレイアダプタを表すためのデバイスを作成
	// 描画と頂点処理をハードウェアで行なう
	if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT,							// ディスプレイアダプタ
									D3DDEVTYPE_HAL,								// ディスプレイタイプ
									hWnd,										// フォーカスするウインドウへのハンドル
									D3DCREATE_HARDWARE_VERTEXPROCESSING,		// デバイス作成制御の組み合わせ
									&d3dpp,										// デバイスのプレゼンテーションパラメータ
									&g_pD3DDevice)))							// デバイスインターフェースへのポインタ
	{
		// 上記の設定が失敗したら
		// 描画をハードウェアで行い、頂点処理はCPUで行なう
		if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, 
										D3DDEVTYPE_HAL, 
										hWnd, 
										D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
										&d3dpp,
										&g_pD3DDevice)))
		{
			// 上記の設定が失敗したら
			// 描画と頂点処理をCPUで行なう
			if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, 
											D3DDEVTYPE_REF,
											hWnd, 
											D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
											&d3dpp,
											&g_pD3DDevice)))
			{
				// 初期化失敗
				return E_FAIL;
			}
		}
	}

	//-------- ここから３Ｄ関連
	// ライトを有効にする
	g_pD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	// アンビエントライト（環境光）を設定する
	g_pD3DDevice->SetRenderState(D3DRS_AMBIENT, 0xffffffff);
	// スペキュラ（鏡面反射）を有効にする
	g_pD3DDevice->SetRenderState(D3DRS_SPECULARENABLE, TRUE);

	// ライトをあてる（白色で鏡面反射ありに設定）
	D3DXVECTOR3 vecDirection(0, 0, 1);			// 光の向きを決める
	D3DLIGHT9 light = {D3DLIGHT_DIRECTIONAL};	// ライトオブジェクト
	light.Diffuse.r  = 1.0f;
	light.Diffuse.g  = 1.0f;
	light.Diffuse.b  = 1.0f;
	light.Specular.r = 0.0f;
	light.Specular.g = 0.0f;
	light.Specular.b = 0.0f;
	D3DXVec3Normalize((D3DXVECTOR3*)&light.Direction, &vecDirection);
	g_pD3DDevice->SetLight(0, &light);
	g_pD3DDevice->LightEnable(0, TRUE);			// ライト０を有効

	// プロジェクショントランスフォーム（射影変換）
	D3DXMATRIXA16 matProj;
	D3DXMatrixPerspectiveFovLH(&matProj, D3DXToRadian(45), ASPECT_RATIO, NEAR_CLIP, FAR_CLIP);
	g_pD3DDevice->SetTransform(D3DTS_PROJECTION, &matProj);

	// ビュートランスフォーム（視点座標変換）
	D3DXVECTOR3 vecEyePt(   0.0f, 0.0f, -1000.0f);	// カメラ（視点）位置
	D3DXVECTOR3 vecLookatPt(0.0f, 0.0f,  0.0f);	// 注視位置
	D3DXVECTOR3 vecUpVec(   0.0f, 1.0f,  0.0f);	// 上方位置
	D3DXMATRIXA16 matView;
	D3DXMatrixLookAtLH( &matView, &vecEyePt, &vecLookatPt, &vecUpVec );
	g_pD3DDevice->SetTransform( D3DTS_VIEW, &matView );

	//------- ここまで

	// レンダリングステートパラメータの設定
    g_pD3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);				// カリングを行わない
	g_pD3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);						// Zバッファを使用
	g_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);				// αブレンドを行う
	g_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);		// αソースカラーの指定
	g_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	// αデスティネーションカラーの指定

	//透明色部分を正しく表示する設定
	g_pD3DDevice->SetRenderState ( D3DRS_ALPHATESTENABLE , TRUE );
	g_pD3DDevice->SetRenderState ( D3DRS_ALPHAFUNC , D3DCMP_GREATER );

	// サンプラーステートパラメータの設定
	g_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);	// テクスチャＵ値の繰り返し設定
	g_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);	// テクスチャＶ値の繰り返し設定
	g_pD3DDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);	// テクスチャ拡大時の補間設定
	g_pD3DDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);	// テクスチャ縮小時の補間設定

#ifdef _DEBUG
	// 情報表示用フォントを設定
	D3DXCreateFont( g_pD3DDevice, 18, 0, 0, 0, FALSE, SHIFTJIS_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Terminal"), &g_pD3DXFont );
#endif

	// サウンドの初期化
	InitSound(hWnd);

	// テクスチャーロード
	InitTexture( g_pD3DDevice );

	// データの初期化
	InitData();

	// フェードポリゴンの初期化
	InitFade( g_pD3DDevice );

	// Tweenの初期化処理
	InitTween();

	// プレイヤーの初期化処理
	//InitMeshPlayer(g_pD3DDevice);

	// プレイヤーの初期化処理
	InitPlayer(g_pD3DDevice);

	// ステージの初期化処理
	InitStage(g_pD3DDevice);

	// ブロックの初期化はここへ
	{
		// 木の初期化
		InitBlockTree();

		// ステージ_パイプブロック
		InitBlockCheckPoint();

		// ステージ_地形ブロックの初期化
		InitBlockLand();

		// ステージ_やり直しブロックの初期化
		InitBlockRetry();

		// ステージ_風ブロックの初期化
		InitBlockWind();

		// ステージ_フラワーの初期化
		InitBlockFlower();

		// ステージ_つるの初期化
		InitBlockVine();

		// 移動ブロックの初期化
		InitBlockMove();

		// ゴールブロックの初期化
		InitBlockGoal();

		// 蜘蛛の巣
		InitBlockSpiderNet(g_pD3DDevice);

		// 鳥の初期化
		InitBlockBird(g_pD3DDevice);
	}


	// ステージ開始時処理の仮設定
	PreStartStage( 1, 1 );

	// 背景の初期化処理
	InitBackGround(g_pD3DDevice);

	// ポーズ画面の初期化処理
	InitPauseMenu(g_pD3DDevice);

	// クリアー画面の初期化処理
	InitClearMenu(g_pD3DDevice);

	// セレクト画面の初期化処理
	InitSelectStage(g_pD3DDevice);

	// タイトル画面の初期化処理
	InitTitle(g_pD3DDevice);

	

	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void Uninit(void)
{
	// メッシュの終了処理
	//UninitMeshPlayer();

	// プレイヤーの終了処理
	UninitPlayer();

	// ステージの終了処理
	UninitStage();

	// 背景の終了処理
	UninitBackGround();

	// ポーズ画面の終了処理
	UninitPauseMenu();

	// クリアメニュー画面の処理
	UninitClearMenu();

	// フェードポリゴンの終了
	UninitFade();

	// タイトルの終了処理
	UninitTitle();

	// テクスチャーの終了処理
	UninitTexture();

	// サウンドの終了処理
	UninitSound();

#ifdef _DEBUG
	if( g_pD3DXFont != NULL ) 
	{	// 情報表示用フォントの開放
		g_pD3DXFont->Release();
		g_pD3DXFont = NULL;
	}
#endif

	if(g_pD3DDevice != NULL)
	{// デバイスの開放
		g_pD3DDevice->Release();
	}

	if(g_pD3D != NULL)
	{// Direct3Dオブジェクトの開放
		g_pD3D->Release();
	}
}

//=============================================================================
// 更新処理
//=============================================================================
void Update(void)
{
	// 入力の更新処理
	UpdateInput();

	// フェードポリゴンの更新
	UpdateFade();

	switch ( GetData(DATA_MAINTRANSITION) )
	{
	case STATE_TITLE:
		UpdateTitle();
		break;

	case STATE_SELECTMENU:
		// 背景の更新処理
		UpdateBackGround();
		// セレクト画面
		UpdateSelectStage();
		break;

	case STATE_GAME:
		// ポーズ画面の更新処理
		if (!(GetClearFlag()))		UpdatePauseMenu();
		// クリア画面の更新処理
		if (!(GetPauseFlag()))		UpdateClearMenu();

		if (!(GetPauseFlag()) && !(GetClearFlag()))
		{
			// プレイヤーの更新処理
			UpdatePlayer();
			// ステージの更新処理
			UpdateStage();
			// 背景の更新処理
			UpdateBackGround();
		}
		break;
	}
}

//=============================================================================
// 描画処理
//=============================================================================
void Draw(void)
{
	// バックバッファ＆Ｚバッファのクリア
	g_pD3DDevice->Clear(0, NULL, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(128, 128, 255, 0), 1.0f, 0);

	// Direct3Dによる描画の開始
	if(SUCCEEDED(g_pD3DDevice->BeginScene()))
	{
		// 2Dポリゴンを3Dポリゴンより奥に描く場合はここ

		// Zバッファだけクリア
		g_pD3DDevice->Clear(0, NULL, (D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(128, 128, 255, 0), 1.0f, 0);

		switch (GetData(DATA_MAINTRANSITION))
		{
		case STATE_TITLE:
			DrawTitle(g_pD3DDevice);
			break;

		case STATE_SELECTMENU:
			DrawBackGround(g_pD3DDevice);
			DrawSelectStage(g_pD3DDevice);
			//DrawSelectStageInfo();
			break;

		case STATE_GAME:
			// 背景の描画処理
			DrawBackGround(g_pD3DDevice);
			// ステージの描画処理
			DrawStage(g_pD3DDevice);
			// プレイヤーの描画処理
			DrawPlayer(g_pD3DDevice);
			// 蜘蛛の巣の描画
			DrawSpiderNet(g_pD3DDevice);
			// ポーズ画面の描画
			DrawPauseMenu(g_pD3DDevice);
			// クリアー画面の描画処理
			DrawClearMenu(g_pD3DDevice);
			break;
		}

		// フェードポリゴン
		DrawFade( g_pD3DDevice );


#ifdef _DEBUG
		// FPS表示
		DrawFPS();
#endif

		// Direct3Dによる描画の終了
		g_pD3DDevice->EndScene();
	}

	// バックバッファとフロントバッファの入れ替え
	g_pD3DDevice->Present(NULL, NULL, NULL, NULL);
}

//=============================================================================
// あたり判定
// 各あたり判定はこちらに
//=============================================================================
void CheckHit(void)
{

}

//=============================================================================
//BBによるあたり判定
//pos1:1つ目の中心座標　size1:サイズ
//pos2:2つ目の中心座標　size2:サイズ
//=============================================================================
bool CheckHitBB ( D3DXVECTOR3 pos1, D3DXVECTOR2 size1, D3DXVECTOR3 pos2, D3DXVECTOR2 size2 )
{
	if ( pos1.x + size1.x/2 > pos2.x - size2.x/2	//pos1の右とpos2の左
							&& 
		pos1.x - size1.x/2 < pos2.x + size2.x/2)	//pos1の左とpos2の右
	{
		if ( pos1.y + size1.y/2 > pos2.y - size2.y/2	//pos1の下とpos2の上
								&& 
			pos1.y - size1.y/2 < pos2.y + size2.y/2)	//pos1の上とpos2の下
		{
			return true;
		}
	}
	return false;
}


#ifdef _DEBUG
//=============================================================================
// FPS表示処理
//=============================================================================
void DrawFPS(void)
{
	RECT rect = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };
	TCHAR str[256];

	wsprintf( str, _T("FPS:%d\n"), g_nCountFPS );

	// テキスト描画
	g_pD3DXFont->DrawText( NULL, str, -1, &rect, DT_LEFT, D3DCOLOR_ARGB( 0xff, 0xff, 0xff, 0xff ) );
}
#endif
