//===================================================================================
//
// ポーズ画面
//
//===================================================================================

#include "PauseMenu.h"
#include "PolygonBase.h"
#include "input.h"
#include "BlockCheckPoint.h"
#include "Data.h"
#include "Fade.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
void PauseMenuChange(void);
void PauseMenuTransition(void);
void PauseMenuScaling(void);
void ChangeValveTexture(void);
void PauseMenuFade(void);
void ReInit(void);
//-----------------------------------------------------------------------------------
// 構造体定義
//-----------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

tObjData			g_tPauseMenu[MAX_PAUSEMENU];	
bool				g_bPauseFlag;
bool				g_bBigFlag;
bool				g_bSmallFlag;
int					g_nSelectPauseMenu;
bool				g_bPauseMenuFadeFlag;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitPauseMenu(LPDIRECT3DDEVICE9 pDevice)
{
	int l,j;
	l = j = 0;
	for ( int i = 0; i < MAX_PAUSEMENU; i++ )
	{
		g_tPauseMenu[i].rot = D3DXVECTOR3(0.0f,0.0f,0.0f);
		g_tPauseMenu[i].fScale = 1.0f;
		g_tPauseMenu[i].bExist = true;
		g_tPauseMenu[i].sizeMax = c_sizePauseMenu00;
			
		if ( i == 0 )
		{
			g_tPauseMenu[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y,0.0f);
			g_tPauseMenu[i].size = D3DXVECTOR2((float)MAX_PAUSE_BG_SIZE_X,(float)MAX_PAUSE_BG_SIZE_Y);
			g_tPauseMenu[i].nType = PAUSE_BG;
		}
		else if ( i == 1 )
		{
			g_tPauseMenu[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y-30.0f,0.0f);
			g_tPauseMenu[i].size = D3DXVECTOR2((float)MAX_PAUSE_FRA_SIZE_X,(float)MAX_PAUSE_FRA_SIZE_Y);
			g_tPauseMenu[i].nType = PAUSE_FRAME;
		}
		else if ( i == 2 )
		{
			g_tPauseMenu[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X-SCREEN_CENTER_X/3,(float)SCREEN_CENTER_Y-SCREEN_CENTER_Y/3,0.0f);
			g_tPauseMenu[i].size = D3DXVECTOR2((float)MAX_PAUSE_MENU_X,(float)MAX_PAUSE_MENU_Y);
			g_tPauseMenu[i].fScale = 0.8f;
			g_tPauseMenu[i].nType = PAUSE_RETIRE;
		}
		else if ( i == 3 )
		{
			g_tPauseMenu[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X+SCREEN_CENTER_X/3,(float)SCREEN_CENTER_Y-SCREEN_CENTER_Y/3,0.0f);
			g_tPauseMenu[i].size = D3DXVECTOR2((float)MAX_PAUSE_MENU_X,(float)MAX_PAUSE_MENU_Y);
			g_tPauseMenu[i].nType = PAUSE_BACK;
		}
		else if ( i >= 4 && i <= 9 )
		{
			g_tPauseMenu[i].pos = D3DXVECTOR3((float)START_VALVE+(MAX_PAUSE_VALVE_X*l),(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/15,0.0f);
			g_tPauseMenu[i].size = D3DXVECTOR2((float)MAX_PAUSE_VALVE_X,(float)MAX_PAUSE_VALVE_Y);
			g_tPauseMenu[i].nType = PAUSE_NOT_SET_VALVE;
			l++;
		}
		else if ( i >= 10 && i <= 15 )
		{
			g_tPauseMenu[i].pos = D3DXVECTOR3((float)START_VALVE+(MAX_PAUSE_VALVE_X*j),(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/15+MAX_PAUSE_VALVE_Y,0.0f);
			g_tPauseMenu[i].size = D3DXVECTOR2((float)MAX_PAUSE_VALVE_X,(float)MAX_PAUSE_VALVE_Y);
			g_tPauseMenu[i].nType = PAUSE_NOT_SET_VALVE;
			j++;
		}

		g_tPauseMenu[i].pAnim = c_animPauseMenu00;

		MakePolygon(pDevice,&g_tPauseMenu[i],TEXTURE_PAUSEMENU);
	}

	g_bPauseFlag = false;
	g_bBigFlag = true;
	g_bSmallFlag = false;
	g_nSelectPauseMenu = 3;
	g_bPauseMenuFadeFlag = false;

	return S_OK;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitPauseMenu(void)
{
	for ( int i = 0; i < MAX_PAUSEMENU; i++ )	UninitPolygonAndTexture(&g_tPauseMenu[i]);
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdatePauseMenu(void)
{
	// PAUSE画面の表示
	if ( GetKeyboardTrigger(DIK_P) ) g_bPauseFlag = !(g_bPauseFlag);
	// バルブのテクスチャ切り替え
	ChangeValveTexture();

	
	// メニューを表示していない時はReturn
	if ( g_bPauseFlag != true ) return;

	// メニューの切り替え
	PauseMenuChange();
	// 選択メニューによる遷移
	PauseMenuTransition();
	// 選択メニューの拡縮
	PauseMenuScaling();
	// セレクト画面へ戻る場合
	PauseMenuFade();

	// テクスチャの設定
	for ( int i = 0; i < MAX_PAUSEMENU; i++ )
	{
		SetAnimationNo(&g_tPauseMenu[i],g_tPauseMenu[i].nType);
		SetTexturePolygon(&g_tPauseMenu[i]);
		SetVertexPolygon(&g_tPauseMenu[i]);
	}
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawPauseMenu(LPDIRECT3DDEVICE9 pDevice)
{
	//透明色部分を正しく表示する設定
	// 2Dポリゴンの描画に影響するので描画が終わったら設定を元に戻す
	pDevice->SetRenderState ( D3DRS_ALPHAREF , 0x00000080 );

	for ( int i = 0; i < MAX_PAUSEMENU; i++ )
	{
		if( g_bPauseFlag != true ){
			// D3DRS_ALPHAREFの設定を戻してほしいので、最後のリセット処理まで処理を継続する
			// ※returnだと処理が中断されてしまう
			continue;
		}
		//if( g_bPauseFlag != true ) return;

		// 初期化で設定済み
		//pDevice->SetRenderState ( D3DRS_ALPHATESTENABLE , TRUE );
		//pDevice->SetRenderState ( D3DRS_ALPHAFUNC , D3DCMP_GREATER );

		// 頂点バッファをデバイスのデータストリームにバインド
		pDevice->SetStreamSource( 0, g_tPauseMenu[i].pD3DVtxBuff, 0, sizeof(VERTEX_2D) );

		// 頂点フォーマットの設定
		pDevice->SetFVF(FVF_VERTEX_2D);

		// テクスチャの設定
		pDevice->SetTexture( 0, g_tPauseMenu[i].pD3DTexture );
	
		// ポリゴンの描画
		pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
	}

	// 2Dポリゴンの描画に影響するので描画が終わったら設定を元に戻す
	pDevice->SetRenderState ( D3DRS_ALPHAREF , 0x00000000 );
}

//-----------------------------------------------------------------------------------
// ポーズフラグの取得
//-----------------------------------------------------------------------------------
bool GetPauseFlag(void)
{
	return g_bPauseFlag;
}

//-----------------------------------------------------------------------------------
// ポーズメニュー切り替え
//-----------------------------------------------------------------------------------
void PauseMenuChange(void)
{
	if ( GetKeyboardPress(DIK_RIGHT) )
	{
		g_nSelectPauseMenu = 3;
		g_tPauseMenu[2].fScale = 0.8f;
		SetSound(SOUND_PLAY,SE_DECISION);
	}
	else if ( GetKeyboardPress(DIK_LEFT) )
	{
		g_nSelectPauseMenu = 2;
		g_tPauseMenu[3].fScale = 1.0f;
		SetSound(SOUND_PLAY,SE_DECISION);
	}
}

//-----------------------------------------------------------------------------------
// 選択中のメニューによる遷移
//-----------------------------------------------------------------------------------
void PauseMenuTransition(void)
{
	if ( GetKeyboardTrigger(DIK_RETURN) )
	{
		switch(g_nSelectPauseMenu)
		{
			// リタイア
		case 2:
			g_bPauseMenuFadeFlag = true;
			SetSound(SOUND_PLAY,SE_DECISION);
			break;

			// ゲームへ戻る
		case 3:
			g_bPauseFlag = false;
			SetSound(SOUND_PLAY,SE_DECISION);
			break;
		}
	}
}

//-----------------------------------------------------------------------------------
// ポーズメニューの拡縮
//-----------------------------------------------------------------------------------
void PauseMenuScaling(void)
{
	switch ( g_nSelectPauseMenu )
	{
	case 2:
		if ( g_bBigFlag && g_bSmallFlag == false )
		{
			g_tPauseMenu[g_nSelectPauseMenu].fScale += 0.015f;
			if ( g_tPauseMenu[g_nSelectPauseMenu].fScale >= 0.95f )
			{
				g_bBigFlag = false;
				g_bSmallFlag = true;
			}
		}
		else if ( g_bSmallFlag && g_bBigFlag == false )
		{
			g_tPauseMenu[g_nSelectPauseMenu].fScale -= 0.015f;
			if ( g_tPauseMenu[g_nSelectPauseMenu].fScale <= 0.65f )
			{
				g_bBigFlag = true;
				g_bSmallFlag = false;
			}
		}
		break;

	case 3:
		if ( g_bBigFlag && g_bSmallFlag == false )
		{
			g_tPauseMenu[g_nSelectPauseMenu].fScale += 0.015f;
			if ( g_tPauseMenu[g_nSelectPauseMenu].fScale >= 1.15f )
			{
				g_bBigFlag = false;
				g_bSmallFlag = true;
			}
		}
		else if ( g_bSmallFlag && g_bBigFlag == false )
		{
			g_tPauseMenu[g_nSelectPauseMenu].fScale -= 0.015f;
			if ( g_tPauseMenu[g_nSelectPauseMenu].fScale <= 0.85f )
			{
				g_bBigFlag = true;
				g_bSmallFlag = false;
			}
		}
		break;
	}
}

//-----------------------------------------------------------------------------------
// テクスチャ切り替え
//-----------------------------------------------------------------------------------
void ChangeValveTexture(void)
{
	int MaxNum = GetMaxValve();
	
	for ( int i = 4; i < MaxNum+4; i++)
	{
		g_tPauseMenu[i].nType = PAUSE_VALVE;
	}

	int num = GetManipulatedValve();

	for ( int i = 4; i < num+4; i++ )
	{
		g_tPauseMenu[i].nType = PAUSE_SET_VALVE;
	}
}

//-----------------------------------------------------------------------------------
// ポーズ画面からセレクト画面への遷移
//-----------------------------------------------------------------------------------
void PauseMenuFade(void)
{
	if ( g_bPauseMenuFadeFlag )
	{
		SetFade(FADE_OUT,60,D3DXCOLOR(0.0f,0.0f,0.0f,1.0f),0.0f);	
		g_bPauseMenuFadeFlag = false;
	}
	else if ( g_bPauseMenuFadeFlag == false && GetFadeOutComplete() )
	{
		SetFade(FADE_IN,60,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f),0.0f);
		ReInit();
		SetData(DATA_MAINTRANSITION,STATE_SELECTMENU);
	}
}

//-----------------------------------------------------------------------------------
// 変数の再初期化
//-----------------------------------------------------------------------------------
void ReInit(void)
{
	g_bPauseFlag = false;
	g_bBigFlag = true;
	g_bSmallFlag = false;
	g_nSelectPauseMenu = 3;
	g_bPauseMenuFadeFlag = false;

	int l ,j;
	l = j = 0;
	for(int i=4; i<MAX_PAUSEMENU;i++)
	{		
		if ( i >= 4 && i <= 9 )
		{
			g_tPauseMenu[i].pos = D3DXVECTOR3((float)START_VALVE+(MAX_PAUSE_VALVE_X*l),(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/15,0.0f);
			g_tPauseMenu[i].size = D3DXVECTOR2((float)MAX_PAUSE_VALVE_X,(float)MAX_PAUSE_VALVE_Y);
			g_tPauseMenu[i].nType = PAUSE_NOT_SET_VALVE;
			l++;
		}
		else if ( i >= 10 && i <= 15 )
		{
			g_tPauseMenu[i].pos = D3DXVECTOR3((float)START_VALVE+(MAX_PAUSE_VALVE_X*j),(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/15+MAX_PAUSE_VALVE_Y,0.0f);
			g_tPauseMenu[i].size = D3DXVECTOR2((float)MAX_PAUSE_VALVE_X,(float)MAX_PAUSE_VALVE_Y);
			g_tPauseMenu[i].nType = PAUSE_NOT_SET_VALVE;
			j++;
		}
	}
}