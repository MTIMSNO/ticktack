//=============================================================================
//
// ポリゴン処理 [polygon.h]
// Author : AKIRA TANAKA
//
//=============================================================================
#ifndef ___MESH_H___
#define ___MESH_H___

#include "main.h"

// 構造体定義
typedef struct _tMesh
{
	LPD3DXMESH					pD3DMesh;		// メッシュオブジェクト
	D3DMATERIAL9*				pD3DMaterials;	// マテリアルオブジェクト
	LPDIRECT3DTEXTURE9*			pD3DTextures;	// テクスチャオブジェクト
	DWORD						dwNumMaterials;	// マテリアル数

	D3DXVECTOR3					pos;			// 平行移動量
	D3DXVECTOR3					rot;			// 回転（ラジアン）
	D3DXVECTOR3					scale;			// 拡大縮小(1.0fベース)
}tMesh;

// プロトタイプ宣言
HRESULT InitMesh( LPDIRECT3DDEVICE9 pDevice, tMesh* mesh, TCHAR* xfile );
void UninitMesh( tMesh* mesh );
void UpdateMesh( tMesh* mesh );
void DrawMesh( LPDIRECT3DDEVICE9 pDevice, tMesh* mesh );


#endif