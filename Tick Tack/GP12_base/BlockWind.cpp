#include "BlockWind.h"
#include "Player.h"
#include "BlockSpiderNet.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PreStartBlockWind(void);
void UpdateBlockWind(void);
TCHAR* GetBlockWind_TableFileName(void);

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*		g_Stage_BlockWind[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];

//=============================================================================
// 初期化処理
//=============================================================================
void InitBlockWind(void){
	LinkStage_CellArray( g_Stage_BlockWind, STAGE_LAYER_WIND );

	LinkStage_PreStartFunc( PreStartBlockWind, STAGE_LAYER_WIND );

	LinkStage_UpdateFunc( UpdateBlockWind, STAGE_LAYER_WIND );

	LinkStage_TableFunc( GetBlockWind_TableFileName, STAGE_LAYER_WIND );

	SetStageLayerAnimation( STAGE_LAYER_WIND, c_sizeWind00, c_animWind00, "風アニメーション.png" );
}

//=============================================================================
// ステージ開始時の処理
//=============================================================================
void PreStartBlockWind(void){
	for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
		for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
			g_Stage_BlockWind[i][j]->obj.bExist = false;
			SetAnimationNo( &g_Stage_BlockWind[i][j]->obj, g_Stage_BlockWind[i][j]->type );
		}
	}
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockWind(void){
	int tmpPNum = GetPlayerNumber();
	if( tmpPNum != NAME_TACK ){
		// チックへ切り替わった瞬間に風ブロック消去
		if( GetPlayerChange() ){
			for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
				for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
					g_Stage_BlockWind[i][j]->obj.bExist = false;
				}
			}
		}

		// タックでなければ風処理はしない
		return;
	}

	// タックへ切り替わった瞬間に風ブロック可視化
	if( GetPlayerChange() ){
		for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
			for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
				if( g_Stage_BlockWind[i][j]->type > 0 ){
					g_Stage_BlockWind[i][j]->obj.bExist = true;
				}
			}
		}
	}

	RECT rect;

	// 可視範囲の風ブロックアニメを進行
	GetStageRect_Disp( &rect );
	for(int i=rect.top; i<rect.bottom; i++){
		for(int j=rect.left; j<rect.right; j++){
			if( g_Stage_BlockWind[i][j]->obj.nAnimNo > 0 ){
				Process_PolygonTexAnimation( &g_Stage_BlockWind[i][j]->obj );
			}
		}
	}

	// プレイヤーの当たり判定取得
	const tCollisionData Col_P = GetPlayerCollision( tmpPNum );
	// プレイヤー当たり判定内のセル範囲を取得
	GetStageRect_AroundPlayer( &rect, D3DXVECTOR3( Col_P.radius, Col_P.radius, 0.0f ) );

	// 判定内にあるオブジェクトをチェック
	for(int i=rect.top; i<rect.bottom; i++){
		for(int j=rect.left; j<rect.right; j++){
			D3DXVECTOR3 tmpSpeed(0.0f,0.0f,0.0f);
			const tStageCell& tmpObj = *g_Stage_BlockWind[i][j];
			switch( tmpObj.type ){
			case 2:
				if( CheckCollision( Col_P, tmpObj.collision ) ){
					tmpSpeed.y += BLOCKWIND_POWER_UP;
					SetPlayerSpeed( tmpSpeed );
					SetDeleteSpiderNet();
					SetSound(SOUND_PLAY,SE_WIND);
				}
				break;
			case 4:
				if( CheckCollision( Col_P, tmpObj.collision ) ){
					tmpSpeed.x += BLOCKWIND_POWER_LEFT;
					SetPlayerSpeed( tmpSpeed );
					SetDeleteSpiderNet();
					SetSound(SOUND_PLAY,SE_WIND);
				}
				break;
			case 6:
				if( CheckCollision( Col_P, tmpObj.collision ) ){
					tmpSpeed.x += BLOCKWIND_POWER_RIGHT;
					SetPlayerSpeed( tmpSpeed );
					SetDeleteSpiderNet();
					SetSound(SOUND_PLAY,SE_WIND);
				}
				break;
			default:
				break;
			}
		}
	}
}

//=============================================================================
// 配置データファイル名取得
//=============================================================================
TCHAR* GetBlockWind_TableFileName(void){
	return BLOCKWIND_CELLTABLE_FILENAME;
}