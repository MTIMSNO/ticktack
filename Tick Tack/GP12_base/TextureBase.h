//=============================================================================
//
// テクスチャー処理 [texture.h]
//
//=============================================================================
#ifndef ___TEXTURE_H___
#define ___TEXTURE_H___

#include "main.h"
#include "TextureFont.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define TEXTURE_DIRECTORY_PATH	_T("data/TEXTURE/")	// テクスチャフォルダのパス
#define TEXTURE_MAXNUM			(50)				// ロード可能なファイル数

typedef struct _tTextureCache{
	std::string			file_name;
	LPDIRECT3DTEXTURE9	pD3DTexture;
}tTextureCache;

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
HRESULT InitTexture( LPDIRECT3DDEVICE9 pDevice );
void UninitTexture(void);
LPDIRECT3DTEXTURE9 GetTexturePoint( const TCHAR* file_name );

#endif