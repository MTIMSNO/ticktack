//=============================================================================
//
// 当たり判定処理 [CollisionBase.h]
//
//=============================================================================

#pragma once

#include "main.h"

//-----------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------

//*****************************************************************************
// 列挙型定義
//*****************************************************************************
enum{
	COLLISION_HANDLE_CENTER,
	COLLISION_HANDLE_MIDDLETOP,
	COLLISION_HANDLE_MAX
};

enum{
	COLLISION_TYPE_CIRCLE,
	COLLISION_TYPE_BOX,
	COLLISION_TYPE_MAX
};

//*****************************************************************************
// 構造体定義
//*****************************************************************************
typedef struct _tCollisionData{
	int		type;		// COLLISION_TYPE_〜〜
	float	radius;		// 当たり判定中心からの半径
	D3DXVECTOR3	length;	// 当たり判定中心からの長さ（XYZ）

	D3DXVECTOR3	axis[3];	// 分離軸（XYZ）
	int			handle;		// 操作起点
	const D3DXVECTOR3*	pos;	// 当たり判定中心へのポインタ
}tCollisionData;

//-----------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------
bool CheckCollision( const tCollisionData& DATA1, const tCollisionData& DATA2 );
// 当たり判定作成
// pos	: 当たり判定中心座標のポインタ
// type : COLLISION_TYPE_〜〜
// data : 箱の場合はXYZ三軸方向に対する、中心点からの長さ
//		　円の場合は先頭データを半径として反映
void CreateCollision( tCollisionData* data, const D3DXVECTOR3& pos, int type, float data1, float data2=0.0f, float data3=0.0f );
// 当たり判定の回転設定
void SetCollisionRotAxisNormalize( tCollisionData* data, const D3DXVECTOR3& rot );

// OBB軸線分上から、対象点の相対位置を示す係数取得
float GetPosNum_OBBtoPoint( const D3DXVECTOR3& Vec_toPoint, const D3DXVECTOR3& axis, const float length );

// 操作起点設定
void SetCollisionHandle( tCollisionData* data, int type );

