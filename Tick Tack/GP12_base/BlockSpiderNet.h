//=============================================================================
//
// ファイルを追加する場合のサンプル
// ※ヘッダーとソース部分の、「BlockSample」と「BLOCKSAMPLE」を新しい名前に変更してね
//		こっちで操作したいのに出来ないってのがあれば、教えてください
//		ベースの方に関数作ります
//
//=============================================================================

#pragma once

#include "main.h"
#include "StageBase.h"
#include "Sound.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
// 使用するCSVファイル名(ベース部分)を設定する
#define BLOCKSPIDERNET_CELLTABLE_FILENAME	_T("BlockSpiderNet")

#define TEXTURE_SPIDERNET					_T("SpiderNet.png")
#define MAX_SPIDERNET						(30)
#define SPIDERNET_SIZE						(128)

//*****************************************************************************
// マクロ定義
//*****************************************************************************

enum SPIDERNET
{
	SPIDER_NONE = 0,	// 何もなし
	SPIDER_NET,			// 蜘蛛の巣
};


//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
// 新しくファイルを作る場合はInit関数のみを、「main.cpp」内になる「InitStage」関数より上側に追加してください
// アップデート・描画・終了処理は、「StageBase.cpp」が勝手に追加します
void InitBlockSpiderNet(LPDIRECT3DDEVICE9 pDevice);

// 描画
void DrawSpiderNet(LPDIRECT3DDEVICE9 pDevice);

// 蜘蛛の巣とのあたり判定
bool CheckHitSpiderNet(const tCollisionData& DATA );
// 蜘蛛の巣の設置
void SetSpiderNet(void);
// 蜘蛛の巣を消すように設定
void SetDeleteSpiderNet(void);
// 蜘蛛を非表示に
void HideSpiderNet(void);


