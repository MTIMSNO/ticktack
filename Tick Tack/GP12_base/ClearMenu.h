//===================================================================================
//
// ポーズ画面
//
//===================================================================================

#include "main.h"
#include "Sound.h"
#include "BlockSpiderNet.h"
#include "BlockGoal.h"
#include "Player.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

#define TEXTURE_CLEARMENU			_T("ClearMenu.png")

#define MAX_CLEARMENU				(4)

#define MAX_CLEAR_BG_SIZE_X			(900)
#define MAX_CLEAR_BG_SIZE_Y			(700)

#define MAX_CLEAR_MENU_X			(150)
#define MAX_CLEAR_MENU_Y			(150)

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
HRESULT InitClearMenu(LPDIRECT3DDEVICE9 pDevice);
void UninitClearMenu(void);
void UpdateClearMenu(void);
void DrawClearMenu(LPDIRECT3DDEVICE9 pDevice);
bool GetClearFlag(void);
void SetClearFlag(bool flag);
