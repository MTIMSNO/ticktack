//===================================================================================
//
// データ
//
//===================================================================================

#include "Data.h"


//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

tVariousData	tData;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
void InitData(void)
{
	tData.nMainTransition = STATE_TITLE;
	// 1-1のステージに設定
	tData.nSelectMain = 1;
	tData.nSelectSub = 1;
}

//-----------------------------------------------------------------------------------
// 情報の取得
//-----------------------------------------------------------------------------------
int GetData(int type)
{
	switch (type)
	{
	case DATA_MAINTRANSITION:
		return tData.nMainTransition;

	case DATA_SELECTMAIN:
		return tData.nSelectMain;

	case DATA_SELECTSUB:
		return tData.nSelectSub;
	}

	return -1;
}

//-----------------------------------------------------------------------------------
// 情報の設定
//-----------------------------------------------------------------------------------
void SetData(int type, int data)
{
	switch (type)
	{
	case DATA_MAINTRANSITION:
		tData.nMainTransition = data;
		break;

	case DATA_SELECTMAIN:
		tData.nSelectMain = data;
		break;

	case DATA_SELECTSUB:
		tData.nSelectSub = data;
		break;
	}
}