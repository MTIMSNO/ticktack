//===================================================================================
//
// クリア画面
//
//===================================================================================

#include "ClearMenu.h"
#include "PolygonBase.h"
#include "input.h"
#include "BlockCheckPoint.h"
#include "Data.h"
#include "Fade.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
void ClearMenuChange(void);
void ClearMenuTransition(void);
void ClearMenuScaling(void);
void ClearMenuFade(void);
void ReInitClear(void);
int NextStage(int MainNum,int SubNum,int GetStage);
//-----------------------------------------------------------------------------------
// 構造体定義
//-----------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

tObjData			g_tClearMenu[MAX_CLEARMENU];	
bool				g_bClearFlag;
bool				g_bClearBigFlag;
bool				g_bClearSmallFlag;
int					g_nSelectClearMenu;
bool				g_bClearMenuFadeFlag;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitClearMenu(LPDIRECT3DDEVICE9 pDevice)
{
	int l,j;
	l = j = 0;
	for ( int i = 0; i < MAX_CLEARMENU; i++ )
	{
		g_tClearMenu[i].rot = D3DXVECTOR3(0.0f,0.0f,0.0f);
		g_tClearMenu[i].fScale = 1.0f;
		g_tClearMenu[i].bExist = true;
		g_tClearMenu[i].sizeMax = c_sizeClearMenu00;
		g_tClearMenu[i].size = D3DXVECTOR2((float)MAX_CLEAR_MENU_X,(float)MAX_CLEAR_MENU_Y);

			
		if ( i == 0 )
		{
			g_tClearMenu[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y,0.0f);
			g_tClearMenu[i].size = D3DXVECTOR2((float)MAX_CLEAR_BG_SIZE_X,(float)MAX_CLEAR_BG_SIZE_Y);
			g_tClearMenu[i].nType = CLEAR_BG;
		}
		else if ( i == 1 )
		{
			g_tClearMenu[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y-20.0f,0.0f);
			g_tClearMenu[i].size = D3DXVECTOR2((float)MAX_CLEAR_MENU_X*1.5f,(float)MAX_CLEAR_MENU_Y*1.5f);
			g_tClearMenu[i].nType = CLEAR_LOGO;
		}
		else if ( i == 2 )
		{
			g_tClearMenu[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X-SCREEN_CENTER_X/3,(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/4,0.0f);
			g_tClearMenu[i].nType = CLEAR_SELECT;
		}
		else if ( i == 3 )
		{
			g_tClearMenu[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X+SCREEN_CENTER_X/3,(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/4,0.0f);
			g_tClearMenu[i].nType = CLEAR_NEXT;
		}


		g_tClearMenu[i].pAnim = c_animClearMenu00;

		MakePolygon(pDevice,&g_tClearMenu[i],TEXTURE_CLEARMENU);
	}

	g_bClearFlag = false;
	g_bClearBigFlag = true;
	g_bClearSmallFlag = false;
	g_nSelectClearMenu = 3;
	g_bClearMenuFadeFlag = false;

	return S_OK;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitClearMenu(void)
{
	for ( int i = 0; i < MAX_CLEARMENU; i++ )	UninitPolygonAndTexture(&g_tClearMenu[i]);
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateClearMenu(void)
{
	// CLEAR画面の表示
	//if ( GetKeyboardTrigger(DIK_C)) g_bClearFlag = !(g_bClearFlag);
	if (GetGoalFlag() != false) g_bClearFlag = true;
	
	// メニューを表示していない時はReturn
	if ( g_bClearFlag != true ) return;

	// メニューの切り替え
	ClearMenuChange();
	// 選択メニューによる遷移
	ClearMenuTransition();
	// 選択メニューの拡縮
	ClearMenuScaling();
	// セレクト画面へ戻る場合
	ClearMenuFade();

	// テクスチャの設定
	for ( int i = 0; i < MAX_CLEARMENU; i++ )
	{
		SetAnimationNo(&g_tClearMenu[i],g_tClearMenu[i].nType);
		SetTexturePolygon(&g_tClearMenu[i]);
		SetVertexPolygon(&g_tClearMenu[i]);
	}
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawClearMenu(LPDIRECT3DDEVICE9 pDevice)
{
	//透明色部分を正しく表示する設定
	// 2Dポリゴンの描画に影響するので描画が終わったら設定を元に戻す
	pDevice->SetRenderState ( D3DRS_ALPHAREF , 0x00000080 );

	for ( int i = 0; i < MAX_CLEARMENU; i++ )
	{
		if( g_bClearFlag != true ){
			// D3DRS_ALPHAREFの設定を戻してほしいので、最後のリセット処理まで処理を継続する
			// ※returnだと処理が中断されてしまう
			continue;
		}

		// 頂点バッファをデバイスのデータストリームにバインド
		pDevice->SetStreamSource( 0, g_tClearMenu[i].pD3DVtxBuff, 0, sizeof(VERTEX_2D) );

		// 頂点フォーマットの設定
		pDevice->SetFVF(FVF_VERTEX_2D);

		// テクスチャの設定
		pDevice->SetTexture( 0, g_tClearMenu[i].pD3DTexture );
	
		// ポリゴンの描画
		pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
	}

	// 2Dポリゴンの描画に影響するので描画が終わったら設定を元に戻す
	pDevice->SetRenderState ( D3DRS_ALPHAREF , 0x00000000 );
}

//-----------------------------------------------------------------------------------
// ポーズフラグの取得
//-----------------------------------------------------------------------------------
bool GetClearFlag(void)
{
	return g_bClearFlag;
}

//-----------------------------------------------------------------------------------
// ポーズフラグの取得
//-----------------------------------------------------------------------------------
void SetClearFlag(bool flag)
{
	g_bClearFlag = flag;
}

//-----------------------------------------------------------------------------------
// メニュー切り替え
//-----------------------------------------------------------------------------------
void ClearMenuChange(void)
{
	if ( GetKeyboardPress(DIK_RIGHT) )
	{
		g_nSelectClearMenu = 3;
		g_tClearMenu[2].fScale = 1.0f;
		SetSound(SOUND_PLAY,SE_DECISION);
	}
	else if ( GetKeyboardPress(DIK_LEFT) )
	{
		g_nSelectClearMenu = 2;
		g_tClearMenu[3].fScale = 1.0f;
		SetSound(SOUND_PLAY,SE_DECISION);
	}
}

//-----------------------------------------------------------------------------------
// 選択中のメニューによる遷移
//-----------------------------------------------------------------------------------
void ClearMenuTransition(void)
{
	if ( GetKeyboardTrigger(DIK_RETURN) )
	{
		switch(g_nSelectClearMenu)
		{
			// セレクト画面へ
		case 2:
			g_bClearMenuFadeFlag = true;
			SetSound(SOUND_PLAY,SE_DECISION);
			break;

			// 次のステージへ
		case 3:
			g_bClearMenuFadeFlag = true;
			SetSound(SOUND_PLAY,SE_DECISION);
			break;
		}
	}
}

//-----------------------------------------------------------------------------------
// ポーズメニューの拡縮
//-----------------------------------------------------------------------------------
void ClearMenuScaling(void)
{
	switch ( g_nSelectClearMenu )
	{
	case 2:
		if ( g_bClearBigFlag && g_bClearSmallFlag == false )
		{
			g_tClearMenu[g_nSelectClearMenu].fScale += 0.015f;
			if ( g_tClearMenu[g_nSelectClearMenu].fScale >= 1.15f )
			{
				g_bClearBigFlag = false;
				g_bClearSmallFlag = true;
			}
		}
		else if ( g_bClearSmallFlag && g_bClearBigFlag == false )
		{
			g_tClearMenu[g_nSelectClearMenu].fScale -= 0.015f;
			if ( g_tClearMenu[g_nSelectClearMenu].fScale <= 0.85f )
			{
				g_bClearBigFlag = true;
				g_bClearSmallFlag = false;
			}
		}
		break;

	case 3:
		if ( g_bClearBigFlag && g_bClearSmallFlag == false )
		{
			g_tClearMenu[g_nSelectClearMenu].fScale += 0.015f;
			if ( g_tClearMenu[g_nSelectClearMenu].fScale >= 1.15f )
			{
				g_bClearBigFlag = false;
				g_bClearSmallFlag = true;
			}
		}
		else if ( g_bClearSmallFlag && g_bClearBigFlag == false )
		{
			g_tClearMenu[g_nSelectClearMenu].fScale -= 0.015f;
			if ( g_tClearMenu[g_nSelectClearMenu].fScale <= 0.85f )
			{
				g_bClearBigFlag = true;
				g_bClearSmallFlag = false;
			}
		}
		break;
	}
}

//-----------------------------------------------------------------------------------
// クリアメニュー画面からセレクト画面への遷移
//-----------------------------------------------------------------------------------
void ClearMenuFade(void)
{
	if ( g_bClearMenuFadeFlag )
	{
		SetFade(FADE_OUT,60,D3DXCOLOR(0.0f,0.0f,0.0f,1.0f),0.0f);	
		g_bClearMenuFadeFlag = false;
	}
	else if ( g_bClearMenuFadeFlag == false && GetFadeOutComplete() )
	{
		switch(g_nSelectClearMenu)
		{
		case 2:
			HideSpiderNet();
			ReInitGoal();
			ReInitPlayer();
			SetFade(FADE_IN,60,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f),0.0f);
			SetData(DATA_MAINTRANSITION,STATE_SELECTMENU);
			ReInitClear();
			break;


		case 3:
			HideSpiderNet();
			ReInitGoal();
			ReInitPlayer();
			int MainStage = NextStage(GetData(DATA_SELECTMAIN),GetData(DATA_SELECTSUB),0);
			int SubStage = NextStage(GetData(DATA_SELECTMAIN),GetData(DATA_SELECTSUB),1);
			SetFade(FADE_IN,60,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f),0.0f);
			SetData(DATA_SELECTMAIN,MainStage);
			SetData(DATA_SELECTSUB,SubStage);
			PreStartStage(MainStage,SubStage);
			ReInitClear();
			break;
		}
	}
}

//-----------------------------------------------------------------------------------
// 次のステージの算出
// 第1引数:現在のメインステージ
// 第2引数:現在のサブステージ
// 受け取りたいデータの種類
//	0 --- MAIN / 1 --- SUB
//-----------------------------------------------------------------------------------
int NextStage(int MainNum,int SubNum,int GetStage)
{
	SubNum++;
	if(SubNum > 3)
	{
		MainNum++;
		SubNum = 1;
		if(MainNum > 4)
		{
			MainNum = 4;
			SubNum = 3;
		}
	}

	switch(GetStage)
	{
	case 0:
		return MainNum;

	case 1:
		return SubNum;
	}
	return -1;
}

//-----------------------------------------------------------------------------------
// 変数の再初期化
//-----------------------------------------------------------------------------------
void ReInitClear(void)
{
	g_bClearFlag = false;
	g_bClearBigFlag = true;
	g_bClearSmallFlag = false;
	g_nSelectClearMenu = 3;
	g_bClearMenuFadeFlag = false;
}