//=============================================================================
//
// サウンドベース処理 [sound.h]
//
//=============================================================================
#ifndef ___SOUND_H___
#define ___SOUND_H___

#include <windows.h>
#include <tchar.h>
#include <dsound.h>
#include <mmsystem.h>

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define MAX_SOUND	(5)

//*****************************************************************************
// 列挙型定義
//*****************************************************************************
enum SOUND
{
	// BGM
	BGM_GAME,

	// SE
	SE_DECISION,
	SE_VALVE,
	SE_SPIDERNET,
	SE_WIND,
};

enum
{	// 再生用フラグ
	E_DS8_FLAG_NONE,
	E_DS8_FLAG_LOOP,	// DSBPLAY_LOOPING=1
	E_DS8_FLAG_MAX
};

enum 
{
	SOUND_PLAY,
	SOUND_STOP,
};

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
HRESULT					InitSound( HWND hWnd );	// 初期化
void					UninitSound();			// 後片付
LPDIRECTSOUNDBUFFER8	LoadSound( int no );	// サウンドのロード
void					PlaySound( LPDIRECTSOUNDBUFFER8 pBuffer, int flag/*=0*/ , long Volume );	// 音ごとに再生
bool					IsPlaying(	int num);														// 再生中かどうか
void					StopSound( LPDIRECTSOUNDBUFFER8 pBuffer );										// 再生と止める
void SetSound(int state,int num);

#endif