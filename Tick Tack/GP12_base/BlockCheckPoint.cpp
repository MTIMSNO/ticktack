#include "BlockCheckPoint.h"
#include "Player.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PreStartBlockCheckPoint(void);
void UpdateBlockCheckPoint(void);
TCHAR* GetBlockCheckPoint_TableFileName(void);
void UpdateContent(int i,int j);

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*		g_Stage_BlockCheckPoint[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];
int nMaxValve;


//=============================================================================
// 初期化処理
//=============================================================================
void InitBlockCheckPoint(void){
	// ここで「StageBase.cpp」内で用意した、各マスのポリゴンへのポインタを取得する
	// ※二番目の引数は、「StageBase.h」『列挙型定義』に追加した、『STAGE_LAYER_〜〜』を入れる
	// 　このサンプルでは地形より奥に表示しているので、『STAGE_LAYER_BACK001』を設定してます
	LinkStage_CellArray( g_Stage_BlockCheckPoint, STAGE_LAYER_CHECKPOINT );

	// ここで「StageBase.cpp」から、
	// 「BlockCheckPoint.cpp」内にある「PreStartBlockCheckPoint」「UpdateBlockCheckPoint」「GetBlockCheckPoint_TableFileName」関数にアクセスできるようにする
	// ※関数名を変更した場合は、ここの引数名も変更する
	// 　レイヤー設定は、上と同じものを使う
	LinkStage_PreStartFunc( PreStartBlockCheckPoint, STAGE_LAYER_CHECKPOINT );
	LinkStage_UpdateFunc( UpdateBlockCheckPoint, STAGE_LAYER_CHECKPOINT );
	LinkStage_TableFunc( GetBlockCheckPoint_TableFileName, STAGE_LAYER_CHECKPOINT );
}

//=============================================================================
// ステージ開始時の処理
// ※各ステージの開始時に毎回やって欲しい処理
//=============================================================================
void PreStartBlockCheckPoint(void){
	nMaxValve = 0;
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		if ( g_Stage_BlockCheckPoint[i][j]->type == PIPE_LENGTH_VALVE ||
			 g_Stage_BlockCheckPoint[i][j]->type == PIPE_WIGHT_VALVE )
			 nMaxValve++;
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockCheckPoint(void)
{
}

//=============================================================================
// 配置データファイル名取得
// ※「BlockCheckPoint」と「BLOCKSAMPLE」の部分を、変更してね
//=============================================================================
TCHAR* GetBlockCheckPoint_TableFileName(void){
	return BLOCKCHECKPOINT_CELLTABLE_FILENAME;
}

//=============================================================================
// バルブとのあたり判定
//=============================================================================
bool CheckHitValve(const tCollisionData& DATA )
{
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	{
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			if (!(g_Stage_BlockCheckPoint[i][j]->type == PIPE_LENGTH_VALVE ||
				  g_Stage_BlockCheckPoint[i][j]->type == PIPE_WIGHT_VALVE ))
				continue;

			CreateCollision(&g_Stage_BlockCheckPoint[i][j]->collision,g_Stage_BlockCheckPoint[i][j]->obj.pos,
							COLLISION_TYPE_CIRCLE,0.5f);

			// バルブのとき
			if ( CheckCollision(DATA,g_Stage_BlockCheckPoint[i][j]->collision) )
			{
				UpdateContent(i,j);
				return true;
			}
		}
	}
	return false;
}

//=============================================================================
// 配列の内容更新
//=============================================================================
void UpdateContent(int i,int j)
{
	// バルブに関係ないブロックの場合はリターン
	if ( g_Stage_BlockCheckPoint[i][j]->type < PIPE_LENGTH_VALVE ) return;

	// バルブのとき
	// その向きに応じた操作済みテクスチャに変更
	if ( GetPlayerFlag(0) )
	switch ( g_Stage_BlockCheckPoint[i][j]->type )
	{
	case PIPE_LENGTH_VALVE:
		g_Stage_BlockCheckPoint[i][j]->type = PIPE_LENGTH_SETTLED_VALVE;
		SetStageCellType(STAGE_LAYER_CHECKPOINT,i,j,g_Stage_BlockCheckPoint[i][j]->type);
		break;
		
	case PIPE_WIGHT_VALVE:
		g_Stage_BlockCheckPoint[i][j]->type = PIPE_WIGHT_SETTLED_VALVE;
		SetStageCellType(STAGE_LAYER_CHECKPOINT,i,j,g_Stage_BlockCheckPoint[i][j]->type);
		break;
	}
}

//=============================================================================
// ステージに設定してあるバルブの数を取得
//=============================================================================
int GetMaxValve(void)
{
	return nMaxValve;
}

//=============================================================================
// 操作済みバルブの数の取得
//=============================================================================
int GetManipulatedValve(void)
{
	int nManiVal = 0;
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		if ( g_Stage_BlockCheckPoint[i][j]->type == PIPE_LENGTH_SETTLED_VALVE ||
			 g_Stage_BlockCheckPoint[i][j]->type == PIPE_WIGHT_SETTLED_VALVE )
			 nManiVal++;

	return nManiVal;
}
