#include "BlockSpiderNet.h"
#include "Player.h"
#include <time.h>

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PreStartBlockSpiderNet(void);
void UpdateBlockSpiderNet(void);
void SetVertexSpiderNetPolygon( tObjData* obj );
TCHAR* GetBlockSpiderNet_TableFileName(void);

void DeleteSpiderNet(void);

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*				g_Stage_BlockSpiderNet[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];
tObjData				g_SpiderNet[MAX_SPIDERNET];
D3DXVECTOR3				g_SpiderNetSpeed[MAX_SPIDERNET];
bool					g_bHitSpiderNet;
bool					g_bDeleteSpiderNet;

//=============================================================================
// 初期化処理
// ※起動時に一回だけやって欲しい処理
//=============================================================================
void InitBlockSpiderNet(LPDIRECT3DDEVICE9 pDevice){
	// ここで「StageBase.cpp」内で用意した、各マスのポリゴンへのポインタを取得する
	// ※二番目の引数は、「StageBase.h」『列挙型定義』に追加した、『STAGE_LAYER_〜〜』を入れる
	// 　このサンプルでは地形より奥に表示しているので、『STAGE_LAYER_BACK001』を設定してます
	LinkStage_CellArray( g_Stage_BlockSpiderNet, STAGE_LAYER_SPIDERNET );

	// ここで「StageBase.cpp」から、
	// 「BlockSpiderNet.cpp」内にある「PreStartBlockSpiderNet」「UpdateBlockSpiderNet」「GetBlockSpiderNet_TableFileName」関数にアクセスできるようにする
	// ※関数名を変更した場合は、ここの引数名も変更する
	// 　レイヤー設定は、上と同じものを使う
	LinkStage_PreStartFunc( PreStartBlockSpiderNet, STAGE_LAYER_SPIDERNET );
	LinkStage_UpdateFunc( UpdateBlockSpiderNet, STAGE_LAYER_SPIDERNET );
	LinkStage_TableFunc( GetBlockSpiderNet_TableFileName, STAGE_LAYER_SPIDERNET );

	// あたり判定の作成
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	{
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			CreateCollision(&g_Stage_BlockSpiderNet[i][j]->collision,g_Stage_BlockSpiderNet[i][j]->obj.pos,COLLISION_TYPE_CIRCLE,(float)STAGE_CELL_HEIGHT/25);
		}
	}

	for ( int i = 0; i < MAX_SPIDERNET; i++ )
	{
		g_SpiderNet[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y,0.0f);
		g_SpiderNet[i].rot = D3DXVECTOR3(0.0f,0.0f,0.0f);		
		g_SpiderNet[i].fScale = 1.0f;
		g_SpiderNet[i].sizeMax = D3DXVECTOR2((float)640,(float)602);
		g_SpiderNet[i].size = D3DXVECTOR2((float)SPIDERNET_SIZE,(float)SPIDERNET_SIZE);
		g_SpiderNetSpeed[i] = D3DXVECTOR3(0.0f,0.0f,0.0f);

		// ポリゴン作成
		MakePolygon(pDevice,&g_SpiderNet[i],TEXTURE_SPIDERNET);

		g_SpiderNet[i].bExist = false;
	}

	g_bHitSpiderNet = g_bDeleteSpiderNet = false;
}

//=============================================================================
// ステージ開始時の処理
// ※各ステージの開始時に毎回やって欲しい処理
//=============================================================================
void PreStartBlockSpiderNet(void){
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockSpiderNet(void)
{
	DeleteSpiderNet();
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawSpiderNet(LPDIRECT3DDEVICE9 pDevice)
{
	for ( int i = 0; i < MAX_SPIDERNET; i++ )
	{
		if ( g_SpiderNet[i].bExist == true )
		{
			// 頂点データの修正
			SetVertexPolygon(&g_SpiderNet[i]);
			// テクスチャ設定
			SetTexturePolygon(&g_SpiderNet[i]);

			// 頂点バッファをデバイスのデータストリームにバインド
			pDevice->SetStreamSource( 0, g_SpiderNet[i].pD3DVtxBuff, 0, sizeof(VERTEX_2D) );

			// 頂点フォーマットの設定
			pDevice->SetFVF(FVF_VERTEX_2D);

			// テクスチャの設定
			pDevice->SetTexture( 0, g_SpiderNet[i].pD3DTexture );
	
			// ポリゴンの描画
			pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
		}
	}
}

//=============================================================================
// 配置データファイル名取得
// ※「BlockSpiderNet」と「BLOCKSAMPLE」の部分を、変更してね
//=============================================================================
TCHAR* GetBlockSpiderNet_TableFileName(void){
	return BLOCKSPIDERNET_CELLTABLE_FILENAME;
}

//=============================================================================
// 蜘蛛の巣とのあたり判定
//=============================================================================
bool CheckHitSpiderNet(const tCollisionData& DATA)
{
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	{
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			if ( g_Stage_BlockSpiderNet[i][j]->type != SPIDER_NET )	continue;

			if (CheckCollision(DATA,g_Stage_BlockSpiderNet[i][j]->collision))
			{
				SetSound(SOUND_PLAY,SE_SPIDERNET);
				return true;
			}
		}
	}
	return false;
}

//=============================================================================
// 表示する蜘蛛の巣の選択
//=============================================================================
void SetSpiderNet(void)
{
	// 乱数の初期化
	srand((unsigned int)time(NULL));

	int count = 0;
	for ( int i = 0; i < MAX_SPIDERNET; i++ )
	{
		if ( g_SpiderNet[i].bExist != false ) count++;
		
		if ( count == MAX_SPIDERNET )
		{
			int num = rand()%29 + 0;
			g_SpiderNet[num].bExist = false;
			count = 0;
		}
	}

	for ( int i = 0; i < MAX_SPIDERNET; i++ )
	{
		// 表示してたらコンテニュー
		if ( g_SpiderNet[i].bExist != false) continue;

		// 表示するように設定
		g_SpiderNet[i].bExist = true;
		// 表示するときの中心座標と大きさの設定
		D3DXVECTOR3 pos = D3DXVECTOR3(0.0f,0.0f,0.0f);
		float Scale = (float)(rand()% 35/10 + 2.5f);
		pos.x = (float)(rand()% 750 + 50);
		pos.y = (float)(rand()% 550 + 50);
		g_SpiderNetSpeed[i].x = (float)(rand()% 200/10 + (rand()%15 - 12));
		g_SpiderNetSpeed[i].y = (float)(rand()% 200/10 + (rand()%15 - 11));

		g_SpiderNet[i].fScale = Scale;
		g_SpiderNet[i].pos = pos;

		break;
	}
}

//=============================================================================
// 蜘蛛の巣を消すように設定
//=============================================================================
void SetDeleteSpiderNet(void)
{
	g_bDeleteSpiderNet = true;
}

//=============================================================================
// 表示している蜘蛛の巣を消す
//=============================================================================
void DeleteSpiderNet(void)
{
	if ( g_bDeleteSpiderNet != true ) return;

	for ( int i = 0; i < MAX_SPIDERNET; i++ )
	{
		// 表示していなかったらコンテニュー
		if ( g_SpiderNet[i].bExist != true ) continue;

		// 画面外まで移動しまーす。
		g_SpiderNet[i].pos += g_SpiderNetSpeed[i];

		if ( g_SpiderNet[i].pos.x < 0 - SPIDERNET_SIZE*g_SpiderNet[i].fScale || g_SpiderNet[i].pos.x > SCREEN_WIDTH + SPIDERNET_SIZE*g_SpiderNet[i].fScale ||
			 g_SpiderNet[i].pos.y < 0 - SPIDERNET_SIZE*g_SpiderNet[i].fScale || g_SpiderNet[i].pos.y > SCREEN_HEIGHT + SPIDERNET_SIZE*g_SpiderNet[i].fScale )
		{
			g_SpiderNet[i].bExist = false;
		}
	}

	int count = 0;
	// 全て非表示になったら終了
	for ( int i = 0; i < MAX_SPIDERNET; i++ )
	{
		if ( g_SpiderNet[i].bExist != true )
			count++;

		if ( count == MAX_SPIDERNET )
		{
			g_bDeleteSpiderNet = false;
		}
	}
}

//=============================================================================
// 蜘蛛の巣を非表示に
//=============================================================================
void HideSpiderNet(void)
{
	for(int i = 0; i < MAX_SPIDERNET; i++)
	{
		g_SpiderNet[i].bExist = false;
	}
}