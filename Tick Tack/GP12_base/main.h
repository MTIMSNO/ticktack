//=============================================================================
//
// ベースプログラム [main.h]
//
//=============================================================================
#ifndef ___MAIN_H___
#define ___MAIN_H___

#include "windows.h"
#include "d3dx9.h"
#include <tchar.h>
#include <algorithm>
#include <string>
#include "Tween.h"
#include "function.h"

#define DIRECTINPUT_VERSION 0x0800		// 警告対処
#include "dinput.h"
#include "mmsystem.h"

#if 1	// [ここを"0"にした場合、"構成プロパティ" -> "リンカ" -> "入力" -> "追加の依存ファイル"に対象ライブラリを設定する]
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")
#pragma comment (lib, "winmm.lib")
#endif

#define SCREEN_WIDTH	(800)				// ウインドウの幅
#define SCREEN_HEIGHT	(600)				// ウインドウの高さ
#define SCREEN_CENTER_X	(SCREEN_WIDTH / 2)	// ウインドウの中心Ｘ座標
#define SCREEN_CENTER_Y	(SCREEN_HEIGHT / 2)	// ウインドウの中心Ｙ座標

#define	NUM_VERTEX		(4)					// 頂点数
#define	NUM_POLYGON		(2)					// ポリゴン数

// 3D用
const float	ASPECT_RATIO	= (float)SCREEN_WIDTH/SCREEN_HEIGHT;	// アスペクト比
const float	NEAR_CLIP		= 1.0f;									// ニアクリップを行う距離
const float	FAR_CLIP		= 3000.0f;								// ファークリップを行う距離

#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr) = NULL; } }

// 頂点フォーマット( 頂点座標[2D] / 反射光 / テクスチャ座標 )
#define	FVF_VERTEX_2D	(D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
// 頂点フォーマット( 頂点座標[3D] / 法線ベクトル / 反射光 / テクスチャ座標 )
#define	FVF_VERTEX_3D	(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1)

// 上記頂点フォーマットに合わせた構造体を定義
typedef struct
{
	D3DXVECTOR3 vtx;		// 頂点座標
	float rhw;				// テクスチャのパースペクティブコレクト用
	D3DCOLOR diffuse;		// 反射光
	D3DXVECTOR2 tex;		// テクスチャ座標
} VERTEX_2D;

typedef struct
{
	D3DXVECTOR3 vtx;		// 頂点座標
	D3DXVECTOR3 normal;		// 法線ベクトル
	D3DCOLOR diffuse;		// 反射光
	D3DXVECTOR2 tex;		// テクスチャ座標
} VERTEX_3D;


enum MAINTRANSITION
{
	STATE_TITLE,
	STATE_SELECTMENU,
	STATE_GAME,
};

enum PlayerName
{
	NAME_TICK,
	NAME_TACK
};

enum STAGENAME
{
	STAGE_NONE,
	STAGE_FOREST,
	STAGE_VOLCANO,
	STAGE_SEA,

};

#endif