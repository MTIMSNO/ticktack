//=============================================================================
//
// ステージ処理 [Flower.h]
//
//=============================================================================

#ifndef ___STAGEFLOWER_BASE_H___
#define ___STAGEFLOWER_BASE_H___

#include "main.h"
#include "CollisionBase.h"

//-----------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------
#define FLOWER_NUM_MAIN	(4)
#define FLOWER_NUM_SUB	(5)

//-----------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------
HRESULT InitFlowerGimmick(LPDIRECT3DDEVICE9 pDevice);
void UninitFlowerGimmick(void);
void UpdateFlowerGimmick(void);
void DrawFlowerGimmick(LPDIRECT3DDEVICE9 pDevice);

// 任意マスステージセルの設定
void SetFlowerCellType( int row, int column, int type );

// 任意ステージの初期設定
// ※1-1から開始、STAGE_NUM_MAIN-STAGE_NUM_SUBまで
void SetFlower_FlowerNum( int main_stage, int sub_stage );

// パイプとの当たり判定
bool CheckHit_FlowerCell( const tCollisionData& DATA );

D3DXVECTOR3 GetFlowerGimmickPos(int x,int y);
void SetFlowerGimmickPos(int x,int y,D3DXVECTOR3 pos);

#endif