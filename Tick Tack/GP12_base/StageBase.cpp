//=============================================================================
//=============================================================================

#include "StageBase.h"
#include "input.h"
#include "Player.h"

#pragma warning (disable:4244)	// 暗黙型変換警告
#pragma warning (disable:4996)	// fopen警告

//*****************************************************************************
// マクロ定義
//*****************************************************************************
//#define _DEBUG_
//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void SetStage_StageNum( int main_stage, int sub_stage );
void SetStageCell_StageNum( int main_stage, int sub_stage );
void Process_SetStageCellType( int layer, int row, int column, int type, RECT rect[] );

void SetStage_Scroll(void);
void Process_StageScroll( D3DXVECTOR3& Vec_Scroll );
void ResetStageScroll(void);

bool CheckHitStageCell_toPlayer( int player_num );
bool CheckHitStageCell_toPlayer2(int player_num );
void ProcessStage_AdjustPlayer( int player_num, D3DXVECTOR3* pos_now, const D3DXVECTOR3& pos_pre, const tCollisionData& Col_p, const tCollisionData& Col_b );

void GetStageCellRect( RECT data[], const TCHAR* file );

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell	g_StageCell[STAGE_LAYER_MAX][STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];
RECT		g_StageRectDisp;

void	(*gp_StageCell_PreStartFunc[STAGE_LAYER_MAX])(void);
void	(*gp_StageCell_UpdateFunc[STAGE_LAYER_MAX])(void);
TCHAR* (*gp_StageCell_GetTableFileNameFunc[STAGE_LAYER_MAX])(void);

#ifdef _DEBUG_
tObjData			g_StageDebug_Hit;
#endif

//=============================================================================
// セルデータ管理配列の初期化
//=============================================================================
void LinkStage_CellArray( tStageCell* data[][STAGE_CELL_COLUMN_NUM],  int layer ){
	for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
		for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
			data[i][j] = &g_StageCell[layer][i][j];
		}
	}
}

//=============================================================================
// プレスタート関数のポインタを設定
//=============================================================================
void LinkStage_PreStartFunc( void (*data)(void),  int layer ){
	gp_StageCell_PreStartFunc[layer] = data;
}

//=============================================================================
// アップデート関数のポインタを設定
//=============================================================================
void LinkStage_UpdateFunc( void (*data)(void),  int layer ){
	gp_StageCell_UpdateFunc[layer] = data;
}

//=============================================================================
// テーブル取得関数のポインタを設定
//=============================================================================
void LinkStage_TableFunc( TCHAR* (*data)(void),  int layer ){
	gp_StageCell_GetTableFileNameFunc[layer] = data;
}

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT InitStage(LPDIRECT3DDEVICE9 pDevice)
{
	for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
		for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
			for(int k=0; k<STAGE_LAYER_MAX; k++){
				// 各セルに対応するオブジェクトを作成し、データを初期化する
				g_StageCell[k][i][j].type = 0;
				tObjData&		tmpObj = g_StageCell[k][i][j].obj;
				tCollisionData&	tmpCol = g_StageCell[k][i][j].collision;

				tmpObj.pos = D3DXVECTOR3( STAGE_CELL_WIDTH * j, STAGE_CELL_HEIGHT * i, 0.0f );
				tmpObj.rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
				tmpObj.fScale = 1.0f;
				tmpObj.size = D3DXVECTOR2( STAGE_CELL_WIDTH, STAGE_CELL_HEIGHT );
				tmpObj.pAnim = NULL;

				CreateCollision(
					&tmpCol,
					tmpObj.pos, COLLISION_TYPE_BOX,
					STAGE_CELL_WIDTH/2, STAGE_CELL_HEIGHT/2, 0.0f
					);

				MakePolygon( pDevice, &tmpObj, STAGE_CELL_TEXTURE );
				tmpObj.bExist = false;
			}
		}
	}

#ifdef _DEBUG_
	tObjData&	tmpObj = g_StageDebug_Hit;

	tmpObj.pos = D3DXVECTOR3( 50.0f, 50.0f, 0.0f );
	tmpObj.rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	tmpObj.fScale = 1.0f;
	tmpObj.size = D3DXVECTOR2( 50.0f, 50.0f );
	tmpObj.pAnim = NULL;
	MakePolygon( pDevice, &tmpObj, "×" );
#endif

	return S_OK;
}

//=============================================================================
// ステージ開始時の処理
//=============================================================================
void PreStartStage( int stage_main, int stage_sub ){
	SetStage_StageNum( stage_main, stage_sub );

	// 各レイヤーのプレスタート処理
	for(int i=0; i<STAGE_LAYER_MAX; i++){
		(*gp_StageCell_PreStartFunc[i])();
	}

	// スクロール初期化
	ResetStageScroll();

	// セルテーブル上のディスプレイ範囲を設定
	GetStageRect_AroundPlayer( &g_StageRectDisp, D3DXVECTOR3( SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 0.0f ) );

	SetClearFlag(false);
	ReInitGoal();
}

//=============================================================================
// 終了処理
//=============================================================================
void UninitStage(void)
{
	for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
		for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
			for(int k=0; k<STAGE_LAYER_MAX; k++){
				UninitPolygonAndTexture( &g_StageCell[k][i][j].obj );
			}
		}
	}
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateStage(void)
{
	// サウンド設定
	switch(GetData(DATA_MAINTRANSITION))
	{
	case STATE_SELECTMENU:
		if(IsPlaying(BGM_GAME) != false)	SetSound(SOUND_STOP,BGM_GAME);
		break;

	case STATE_GAME:
		if(IsPlaying(BGM_GAME) != true)		SetSound(SOUND_PLAY,BGM_GAME);
		break;
	}

	int	player_num = GetPlayerNumber();

	// 各レイヤーのアップデート処理
	for(int i=0; i<STAGE_LAYER_MAX; i++){
		(*gp_StageCell_UpdateFunc[i])();
	}

	// プレイヤーとの判定
	CheckHitStageCell_toPlayer( player_num );
	for(int i=0; i<MAX_PLAYER; i++){
		CheckHitStageCell_toPlayer2( i );
	}

	// スクロール処理
	// ※プレイヤー座標に依存するので、プレイヤー移動が全て終わってから処理する
	static int pre_PlayerNum = 0;
	D3DXVECTOR3 Vec_Scroll = GetPlayerPos( player_num );

	if( pre_PlayerNum != player_num ){
		// キャラ切り替えがあった場合は、変更後のキャラが画面中心に来るようにスクロールする
		Vec_Scroll -= GetPlayerBeforePos( pre_PlayerNum );
		pre_PlayerNum = player_num;
	}
	else{
		// 移動量に応じてスクロールする
		Vec_Scroll -= GetPlayerBeforePos( player_num );
	}
	Process_StageScroll( Vec_Scroll );


#ifdef _DEBUG_
	// プレイヤー判定のテスト
	{
		if( CheckHitStageCell_toPlayer( GetPlayerCollision(0) ) ){
			ChangeTexture( &g_StageDebug_Hit, "○" );
		}
		else{
			ChangeTexture( &g_StageDebug_Hit, "×" );
		}
		SetStage_Scroll();
	}

	// ボタンを押すとブロック配置が変化します
	if ( GetKeyboardPress(DIK_Z) ){
		SetStage_StageNum( 1, 1 );
	}
	else if ( GetKeyboardPress(DIK_X) ){
		SetStage_StageNum( 1, 2 );
	}
	else if ( GetKeyboardPress(DIK_C) ){
		SetStage_StageNum( 1, 3 );
	}
#endif
}

//=============================================================================
// 描画処理
//=============================================================================
void DrawStage(LPDIRECT3DDEVICE9 pDevice)
{
	RECT&	rect = g_StageRectDisp;

	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_2D);

	for(int k=0; k<STAGE_LAYER_MAX; k++){
		for(int i=rect.top; i<rect.bottom; i++){
			for(int j=rect.left; j<rect.right; j++){
				tObjData& tmpObj = g_StageCell[k][i][j].obj;
				// 非表示状態なら描画しない
				if( tmpObj.bExist == false ){
					continue;
				}

				// テクスチャの設定
				pDevice->SetTexture( 0, g_StageCell[k][i][j].obj.pD3DTexture );
				// 頂点バッファをデバイスのデータストリームにバインド
				pDevice->SetStreamSource( 0, tmpObj.pD3DVtxBuff, 0, sizeof(VERTEX_2D) );
				// ポリゴンの描画
				pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
			}
		}
	}
#ifdef _DEBUG_
	// 頂点バッファをデバイスのデータストリームにバインド
	pDevice->SetStreamSource( 0, g_StageDebug_Hit.pD3DVtxBuff, 0, sizeof(VERTEX_2D) );
	// テクスチャの設定
	pDevice->SetTexture( 0, g_StageDebug_Hit.pD3DTexture );
	// ポリゴンの描画
	pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
#endif
}
//=============================================================================
// 各ステージごとの初期設定
//=============================================================================
void SetStage_StageNum( int main_stage, int sub_stage ){
	// データ範囲外を操作しないようにチェック
	if( ( main_stage > STAGE_NUM_MAIN ) ||
		( main_stage <= 0 ) ||
		( sub_stage  > STAGE_NUM_SUB ) ||
		( sub_stage  <= 0 ) ){
		return;
	}

	// ステージに応じた配置テーブルを読み込み、各セルに反映する
	SetStageCell_StageNum( main_stage, sub_stage );
}

//=============================================================================
// 各ステージごとのセル配置データを反映
//=============================================================================
void SetStageCell_StageNum( int main_stage, int sub_stage ){
	int data[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM] = {{0}};	// 配置テーブル取得用
	char main[128]={0}, sub[128]={0};		// ステージ番号の文字列取得用
	RECT rect[STAGE_CELL_IDMAX] = {{0}};	// チップごとのテクスチャ範囲取得用

	for(int k=0; k<STAGE_LAYER_MAX; k++){
		for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
			for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
				data[i][j]=0;
			}
		}

		// 各レイヤーに応じた配置テーブルファイル名を取得
		// intデータを文字列へ変換
		sprintf( main, "_%d", main_stage );
		sprintf(  sub, "_%d",  sub_stage );
		// 取得対象のCSVファイル名を算出
		std::string	file_base = (std::string)(*gp_StageCell_GetTableFileNameFunc[k])(),
			file_table = file_base + (std::string)main + (std::string)sub + ".csv",
			file_rect  = file_base + "_RECT.csv";

		// 配置テーブルを取得
		GetStageCellTable( data, file_table.c_str() );

		// テクスチャ範囲を取得
		GetStageCellRect( rect, file_rect.c_str() );

		// 配置を設定する
		for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
			for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
				Process_SetStageCellType( k, i, j, data[i][j], rect );
			}
		}
	}
}

//=============================================================================
// チップ設定処理
//=============================================================================
void Process_SetStageCellType( int layer, int row, int column, int type, RECT rect[] ){
	// データ範囲外を操作しないようにチェック
	if( ( row >= STAGE_CELL_ROW_NUM ) ||
		( column >= STAGE_CELL_COLUMN_NUM ) ||
		( type < 0 ) ||
		( type >= STAGE_CELL_IDMAX ) ){
		return;
	}

	// 新しいセルデータを反映し、ブロックがある場合はテクスチャ変更
	if( ( g_StageCell[layer][row][column].type = type ) > 0 ){
		SetPolygonTextureRect( g_StageCell[layer][row][column].obj, rect[ type ] );
		g_StageCell[layer][row][column].obj.bExist = true;
		// スクロール反映待ちの可能性があるので、頂点座標の設定を行う
		SetVertexPolygon( &g_StageCell[layer][row][column].obj );
	}
	else{
		// チップ設定が無い場合は描画を切る
		g_StageCell[layer][row][column].obj.bExist = false;
	}
}

//=============================================================================
// 任意セルのチップ設定
//=============================================================================
void SetStageCellType( int layer, int row, int column, int type ){
	RECT rect[STAGE_CELL_IDMAX] = {{0}};

	// 各レイヤーに応じた配置テーブルファイル名を取得
	std::string	file_base = (std::string)(*gp_StageCell_GetTableFileNameFunc[layer])(),
		file_rect  = file_base + "_RECT.csv";

	// テクスチャ範囲を取得
	GetStageCellRect( rect, file_rect.c_str() );

	// 対象セルのチップを変更
	Process_SetStageCellType( layer, row, column, type, rect );
}

//=============================================================================
// 地形との当たり判定処理
//=============================================================================
bool CheckHitStageCell_toPlayer( int player_num ){
	bool flag = false;
	RECT rect;
	const tCollisionData& DATA = GetPlayerCollision( player_num );
	D3DXVECTOR3 pos_now = GetPlayerPos(player_num), pos_pre = GetPlayerBeforePos(player_num),
		Vec_OBBlength = DATA.axis[0] * DATA.length.x + DATA.axis[1] * DATA.length.y;

	// プレイヤー当たり判定内にあるセル範囲番号を取得
	GetStageRect_AroundPlayer( &rect, Vec_OBBlength );

	for(int i=rect.top; i<rect.bottom; i++){
		for(int j=rect.left; j<rect.right; j++){
			if( g_StageCell[STAGE_LAYER_LAND][i][j].type == 0 ){
				continue;
			}

			tCollisionData& ColData = g_StageCell[STAGE_LAYER_LAND][i][j].collision;
			if( CheckCollision( DATA, ColData ) ){
				// 接触している場合は、押し戻し処理
				ProcessStage_AdjustPlayer( player_num, &pos_now, pos_pre, DATA, ColData );

				SetPlayerPos( player_num, pos_now );

				// 押し戻し後も他のブロックにめり込んでいる可能性があるので、衝突フラグだけ立ててループ継続
				flag = true;
			}
		}
	}

	return flag;
}
//=============================================================================
// 地形との当たり判定処理　*移動ブロック用
//=============================================================================
bool CheckHitStageCell_toPlayer2( int player_num ){
	bool flag = false;
	RECT rect;
	const tCollisionData& DATA = GetPlayerCollision( player_num );
	D3DXVECTOR3 pos_now = GetPlayerPos(player_num), pos_pre = GetPlayerBeforePos(player_num),
		Vec_OBBlength = DATA.axis[0] * DATA.length.x + DATA.axis[1] * DATA.length.y;

	// プレイヤー当たり判定内にあるセル範囲番号を取得
	GetStageRect_AroundPlayer( &rect, Vec_OBBlength );

	for(int i=rect.top; i<rect.bottom; i++){
		for(int j=rect.left; j<rect.right; j++){
			if( g_StageCell[STAGE_LAYER_BLOCK_MOVE][i][j].type == 0 ){
				continue;
			}

			tCollisionData& ColData2 = g_StageCell[STAGE_LAYER_BLOCK_MOVE][i][j].collision;

			if( CheckCollision( DATA, ColData2 ) ){
				// 接触している場合は、押し戻し処理

				ProcessStage_AdjustPlayer( player_num, &pos_now, pos_pre, DATA, ColData2 );

				SetPlayerPos(player_num,pos_now);

				// 押し戻し後も他のブロックにめり込んでいる可能性があるので、衝突フラグだけ立ててループ継続
				flag = true;
			}
		}
	}

	return flag;
}
//=============================================================================
// 地形にめり込んだプレイヤーを押し戻す
//=============================================================================
void ProcessStage_AdjustPlayer( int player_num, D3DXVECTOR3* pos_now, const D3DXVECTOR3& pos_pre, const tCollisionData& Col_p, const tCollisionData& Col_b ){
	D3DXVECTOR3 tmp_axis(0.0f,0.0f,0.0f), Vec_Adjust(0.0f,0.0f,0.0f), 
		Vec_OBBtoPoint = GetPlayerPos(player_num) - *Col_b.pos,	// OBB中心点からプレイヤー現在位置へのベクトル
		Vec_OBBtoPrePoint = pos_pre - *Col_b.pos;		// OBB中心点からプレイヤー前フレーム位置へのベクトル
	float span=0.0f, tmp_length=0.0f,
		tmp_x = GetPosNum_OBBtoPoint( Vec_OBBtoPrePoint, Col_b.axis[0], Col_b.length.x ),	// OBBの軸１方向に対する、プレイヤー前フレーム位置を計算
		tmp_y = GetPosNum_OBBtoPoint( Vec_OBBtoPrePoint, Col_b.axis[1], Col_b.length.y );	// OBBの軸２方向に対する、プレイヤー前フレーム位置を計算
	int direction=0;	// 押し戻す際の向き（正負）係数用

	switch( Col_p.type ){
	case COLLISION_TYPE_CIRCLE:
		// 前フレーム位置を比較し、より離れている軸方向へ押し戻す
		if( fabs( tmp_x ) > fabs( tmp_y ) ){
			// 軸１方向から衝突
			tmp_length = Col_b.length.x;
			tmp_axis = Col_b.axis[0];
			direction = ( tmp_x > 0 )? 1: -1;
		}
		else{
			// 軸２方向から衝突
			tmp_length = Col_b.length.y;
			tmp_axis = Col_b.axis[1];
			direction = ( tmp_y > 0 )? 1: -1;
		}

		// めり込んだ長さを計算
		span = Col_p.radius + tmp_length - fabs( D3DXVec3Dot( &Vec_OBBtoPoint, &tmp_axis ) );
		// 衝突した軸方向へ押し戻すベクトルを算出
		Vec_Adjust += tmp_axis * span * direction;

		break;
	case COLLISION_TYPE_BOX:
		// ___ToDo：プレイヤー当たり判定を箱にする時の押し戻し処理
		break;
	default:
		break;
	}

	// 押し戻しベクトルを現在位置へ反映する
	*pos_now += Vec_Adjust;
}

//=============================================================================
// スクロール処理
//=============================================================================
void Process_StageScroll( D3DXVECTOR3& Vec_Scroll ){
	RECT&	rect = g_StageRectDisp;

	if( D3DXVec3LengthSq( &Vec_Scroll ) ){
		// プレイヤーの位置をスクロール分戻す
		// ※プレイヤーは常に画面中心になるようにスクロールさせる
		SetPlayerPos( GetPlayerNumber(), GetPlayerPos(GetPlayerNumber()) - Vec_Scroll );

		// 表示するテーブル範囲を更新
		GetStageRect_AroundPlayer( &rect, D3DXVECTOR3( SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 0.0f ) );

		for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
			for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
				for(int k=0; k<STAGE_LAYER_MAX; k++){
					g_StageCell[k][i][j].obj.pos -= Vec_Scroll;

					if( g_StageCell[k][i][j].type > 0 ){
						// 負荷が大きいので、見えてないセルポリゴンは移動させない
						// ※座標値だけ移動させておいて、見えるようになった時に移動させる
						SetVertexPolygon( &g_StageCell[k][i][j].obj );
					}
				}
			}
		}
	}
}

//=============================================================================
// スクロールの初期化
//=============================================================================
void ResetStageScroll(void){
	for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
		for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
			for(int k=0; k<STAGE_LAYER_MAX; k++){
				tObjData&		tmpObj = g_StageCell[k][i][j].obj;

				tmpObj.pos = D3DXVECTOR3( STAGE_CELL_WIDTH * j, STAGE_CELL_HEIGHT * i, 0.0f );
			}
		}
	}
}

//=============================================================================
// 配置テーブルの取得処理
//=============================================================================
void GetStageCellTable( int data[][STAGE_CELL_COLUMN_NUM], const TCHAR* file ){
	FILE*	fp;
	char buf[STAGE_CELL_COLUMN_NUM * 4 + 255];	// チップID四桁 * 列数 + 緩衝
	char* tmp_row;
	int row=0, column=0;
	std::string file_path = STAGE_CELL_PATH + (std::string)file;

	if( ( fp = fopen( file_path.c_str() ,"r") ) != NULL ){
		while( fgets( buf, sizeof( buf ), fp ) ){
			if( ( tmp_row = strtok( buf, "," ) ) == NULL ){
				break;
			};

			data[row][column] = ( strlen( tmp_row ) == 0 )? 0: atoi( tmp_row );
			if( data[row][column] == 999 ){
				data[row][column] = 0;
			}

			column++;

			for(; column<STAGE_CELL_COLUMN_NUM; column++){
				if( ( tmp_row = strtok( NULL, "," ) ) == NULL ){
					break;
				};

				data[row][column] = ( strlen( tmp_row ) == 0 )? 0: atoi( tmp_row );
				if( data[row][column] == 999 ){
					data[row][column] = 0;
				}
			}

			column = 0;
			row++;
		}
	}
	else{
		// 読み込み対象ファイルが存在しない場合
		std::string str1( "ファイル 『』 が存在しません！" );
		str1.insert( 11, file_path );

		MessageBox(NULL,
			str1.c_str(),
			_T("warning"),MB_OK);
	}

	fclose( fp );
}

//=============================================================================
// ブロックテクスチャ範囲の取得処理
//=============================================================================
void GetStageCellRect( RECT data[], const TCHAR* file ){
	FILE*	fp;
	char buf[4 * 4 + 32];	// RECT設定四桁 * 四隅 + 緩衝
	char* tmp_row;
	int id;
	LONG*	pRect=NULL;
	std::string file_path = STAGE_CELL_PATH + (std::string)file;

	if( ( fp = fopen( file_path.c_str() ,"r") ) != NULL ){
		// 項目名の行を飛ばす
		fgets( buf, sizeof( buf ), fp );

		while( fgets( buf, sizeof( buf ), fp ) ){
			if( ( tmp_row = strtok( buf, "," ) ) == NULL ){
				break;
			};

			if( ( id = atoi( tmp_row ) ) == 999 ){
				continue;
			}

			pRect = &data[id].left;

			for(int i=0; i<4; i++){
				if( ( tmp_row = strtok( NULL, "," ) ) == NULL ){
					break;
				};
				*pRect = atoi( tmp_row );
				pRect++;
			}
		}
	}
	else{
		// 読み込み対象ファイルが存在しない場合
		std::string str1( "ファイル 『』 が存在しません！" );
		str1.insert( 11, file_path );

		MessageBox(NULL,
			str1.c_str(),
			_T("warning"),MB_OK);
	}

	fclose( fp );
}

//=============================================================================
// プレイヤーを中心とした任意範囲のセル範囲番号取得
//=============================================================================
void GetStageRect_AroundPlayer( RECT* rect, D3DXVECTOR3& Vec_Width ){
	// セルテーブル上のプレイヤー座標を取得
	D3DXVECTOR3 posPlayer_onField = GetPlayerPos(GetPlayerNumber()) - g_StageCell[STAGE_LAYER_LAND][0][0].obj.pos;
	// 各角のセルIDを算出
	// ※余裕をもって大きめに取得する
	int row_start = (int)( posPlayer_onField.y - Vec_Width.y ) / STAGE_CELL_HEIGHT - 1,
		row_end   = (int)( posPlayer_onField.y + Vec_Width.y ) / STAGE_CELL_HEIGHT + 2,
		column_start = (int)( posPlayer_onField.x - Vec_Width.x ) / STAGE_CELL_WIDTH - 1,
		column_end   = (int)( posPlayer_onField.x + Vec_Width.x ) / STAGE_CELL_WIDTH + 5;

	// 設定範囲外を操作しないように補正
	if( row_start < 0 ){
		row_start = 0;
	}
	if( row_end > STAGE_CELL_ROW_NUM ){
		row_end = STAGE_CELL_ROW_NUM;
	}
	if( column_start < 0 ){
		column_start = 0;
	}
	if( column_end > STAGE_CELL_COLUMN_NUM ){
		column_end = STAGE_CELL_COLUMN_NUM;
	}

	rect->left		= column_start;
	rect->top		= row_start;
	rect->right		= column_end;
	rect->bottom	= row_end;
}

//=============================================================================
// 可視範囲の取得
//=============================================================================
void GetStageRect_Disp( RECT* data ){
	*data = g_StageRectDisp;
}

//=============================================================================
// セルのアニメーション設定
//=============================================================================
void SetStageCellAnimation( tObjData* obj, const D3DXVECTOR2& size, const tAnimKeyData* data[], TCHAR* texture ){
	obj->sizeMax = size;
	obj->pAnim = data;
	ChangeTexture( obj, texture );
}

//=============================================================================
// レイヤーのアニメーション設定
//=============================================================================
void SetStageLayerAnimation( int layer, const D3DXVECTOR2& size, const tAnimKeyData* data[], TCHAR* texture ){
	for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
		for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
			tObjData& obj = g_StageCell[layer][i][j].obj;

			obj.sizeMax = size;
			obj.pAnim = data;
			ChangeTexture( &obj, texture );
		}
	}
}