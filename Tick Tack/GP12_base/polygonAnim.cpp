// アニメーションデータ

#include "polygonAnim.h"

//===================================================================================
// プレイヤー　Ver.Tick
//===================================================================================
const D3DXVECTOR2 c_sizePlayer00(896,768);	// 画像のサイズ
const tAnimKeyData c_animPlayer00_00[] =
{
	{
		3,	// フレーム
		{0, 0, 127, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{128, 0, 255, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{255, 0, 383, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{384, 0, 511, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{512, 0, 639, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{640, 0, 767, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{768, 0, 895, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{256, 128, 383, 255},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{383, 128, 511, 255},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{512, 128, 639, 255},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{650, 128, 767, 255},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{768, 128, 895, 255},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData c_animPlayer00_01[] =
{
	{
		3,	// フレーム
		{0, 257, 128, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{129, 257, 256, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{257, 257, 384, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{385, 257, 512, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{513, 257, 640, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{641, 257, 768, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{769, 257, 896, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{0, 385, 128, 512},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{129, 385, 256, 512},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{257, 385, 384, 512},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{385, 385, 512, 512},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{513, 385, 640, 512},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData c_animPlayer00_02[] =
{
	{
		5,	// フレーム
		{0, 513, 128, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{129, 513, 256, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{257, 513, 384, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{385,513, 512, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{513,513, 640, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{641,513, 768, 640},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};

const tAnimKeyData c_animPlayer00_03[] =
{
	{
		15,	// フレーム
		{129, 640, 256, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{257, 640, 384, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{129, 640, 256, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{257, 640, 384, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{129, 640, 256, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{257, 640, 384, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{129, 640, 256, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{257, 640, 384, 768},	// 座標
		ANIM_FLAG_STOP	// フラグ
	},
};

const tAnimKeyData c_animPlayer00_04[] =
{
	{
		15,	// フレーム
		{0, 640, 127, 768},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};

const tAnimKeyData* c_animPlayer00[] = {
	c_animPlayer00_00,
	c_animPlayer00_01,
	c_animPlayer00_02,
	c_animPlayer00_03,
	c_animPlayer00_04,
};

//===================================================================================
// プレイヤー Ver.Tack
//===================================================================================
const D3DXVECTOR2 c_sizePlayer01(896,768);	// 画像のサイズ
const tAnimKeyData c_animPlayer01_00[] =
{
	{
		3,	// フレーム
		{0, 0, 128, 128},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{129, 0, 256, 128},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{257, 0, 384, 128},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{385, 0, 512, 128},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{513, 0, 640, 128},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{641, 0, 768, 128},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{769, 0, 896, 128},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{257, 129, 384, 256},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{385, 129, 512, 256},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{513, 129, 640, 256},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{641, 129, 768, 256},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{769, 129, 896, 256},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData c_animPlayer01_01[] =
{
	{
		3,	// フレーム
		{0, 257, 128, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{129, 257, 256, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{257, 257, 384, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{385, 257, 512, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{513, 257, 640, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{641, 257, 768, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{769, 257, 896, 384},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{0, 385, 128, 512},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{129, 385, 256, 512},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{257, 385, 384, 512},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{385, 385, 512, 512},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{513, 385, 640, 512},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData c_animPlayer01_02[] =
{
	{
		5,	// フレーム
		{0, 513, 128, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{129, 513, 256, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{257, 513, 384, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{385,513, 512, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{513,513, 640, 640},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		5,	// フレーム
		{641,513, 768, 640},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};

const tAnimKeyData c_animPlayer01_03[] =
{
	{
		15,	// フレーム
		{129, 640, 256, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{257, 640, 384, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{129, 640, 256, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{257, 640, 384, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{129, 640, 256, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{257, 640, 384, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{129, 640, 256, 768},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		15,	// フレーム
		{257, 640, 384, 768},	// 座標
		ANIM_FLAG_STOP	// フラグ
	},
};

const tAnimKeyData c_animPlayer01_04[] =
{
	{
		15,	// フレーム
		{0, 640, 127, 768},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};

const tAnimKeyData* c_animPlayer01[] = {
	c_animPlayer01_00,
	c_animPlayer01_01,
	c_animPlayer01_02,
	c_animPlayer01_03,
	c_animPlayer01_04,
};


//===================================================================================
// 背景
//===================================================================================
const D3DXVECTOR2 c_sizeBackGround00(512,640);	// 画像サイズ
const tAnimKeyData c_animBackGround00_00[] =
{
	{
		1,						// フレーム数
		{0,0,512,128},			// 画像の座標
		ANIM_FLAG_LOOP			// フラグ
	}
};
const tAnimKeyData c_animBackGround00_01[] =
{
	{
		1,						// フレーム数
		{0,129,512,256},			// 画像の座標
		ANIM_FLAG_LOOP			// フラグ
	}
};
const tAnimKeyData c_animBackGround00_02[] =
{
	{
		1,						// フレーム数
		{0,257,512,384},			// 画像の座標
		ANIM_FLAG_LOOP			// フラグ
	}
};
const tAnimKeyData c_animBackGround00_03[] =
{
	{
		1,						// フレーム数
		{0,385,512,512},			// 画像の座標
		ANIM_FLAG_LOOP			// フラグ
	}
};
const tAnimKeyData c_animBackGround00_04[] =
{
	{
		1,						// フレーム数
		{0,513,512,640},			// 画像の座標
		ANIM_FLAG_LOOP			// フラグ
	}
};
const tAnimKeyData* c_animBackGround00[] = {
	c_animBackGround00_00,
	c_animBackGround00_01,
	c_animBackGround00_02,
	c_animBackGround00_03,
	c_animBackGround00_04,
};

//===================================================================================
// ギミック：風
//===================================================================================
const D3DXVECTOR2 c_sizeWind00(1024,512);	// 画像のサイズ
const tAnimKeyData c_animWind00_00[] =
{
	{
		3,	// フレーム
		{0, 0, 255, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{256, 0, 511, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{512, 0, 767, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{768, 0, 1023, 127},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{0, 128, 255, 255},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{256, 128, 511, 255},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{512, 128, 767, 255},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{768, 128, 1023, 255},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{0, 256, 255, 383},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{256, 256, 511, 383},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{512, 256, 767, 383},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{768, 256, 1023, 383},	// 座標
		ANIM_FLAG_NONE	// フラグ
	},
	{
		3,	// フレーム
		{0, 384, 255, 511},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};

const tAnimKeyData* c_animWind00[] = {
	NULL,
	NULL,
	c_animWind00_00,
	NULL,
	c_animWind00_00,
	NULL,
	c_animWind00_00,
	NULL,
	NULL,
	NULL
};


//===================================================================================
// ポーズ画面
//===================================================================================
const D3DXVECTOR2 c_sizePauseMenu00(512,512);	// 画像のサイズ
const tAnimKeyData c_animPauseBackGround00[] =
{
	{
		3,	// フレーム
		{0, 0, 256, 256},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animPauseFrame00[] =
{
	{
		3,	// フレーム
		{257,0, 512, 256},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animPauseRetire00[] =
{
	{
		3,	// フレーム
		{0,257, 128, 384},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animPauseBack00[] =
{
	{
		3,	// フレーム
		{129,257, 256, 384},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animPauseValve00[] =
{
	{
		3,	// フレーム
		{257,257, 384, 384},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animPauseSettledValve00[] =
{
	{
		3,	// フレーム
		{385,257, 512, 384},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animPauseSettledValve01[] =
{
	{
		3,	// フレーム
		{0,385, 128, 512},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData* c_animPauseMenu00[] = {
	c_animPauseBackGround00,
	c_animPauseFrame00,
	c_animPauseRetire00,
	c_animPauseBack00,
	c_animPauseValve00,
	c_animPauseSettledValve00,
	c_animPauseSettledValve01,
};

//===================================================================================
// クリアメニュー画面
//===================================================================================
const D3DXVECTOR2 c_sizeClearMenu00(512,512);	// 画像のサイズ
const tAnimKeyData c_animClearBackGround00[] =
{
	{
		3,	// フレーム
		{0, 0, 255, 255},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animClear00[] =
{
	{
		3,	// フレーム
		{256, 0, 511, 256},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animSelectStage00[] =
{
	{
		3,	// フレーム
		{0, 255, 255, 511},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animNextStage00[] =
{
	{
		3,	// フレーム
		{256, 256, 511, 511},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData* c_animClearMenu00[] = {
	c_animClearBackGround00,
	c_animClear00,
	c_animSelectStage00,
	c_animNextStage00,
};

//===================================================================================
// タイトル
//===================================================================================
const D3DXVECTOR2 c_sizeTitle00(1024,1024);	// 画像のサイズ
const tAnimKeyData c_animTitleLogo00[] =
{
	{
		3,	// フレーム
		{0, 512, 1023, 1023},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animTitleLine00[] =
{
	{
		3,	// フレーム
		{0, 0, 1023, 255},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData c_animTitleLine01[] =
{
	{
		3,	// フレーム
		{0, 256, 1023, 511},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	}
};
const tAnimKeyData* c_animTitle00[] = {
	c_animTitleLogo00,
	c_animTitleLine00,
	c_animTitleLine01,
};


//===================================================================================
// ギミック:鳥
//===================================================================================
const D3DXVECTOR2 c_sizeBird00(1024,1024);	// 画像のサイズ
const tAnimKeyData c_animBird00_00[] =
{
	{
		3,	// フレーム
		{0, 0, 127, 127},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		3,	// フレーム
		{128, 0, 255, 127},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		3,	// フレーム
		{256, 0, 383, 127},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		3,	// フレーム
		{384, 0, 511, 127},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		3,	// フレーム
		{512, 0, 639, 127},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		3,	// フレーム
		{640, 0, 767, 127},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		3,	// フレーム
		{768, 0, 895, 127},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		3,	// フレーム
		{896, 0, 1023, 127},	// 座標
		ANIM_FLAG_LOOP// フラグ
	},
};
const tAnimKeyData c_animBird00_01[] =
{
	{
		5,	// フレーム
		{0, 127, 127, 255},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animBalloon00_00[] =
{
	{
		300,	// フレーム
		{0, 256, 511, 511},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		300,	// フレーム
		{0, 512, 511, 767},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		300,	// フレーム
		{0, 768, 511, 1023},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		300,	// フレーム
		{512, 256, 1023, 511},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		300,	// フレーム
		{512, 512, 1023, 767},	// 座標
		ANIM_FLAG_NONE// フラグ
	},
	{
		300,	// フレーム
		{512, 768, 1023, 1023},	// 座標
		ANIM_FLAG_LOOP// フラグ
	},
};
const tAnimKeyData* c_animBird00[] = {
	c_animBird00_00,	
	c_animBird00_01,
	c_animBalloon00_00,
};

//===================================================================================
// セレクト画面
//===================================================================================
const D3DXVECTOR2 c_sizeSelect00(1024,1536);	// 画像のサイズ
const tAnimKeyData c_animSelect00_00[] =
{
	{
		5,	// フレーム
		{0, 0, 511, 255},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_01[] =
{
	{
		5,	// フレーム
		{512, 0, 1023, 255},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_02[] =
{
	{
		5,	// フレーム
		{0, 256, 511, 511},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_03[] =
{
	{
		5,	// フレーム
		{512, 256, 1023, 511},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_04[] =
{
	{
		5,	// フレーム
		{0, 512, 511, 767},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_05[] =
{
	{
		5,	// フレーム
		{512, 512, 1023, 767},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_06[] =
{
	{
		5,	// フレーム
		{0, 768, 511, 1023},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_07[] =
{
	{
		5,	// フレーム
		{512, 768, 1023, 1023},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_08[] =
{
	{
		5,	// フレーム
		{0, 1024, 511, 1279},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_09[] =
{
	{
		5,	// フレーム
		{512, 1024, 1023, 1279},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_10[] =
{
	{
		5,	// フレーム
		{0, 1280, 511, 1535},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData c_animSelect00_11[] =
{
	{
		5,	// フレーム
		{512, 1280, 1023, 1535},	// 座標
		ANIM_FLAG_LOOP// フラグ
	}
};
const tAnimKeyData* c_animSelect00[] = {
	c_animSelect00_00,
	c_animSelect00_01,
	c_animSelect00_02,
	c_animSelect00_03,
	c_animSelect00_04,
	c_animSelect00_05,
	c_animSelect00_06,
	c_animSelect00_07,
	c_animSelect00_08,
	c_animSelect00_09,
	c_animSelect00_10,
	c_animSelect00_11,
};