//=============================================================================
//
// ステージ処理 [StageBase.h]
//
//=============================================================================

#pragma once

#include "main.h"
#include "CollisionBase.h"
#include "PolygonBase.h"
#include "Sound.h"
#include "Data.h"
#include "BlockGoal.h"
#include "ClearMenu.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

#define STAGE_NUM_MAIN	(4)
#define STAGE_NUM_SUB	(4)

#define STAGE_CELL_PATH			_T("data/STAGE/")
#define STAGE_CELL_TEXTURE		_T("StageCell.png")
#define STAGE_CELL_IDMAX		(100)
#define STAGE_CELL_WIDTH		(50.0f)
#define STAGE_CELL_HEIGHT		(50.0f)
#define STAGE_CELL_COLUMN_NUM	(104)
#define STAGE_CELL_ROW_NUM		(20)

//※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※
//※※※　レイヤーを追加したい時は、↓の『列挙型定義』にデータを追加する
//※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※
//*****************************************************************************
// 列挙型定義
// ※先頭は必ず０から開始させてね
// 　番号が小さいほど奥、大きいほど手前に表示される
//*****************************************************************************
enum{
	STAGE_LAYER_TREE,
	//STAGE_LAYER_PIPE,
	STAGE_LAYER_CHECKPOINT,
	STAGE_LAYER_FLOWER,
	STAGE_LAYER_VINE,
	//STAGE_LAYER_BACK000 = 0,
	//STAGE_LAYER_BACK001,
	//STAGE_LAYER_BACK002,	// 必要に応じて増やしてください
	//STAGE_LAYER_BACK003,
	//STAGE_LAYER_BACK004,
	// ↑　地形より奥側　　↑
	STAGE_LAYER_LAND,		// 基準になる地形レイヤー、変更しないでね
	// ↓　地形より手前側　↓
	//STAGE_LAYER_FRONT001,	// 必要に応じて増やしてください
	//STAGE_LAYER_FRONT002,
	//STAGE_LAYER_FRONT003,
	//STAGE_LAYER_FRONT004,
	STAGE_LAYER_BLOCK_MOVE,
	STAGE_LAYER_SPIDERNET,
	STAGE_LAYER_BIRD,
	STAGE_LAYER_RETRY,
	STAGE_LAYER_GOAL,
	STAGE_LAYER_WIND,
	STAGE_LAYER_MAX
};
//※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※
//※※※　レイヤーを追加したい時は、↑の『列挙型定義』にデータを追加する
//※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※

//*****************************************************************************
// 構造体定義
//*****************************************************************************
typedef struct _tStageCell{
	int				type;
	tObjData		obj;
	tCollisionData	collision;
}tStageCell;

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************

HRESULT InitStage(LPDIRECT3DDEVICE9 pDevice);
void PreStartStage( int stage_main, int stage_sub );
void UninitStage(void);
void UpdateStage(void);
void DrawStage(LPDIRECT3DDEVICE9 pDevice);

// 任意マスステージセルの設定
void SetStageCellType( int layer, int row, int column, int type, RECT rect[] );

// セルデータ管理配列の初期化
void LinkStage_CellArray( tStageCell* data[][STAGE_CELL_COLUMN_NUM], int layer );

// プレスタート関数のポインタを設定
void LinkStage_PreStartFunc( void (*data)(void),  int layer );

// アップデート関数のポインタを設定
void LinkStage_UpdateFunc( void (*data)(void),  int layer );

// テーブル取得関数のポインタを設定
void LinkStage_TableFunc( TCHAR* (*data)(void),  int layer );

// 配置テーブルの取得
void GetStageCellTable( int data[][STAGE_CELL_COLUMN_NUM], const TCHAR* file );

// プレイヤーを中心とした任意範囲のセル範囲番号取得
// left	:	column_start
// top	:	row_start
// right:	column_end
// bottom:	row_end
void GetStageRect_AroundPlayer( RECT* rect, D3DXVECTOR3& Vec_Width );
// 可視範囲の取得
void GetStageRect_Disp( RECT* data );

// 任意マスステージセルの設定
void SetStageCellType( int layer, int row, int column, int type );

// セルのアニメーション設定
void SetStageCellAnimation( tObjData* obj, const D3DXVECTOR2& size, const tAnimKeyData* data[], TCHAR* texture );

void SetStageLayerAnimation( int layer, const D3DXVECTOR2& size, const tAnimKeyData* data[], TCHAR* texture );

