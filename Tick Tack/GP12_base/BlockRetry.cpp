#include "BlockRetry.h"
#include "Player.h"
#include "Fade.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PreStartBlockRetry(void);
void UpdateBlockRetry(void);
TCHAR* GetBlockRetry_TableFileName(void);

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*		g_Stage_BlockRetry[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];
D3DXVECTOR3*	g_BlockRetry_RetryPoint;

//=============================================================================
// 初期化処理
//=============================================================================
void InitBlockRetry(void){
	LinkStage_CellArray( g_Stage_BlockRetry, STAGE_LAYER_RETRY );

	LinkStage_PreStartFunc( PreStartBlockRetry, STAGE_LAYER_RETRY );

	LinkStage_UpdateFunc( UpdateBlockRetry, STAGE_LAYER_RETRY );

	LinkStage_TableFunc( GetBlockRetry_TableFileName, STAGE_LAYER_RETRY );
}

//=============================================================================
// ステージ開始時の処理
//=============================================================================
void PreStartBlockRetry(void){
	// ステージ開始時のキャラクター位置を設定する
	// ※仮設定
	g_BlockRetry_RetryPoint = &g_Stage_BlockRetry[5][5]->obj.pos;
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockRetry(void){
	static bool process_retry = false;

	if( process_retry ){
		// リトライ処理中
		if( SetFade( FADE_IN ) ){
			// 本来移動できない位置へ飛ばす為、地形に巻き込まれる可能性がある
			// とりあえず地形に巻き込まれないように、上方向にずらして戻す
			SetPlayerPos( GetPlayerNumber(), *g_BlockRetry_RetryPoint + D3DXVECTOR3(0.0f,-PLAYER_SIZE_Y/2,0.0f) );
			process_retry = false;
		}

		// 判定は取らない
		return;
	}

	RECT rect;
	// プレイヤーの当たり判定取得
	const tCollisionData Col_P = GetPlayerCollision( GetPlayerNumber() );
	// プレイヤー当たり判定内のセル範囲を取得
	GetStageRect_AroundPlayer( &rect, D3DXVECTOR3( Col_P.radius, Col_P.radius, 0.0f ) );

	for(int i=rect.top; i<rect.bottom; i++){
		for(int j=rect.left; j<rect.right; j++){
			switch( g_Stage_BlockRetry[i][j]->type ){
			case 1:
				// やり直し位置のチェックポイント登録
				if( CheckCollision( Col_P, g_Stage_BlockRetry[i][j]->collision ) ){
					g_BlockRetry_RetryPoint = &g_Stage_BlockRetry[i][j]->obj.pos;
				}
				break;
			case 2:
				// チェックポイントまで戻す
				if( CheckCollision( Col_P, g_Stage_BlockRetry[i][j]->collision ) ){
					if( SetFade( FADE_OUT ) ){
						// 戻すブロックに当たったら、フェード処理を設定する
						// フェードが開始出来たら、リトライ処理を開始する
						process_retry = true;
					}
				}
				break;
			default:
				break;
			}
		}
	}
}

//=============================================================================
// 配置データファイル名取得
//=============================================================================
TCHAR* GetBlockRetry_TableFileName(void){
	return BLOCKRETRY_CELLTABLE_FILENAME;
}