//=============================================================================
//
// ファイルを追加する場合のサンプル
// ※ヘッダーとソース部分の、「BlockSample」と「BLOCKSAMPLE」を新しい名前に変更してね
//		こっちで操作したいのに出来ないってのがあれば、教えてください
//		ベースの方に関数作ります
//
//=============================================================================

#pragma once

#include "main.h"
#include "StageBase.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
// 使用するCSVファイル名(ベース部分)を設定する
#define BLOCKCHECKPOINT_CELLTABLE_FILENAME	_T("BlockCheckPoint")
#define MAX_PIPE							(12)

//*****************************************************************************
// マクロ定義
//*****************************************************************************
enum CHECKPOINT_TYPE
{
	PIPE_NONE = 0,					//0//	何もなし
	PIPE_LENGTH_NORMAL,				//1//	縦方向ノーマルパイプ
	PIPE_WIGTH_NORMAL,				//2//	横方向ノーマルパイプ
	PIPE_CORNER_UPPERLEFT,			//3//	左上角パイプ
	PIPE_CORNER_UPPERRIGHT,			//4//	右上角パイプ
	PIPE_CORNER_LOWERLEFT,			//5//	左下角パイプ
	PIPE_CORNER_LOWERRIGHT,			//6//	右下角パイプ
	PIPE_TJUNC_UPPER,				//7//	上方向T字パイプ
	PIPE_TJUNC_LOWER,				//8//	下方向T字パイプ
	PIPE_TJUNC_LEFT,				//9//	左方向T字パイプ
	PIPE_TJUNC_RIGHT,				//10//	右方向T字パイプ
	PIPE_LENGTH_VALVE,				//11//	縦方向バルブ付パイプ
	PIPE_WIGHT_VALVE,				//12//	横方向バルブ付パイプ
	PIPE_LENGTH_SETTLED_VALVE,		//13//	縦方向済みバルブ付パイプ
	PIPE_WIGHT_SETTLED_VALVE,		//14//	横方向済みバルブ付パイプ

	PIPE_MAX,

	ALARM_CLOCK,					//16//	目覚まし時計


};

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
// 新しくファイルを作る場合はInit関数のみを、「main.cpp」内になる「InitStage」関数より上側に追加してください
// アップデート・描画・終了処理は、「StageBase.cpp」が勝手に追加します
void InitBlockCheckPoint(void);

// バルブとのあたり判定
bool CheckHitValve(const tCollisionData& DATA );
// すべてのバルブの数
int GetMaxValve(void);
// 操作済みになったバルブの数の取得
int GetManipulatedValve(void);
