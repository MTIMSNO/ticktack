//===================================================================================
//
// タイトル画面
//
//===================================================================================

#include "Title.h"
#include "PolygonBase.h"
#include "input.h"
#include "Data.h"
#include "Fade.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
void TitleScaling(void);
//-----------------------------------------------------------------------------------
// 構造体定義
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

tObjData			g_tTitle[MAX_TITLE];	
bool				g_bTitleBigFlag;
bool				g_bTitleSmallFlag;
bool				g_bTitleFadeFlag;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitTitle(LPDIRECT3DDEVICE9 pDevice)
{
	for(int i = 0; i < MAX_TITLE; i++)
	{
		g_tTitle[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y,0.0f);
		g_tTitle[i].fScale = 1.0f;
		g_tTitle[i].bExist = true;
		g_tTitle[i].sizeMax = c_sizeTitle00;
		g_tTitle[i].nType = i;

		if(i == 0)
		{
			g_tTitle[i].size = D3DXVECTOR2((float)SCREEN_WIDTH,(float)SCREEN_HEIGHT);
		}
		else if (i == 1)
		{
			g_tTitle[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y-SCREEN_CENTER_Y/1.35f,0.0f);
			g_tTitle[i].size = D3DXVECTOR2((float)SCREEN_WIDTH,(float)SCREEN_HEIGHT/13);
		}
		else if (i == 2)
		{
			g_tTitle[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/1.35f,0.0f);
			g_tTitle[i].size = D3DXVECTOR2((float)SCREEN_WIDTH,(float)SCREEN_HEIGHT/13);
		}

		g_tTitle[i].pAnim = c_animTitle00;

		MakePolygon(pDevice,&g_tTitle[i],TEXTURE_TITLE);
	}

	g_bTitleBigFlag = true;
	g_bTitleSmallFlag = false;
	g_bTitleFadeFlag = true;

	return S_OK;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitTitle(void)
{
	for ( int i = 0; i < MAX_TITLE; i++ )	UninitPolygonAndTexture(&g_tTitle[i]);
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateTitle(void)
{
	// 選択メニューの拡縮
	//TitleScaling();

	if ( GetKeyboardTrigger())
	{
		SetFade(FADE_OUT,60,D3DXCOLOR(0.0f,0.0f,0.0f,1.0f),0.0f);	
		g_bTitleFadeFlag = false;
	}
	else if ( g_bTitleFadeFlag == false && GetFadeOutComplete() )
	{
		SetFade(FADE_IN,60,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f),0.0f);
		SetData(DATA_MAINTRANSITION,STATE_SELECTMENU);
		g_bTitleFadeFlag = true;
	}

	// テクスチャの設定
	for ( int i = 0; i < MAX_TITLE; i++ )
	{
		SetAnimationNo(&g_tTitle[i],g_tTitle[i].nType);
		if(i== 0)		SetTexturePolygon(&g_tTitle[i]);
		else if(i == 1) SetTextureScroll(&g_tTitle[i],0.003f,0);
		else if(i == 2)	SetTextureScroll(&g_tTitle[i],-0.003f,1);
		SetVertexPolygon(&g_tTitle[i]);
	}
	

}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawTitle(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState ( D3DRS_ALPHAREF , 0x00000080 );

	for ( int i = 0; i < MAX_TITLE; i++ )
	{
		// 頂点バッファをデバイスのデータストリームにバインド
		pDevice->SetStreamSource( 0, g_tTitle[i].pD3DVtxBuff, 0, sizeof(VERTEX_2D) );

		// 頂点フォーマットの設定
		pDevice->SetFVF(FVF_VERTEX_2D);

		// テクスチャの設定
		pDevice->SetTexture( 0, g_tTitle[i].pD3DTexture );
	
		// ポリゴンの描画
		pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
	}

	// 2Dポリゴンの描画に影響するので描画が終わったら設定を元に戻す
	pDevice->SetRenderState ( D3DRS_ALPHAREF , 0x00000000 );
}

//-----------------------------------------------------------------------------------
// ポーズメニューの拡縮
//-----------------------------------------------------------------------------------
void TitleScaling(void)
{
	if ( g_bTitleBigFlag && g_bTitleSmallFlag == false )
	{
		g_tTitle[1].fScale += 0.015f;
		if ( g_tTitle[1].fScale >= 0.95f )
		{
			g_bTitleBigFlag = false;
			g_bTitleSmallFlag = true;
		}
	}
	else if ( g_bTitleSmallFlag && g_bTitleBigFlag == false )
	{
		g_tTitle[1].fScale -= 0.015f;
		if ( g_tTitle[1].fScale <= 0.65f )
		{
			g_bTitleBigFlag = true;
			g_bTitleSmallFlag = false;
		}
	}
}
