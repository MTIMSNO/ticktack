//=============================================================================
//=============================================================================

#include "FlowerBase.h"
#include "GIM_Flower_CellData.h"
#include "PolygonBase.h"
#include "input.h"
#include "Player.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define _DEBUG_

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void SetFlowerCell_StageNum( int main_stage, int sub_stage );
bool CheckHitBB_A ( D3DXVECTOR3 pos1, D3DXVECTOR2 size1, D3DXVECTOR3 pos2, D3DXVECTOR2 size2 );

//*****************************************************************************
// 構造体定義
//*****************************************************************************
typedef struct _tFlowerCell_Block{
	unsigned char	type;
	tObjData		obj;
	tCollisionData	collision;
}tFlowerCell_Block;

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tFlowerCell_Block	g_FlowerCell_Block[FLOWER_CELL_ROW][FLOWER_CELL_COLUMN];

// ステージナンバー
int				g_nFlowerGimmickNum;

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT InitFlowerGimmick(LPDIRECT3DDEVICE9 pDevice)
{
	for(int i=0; i<FLOWER_CELL_ROW; i++){
		for(int j=0; j<FLOWER_CELL_COLUMN; j++){
			// セル配置データの初期化
			g_FlowerCell_Block[i][j].type = FLOWER_NONE;
			tObjData&		tmpObj = g_FlowerCell_Block[i][j].obj;
			tCollisionData&	tmpCol = g_FlowerCell_Block[i][j].collision;

			tmpObj.pos = D3DXVECTOR3( FLOWER_CELL_SIZE_X * j, FLOWER_CELL_SIZE_Y * i, 0.0f );
			tmpObj.rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			tmpObj.fScale = 1.0f;
			tmpObj.size = D3DXVECTOR2( FLOWER_CELL_SIZE_X, FLOWER_CELL_SIZE_Y );
			tmpObj.pAnim = NULL;

			
			CreateCollision(
				&g_FlowerCell_Block[i][j].collision,
				tmpObj.pos, COLLISION_TYPE_BOX,
				FLOWER_CELL_SIZE_X/2, FLOWER_CELL_SIZE_Y/2, 0.0f
				);
				
			MakePolygon( pDevice, &tmpObj, FLOWER_CELL_TEXTURE );
			tmpObj.bExist = false;
		}
	}

	g_nFlowerGimmickNum = 0;
	SetFlower_FlowerNum(1,g_nFlowerGimmickNum);

	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void UninitFlowerGimmick(void)
{
	for(int i=0; i<FLOWER_CELL_ROW; i++){
		for(int j=0; j<FLOWER_CELL_COLUMN; j++){
			UninitPolygonAndTexture( &g_FlowerCell_Block[i][j].obj );
		}
	}
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateFlowerGimmick(void)
{
	for(int i=0; i<FLOWER_CELL_ROW; i++){
		for(int j=0; j<FLOWER_CELL_COLUMN; j++){
			SetVertexPolygon( &g_FlowerCell_Block[i][j].obj );
			SetTexturePolygon( &g_FlowerCell_Block[i][j].obj );
		}
	}
 
	// タックのときに花が成長(ステージを切り替え)
	if ( GetPlayerNumber() == NAME_TACK )
	{
		static int count = 0;
		count++;
		if ( count % 60 == 0 )
		{
			g_nFlowerGimmickNum++;
			if( g_nFlowerGimmickNum > FLOWER_NUM_SUB )
				g_nFlowerGimmickNum = 0;
		}

		SetFlower_FlowerNum(1,g_nFlowerGimmickNum);
	}

	/*
	// ボタンを押すとブロック配置が変化します
	if ( GetKeyboardPress(DIK_A) ){
		SetFlower_FlowerNum( 1, 1 );
	}
	else if ( GetKeyboardPress(DIK_S) ){
		SetFlower_FlowerNum( 1, 2 );
	}
	else if ( GetKeyboardPress(DIK_D) ){
		SetFlower_FlowerNum( 1, 3 );
	}
	*/
}

//=============================================================================
// 描画処理
//=============================================================================
void DrawFlowerGimmick(LPDIRECT3DDEVICE9 pDevice)
{
	for(int i=0; i<FLOWER_CELL_ROW; i++){
		for(int j=0; j<FLOWER_CELL_COLUMN; j++){
			tObjData& tmpObj = g_FlowerCell_Block[i][j].obj;
			if ( tmpObj.bExist == false ){
				continue;
			}

			// 頂点バッファをデバイスのデータストリームにバインド
			pDevice->SetStreamSource( 0, tmpObj.pD3DVtxBuff, 0, sizeof(VERTEX_2D) );
			// 頂点フォーマットの設定
			pDevice->SetFVF(FVF_VERTEX_2D);
			// テクスチャの設定
			pDevice->SetTexture( 0, tmpObj.pD3DTexture );
			// ポリゴンの描画
			pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
		}
	}
}

//=============================================================================
// 各ステージごとの初期設定
//=============================================================================
void SetFlower_FlowerNum( int main_stage, int sub_stage ){
	// データ範囲外を操作しないようにチェック
	if( ( main_stage > FLOWER_NUM_MAIN ) ||
		( main_stage <= 0 ) ||
		( sub_stage  > FLOWER_NUM_SUB ) ||
		( sub_stage  <= 0 ) ){
		return;
	}

	SetFlowerCell_StageNum( main_stage, sub_stage );
}

//=============================================================================
// 各ステージごとのセル配置データを反映
//=============================================================================
void SetFlowerCell_StageNum( int main_stage, int sub_stage ){
	// 配列添え字として扱う為に1減らす
	main_stage--;
	sub_stage--;

	for(int i=0; i<FLOWER_CELL_ROW; i++){
		for(int j=0; j<FLOWER_CELL_COLUMN; j++){
			SetFlowerCellType( i, j, c_FlowerData[main_stage][sub_stage][ i * FLOWER_CELL_COLUMN + j ]);
		}
	}
}

//=============================================================================
// チップ設定処理
//=============================================================================
void SetFlowerCellType( int row, int column, int type ){
	// データ範囲外を操作しないようにチェック
	if( ( row >= FLOWER_CELL_ROW ) ||
		( column >= FLOWER_CELL_COLUMN ) ||
		( type < FLOWER_NONE ) ||
		( type >= FLOWER_MAX ) ){
		return;
	}

	// 新しいセルデータを反映し、ブロックがある場合はテクスチャ変更
	if( ( g_FlowerCell_Block[row][column].type = type ) > FLOWER_NONE ){
		SetPolygonTextureRect( g_FlowerCell_Block[row][column].obj, c_Flower_CellData[ type ] );
		g_FlowerCell_Block[row][column].obj.bExist = true;
	}
	else{
		g_FlowerCell_Block[row][column].obj.bExist = false;
	}
}


//=============================================================================
// 花との当たり判定処理
//=============================================================================
bool CheckHit_FlowerCell( const tCollisionData& DATA ){
	for(int i=0; i<FLOWER_CELL_ROW; i++){
		for(int j=0; j<FLOWER_CELL_COLUMN; j++){
			if(!(g_FlowerCell_Block[i][j].type == 0 )){
				continue;
			}

			// 花があるとき
			if( CheckCollision( DATA, g_FlowerCell_Block[i][j].collision ) ){
				return true;
			}
		}
	}
	return false;
}

//=============================================================================
// フラワーギミックの中心座標の取得
//=============================================================================
D3DXVECTOR3 GetFlowerGimmickPos(int x,int y)
{
	return g_FlowerCell_Block[y][x].obj.pos;
}

//=============================================================================
// フラワーギミックの中心座標の設定
//=============================================================================
void SetFlowerGimmickPos(int x,int y,D3DXVECTOR3 pos)
{
	g_FlowerCell_Block[y][x].obj.pos = pos;
}