#include "BlockTree.h"
#include "Player.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PreStartBlockTree(void);
void UpdateBlockTree(void);
void SetVertexTreePolygon( tObjData* obj );
TCHAR* GetBlockTree_TableFileName(void);

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*				g_Stage_BlockTree[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];

//=============================================================================
// 初期化処理
// ※起動時に一回だけやって欲しい処理
//=============================================================================
void InitBlockTree(void){
	// ここで「StageBase.cpp」内で用意した、各マスのポリゴンへのポインタを取得する
	// ※二番目の引数は、「StageBase.h」『列挙型定義』に追加した、『STAGE_LAYER_〜〜』を入れる
	// 　このサンプルでは地形より奥に表示しているので、『STAGE_LAYER_BACK001』を設定してます
	LinkStage_CellArray( g_Stage_BlockTree, STAGE_LAYER_TREE );

	// ここで「StageBase.cpp」から、
	// 「BlockTree.cpp」内にある「PreStartBlockTree」「UpdateBlockTree」「GetBlockTree_TableFileName」関数にアクセスできるようにする
	// ※関数名を変更した場合は、ここの引数名も変更する
	// 　レイヤー設定は、上と同じものを使う
	LinkStage_PreStartFunc( PreStartBlockTree, STAGE_LAYER_TREE );
	LinkStage_UpdateFunc( UpdateBlockTree, STAGE_LAYER_TREE );
	LinkStage_TableFunc( GetBlockTree_TableFileName, STAGE_LAYER_TREE );

	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			g_Stage_BlockTree[i][j]->obj.size = D3DXVECTOR2(128.0f,256.0f);
			g_Stage_BlockTree[i][j]->obj.sizeMax = D3DXVECTOR2(128.0f,256.0f);
			SetVertexTreePolygon(&g_Stage_BlockTree[i][j]->obj);
		}
}

//=============================================================================
// ステージ開始時の処理
// ※各ステージの開始時に毎回やって欲しい処理
//=============================================================================
void PreStartBlockTree(void){
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockTree(void)
{
}

//=============================================================================
// 配置データファイル名取得
// ※「BlockTree」と「BLOCKSAMPLE」の部分を、変更してね
//=============================================================================
TCHAR* GetBlockTree_TableFileName(void){
	return BLOCKTREE_CELLTABLE_FILENAME;
}

//=============================================================================
// 頂点データの設定
//=============================================================================
void SetVertexTreePolygon( tObjData* obj )
{
	VERTEX_2D* pVtx;

	// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
	obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

	// 基本値を設定
	obj->fRadius = sqrtf(obj->size.x * obj->size.x + obj->size.y * obj->size.y) / 2.0f;
	obj->fBaseAngle = atan2f(obj->size.x, obj->size.y);

	// 頂点座標の設定
	pVtx[0].vtx.x = obj->pos.x - sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[0].vtx.y = obj->pos.y - cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[0].vtx.z = 0.0f;
	pVtx[1].vtx.x = obj->pos.x + sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[1].vtx.y = obj->pos.y - cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[1].vtx.z = 0.0f;
	pVtx[2].vtx.x = obj->pos.x - sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[2].vtx.y = obj->pos.y + cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[2].vtx.z = 0.0f;
	pVtx[3].vtx.x = obj->pos.x + sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[3].vtx.y = obj->pos.y + cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[3].vtx.z = 0.0f;
	

	// 表示ずれ対策（これを行っても、テクスチャサイズの問題でずれる可能性はある）
	for( int i = 0; i <= 3; i++ )
	{
		pVtx[i].vtx.x -= 0.5f;
		pVtx[i].vtx.y -= 0.5f;
		D3DXCOLOR temp = pVtx[i].diffuse;
		temp.a = obj->fAlpha;
		pVtx[i].diffuse = temp;
	}

	// 頂点データをアンロックする
	obj->pD3DVtxBuff->Unlock();
}

