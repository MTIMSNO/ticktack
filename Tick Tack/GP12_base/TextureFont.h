//=============================================================================
//
// テクスチャーフォント処理 [textureFont.h]
//
//=============================================================================
#pragma once

#include "main.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
typedef struct _tFontTextureCache{
	UINT				code;
	LPDIRECT3DTEXTURE9	pD3DTexture;
}tFontTextureCache;

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
HRESULT InitTextureFont(LPDIRECT3DDEVICE9 pDevice);
void UninitTextureFont(void);
int CreateFontTexture(const TCHAR* TEXT);
LPDIRECT3DTEXTURE9 GetFontTexturePoint(const TCHAR* TEXT);
