//=============================================================================
//
// ステージ地形ブロック処理
//
//=============================================================================

#pragma once

#include "main.h"
#include "StageBase.h"
//*****************************************************************************
// 列挙型定義
//*****************************************************************************

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define BLOCKMOVE_CELLTABLE_FILENAME	_T("BlockMove")

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void InitBlockMove(void);
//tStageCell GetBeforeBlockMove(int layer,int i,int j);