//=============================================================================
//
// プレイヤー処理 [Player.cpp]
//
//=============================================================================

#include "Player.h"
#include "polygonAnim.h"
#include "input.h"

#include "BlockCheckPoint.h"
#include "BlockFlower.h"
#include "BlockVine.h"
#include "BlockSpiderNet.h"

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
// プレイヤーオブジェクト
tObjData				g_tPlayer[MAX_PLAYER];
// プレイヤー移動
void MovePlayer(void);
// プレイヤーテクスチャアニメーション
void SetPlayerAnimation( tObjData* obj );
// プレイヤーテクスチャの設定
void SetTexturePlayer( tObjData* obj );
// アニメーション中かどうか
bool IsPlayerAnimation( tObjData* obj, int no /*=-1*/ );
// キャラクターアニメーション
void PlayerAnimation(void);
// キャラクター切り替え
void ChangePlayer(void);
// ギミックアクション
void GimmickAction(void);


#ifdef _DEBUG
// プレイヤー情報の表示
void DrawPlayerInfo(void);
#endif

//*****************************************************************************
// 構造体定義
//*****************************************************************************

typedef struct _tPlayerFlag
{
	// 各方向移動に関するフラグ
	bool		MoveRight;
	bool		MoveLeft;
	bool		MoveUp;

	// 各方向アニメーションフラグ
	bool		AnimRight;
	bool		AnimLeft;
	bool		AnimUp;

	// ギミックのアクションフラグ
	bool		ActValve;
	bool		ActSpiderNet;

	// ギミックのアニメーションフラグ
	bool		AnimValve;

	// つるに掴まってます
	bool		HungVine;

}tPlayerFlag;

//*****************************************************************************
// グローバル変数
//*****************************************************************************

// 1フレーム前のプレイヤー座標

// プレイヤー座標保持用(１フレーム前)
D3DXVECTOR3				g_dPlayerBeforePos[MAX_PLAYER];
// 操作プレイヤーナンバー
int						g_nPlayerNum;
// プレイヤーの移動スピード
D3DXVECTOR2				g_dPlayerSpeed;		
// フラグ一式
tPlayerFlag				g_tPF;

bool					g_bPlayerStop;

// あたり判定用
tCollisionData			g_tPlayerCollision[MAX_PLAYER];

// TEXTURE
TCHAR*					PlayerFileName[] = {
	_T("tick.png"),
	_T("tack.png")
};

// プレイヤーが切り替わったか？
bool	g_PlayerChange=false;
// プレイヤーは移動してもいい？
bool	g_PlayerMovable=true;	// 作業中なので、まだ未使用

// 掴まっているブツへのポインタ
tObjData*	gp_PlayerHungTarget=NULL;
float		g_PlayerHungLength=0.0f;

#ifdef _DEBUG
LPD3DXFONT				g_pD3DXFont2 = NULL;			// フォントへのポインタ
#endif

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT InitPlayer(LPDIRECT3DDEVICE9 pDevice)
{
	// プレイヤーデータの初期設定(共通しない部分)
	g_tPlayer[0].pos = D3DXVECTOR3(250.0f,300.0f,0.0f);
	g_tPlayer[0].sizeMax = c_sizePlayer00;
	g_tPlayer[0].pAnim = c_animPlayer00;

	g_tPlayer[1].pos = D3DXVECTOR3(220.0f,300.0f,0.0f);
	g_tPlayer[1].sizeMax = c_sizePlayer01;
	g_tPlayer[1].pAnim = c_animPlayer01;

	// プレイヤーデータの初期設定(共通する部分)
	for ( int i = 0; i < MAX_PLAYER; i++ )
	{
		// あたり判定用データの初期化
		CreateCollision(&g_tPlayerCollision[i],g_tPlayer[i].pos,
						COLLISION_TYPE_CIRCLE,
						(float)PLAYER_SIZE_X/2,(float)PLAYER_SIZE_Y/2);

		// プレイヤーデータ
		g_tPlayer[i].rot = D3DXVECTOR3(0.0f,0.0f,0.0f);
		g_tPlayer[i].fScale = 1.0f;
		g_tPlayer[i].size = D3DXVECTOR2((float)PLAYER_SIZE_X,(float)PLAYER_SIZE_Y);
		g_tPlayer[i].bExist = true;	
		g_dPlayerBeforePos[i] = D3DXVECTOR3(0.0f,0.0f,0.0f);

		// ポリゴンの作成
		MakePolygon(pDevice,&g_tPlayer[i],PlayerFileName[i]);
	}

	// フラグの初期化処理
	g_tPF.MoveLeft = g_tPF.MoveRight = g_tPF.MoveUp = g_tPF.ActValve =
	g_tPF.AnimLeft = g_tPF.AnimRight = g_tPF.AnimUp = g_tPF.AnimValve =
	g_tPF.HungVine = false;
	g_tPF.ActSpiderNet = true;

	// 各変数の初期化

	g_bPlayerStop = false;

	g_nPlayerNum = 0;
	g_dPlayerSpeed = D3DXVECTOR2(0.0f,0.0f);

#ifdef _DEBUG
	// 情報表示用フォントを設定
	D3DXCreateFont( pDevice, 18, 0, 0, 0, FALSE, SHIFTJIS_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Terminal"), &g_pD3DXFont2 );
#endif

	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void UninitPlayer(void)
{
	for ( int i = 0; i < MAX_PLAYER; i++ )	UninitPolygonAndTexture(&g_tPlayer[i]);
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdatePlayer(void)
{
	// 現在のプレイヤーの位置を保持
	for ( int i = 0; i < MAX_PLAYER; i++ )	g_dPlayerBeforePos[i] = g_tPlayer[i].pos;
	// プレイヤー移動
	MovePlayer();
	// プレイヤーのアニメーション
	PlayerAnimation();
	// プレイヤー切り替え
	ChangePlayer();
	// ギミックアクション
	GimmickAction();
	
	for ( int i = 0; i < MAX_PLAYER; i++ )
	{
		// ポリゴンの座標セット
		SetVertexPolygon(&g_tPlayer[i]);
		// アニメーション
		SetTexturePlayer(&g_tPlayer[i]);
	}
}

//=============================================================================
// 描画処理
// 選択中のプレイヤーを手前に描画する
// (無理やりやってるからほかにいい方法あったら教えてほしい…)
//=============================================================================
void DrawPlayer(LPDIRECT3DDEVICE9 pDevice)
{
	// 頂点バッファをデバイスのデータストリームにバインド
	pDevice->SetStreamSource( 0, g_tPlayer[!(g_nPlayerNum)].pD3DVtxBuff, 0, sizeof(VERTEX_2D) );
	// 頂点フォーマットの設定
	pDevice->SetFVF( FVF_VERTEX_2D );
	// テクスチャの設定
	pDevice->SetTexture( 0, g_tPlayer[!(g_nPlayerNum)].pD3DTexture );
	// ポリゴンの描画
	pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );

	// 頂点バッファをデバイスのデータストリームにバインド
	pDevice->SetStreamSource( 0, g_tPlayer[g_nPlayerNum].pD3DVtxBuff, 0, sizeof(VERTEX_2D) );
	// 頂点フォーマットの設定
	pDevice->SetFVF( FVF_VERTEX_2D );
	// テクスチャの設定
	pDevice->SetTexture( 0, g_tPlayer[g_nPlayerNum].pD3DTexture );
	// ポリゴンの描画
	pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );

#ifdef _DEBUG
	//DrawPlayerInfo();
#endif
}

//=============================================================================
// プレイヤーの移動
//=============================================================================
void MovePlayer(void)
{
	D3DXVECTOR2	Adjust(0.0f,0.0f);

	if( g_tPF.HungVine ){
		// つるに掴まっている場合
		tObjData& tmpObj = *gp_PlayerHungTarget;
		g_tPlayer[g_nPlayerNum].pos.x = 
		g_tPlayer[!g_nPlayerNum].pos.x = tmpObj.pos.x + g_PlayerHungLength * sinf(tmpObj.rot.z) * tmpObj.fScale;
		g_tPlayer[g_nPlayerNum].pos.y = 
		g_tPlayer[!g_nPlayerNum].pos.y = tmpObj.pos.y + g_PlayerHungLength * cosf(tmpObj.rot.z) * tmpObj.fScale;

		// つるから離れる時のボタン
		if( GetKeyboardTrigger(DIK_Z) ){
			// つるから離れる時の処理
			// ※現状はその場から垂直落下、ジャンプさせたいなら速度を弄る
			g_dPlayerSpeed = D3DXVECTOR2(0.0f,0.0f);
			g_tPF.HungVine = false;
		}
		else{
			// つるの上り下り
			if( GetKeyboardPress(DIK_UP) ){
				ADDWITHLIMIT( g_PlayerHungLength, -1.0f, tmpObj.size.y / 10.0f );
			}
			else if( GetKeyboardPress(DIK_DOWN) ){
				ADDWITHLIMIT( g_PlayerHungLength, 1.0f, tmpObj.size.y * (1.0f - 0.1f) );
			}

			// 掴まり続けているので、重力処理とキー入力による移動はしない
			return;
		}
	}
	else if( g_nPlayerNum == NAME_TICK && GetKeyboardTrigger(DIK_UP) && CheckHitBlockVine( &gp_PlayerHungTarget ) ){
		// 掴まりフラグを立てる
		g_tPF.HungVine = true;
		// 出来るだけ違和感がない位置に掴まる
		AdjustPos_OnHungVine( &g_PlayerHungLength, &g_tPlayer[g_nPlayerNum].pos, *gp_PlayerHungTarget );
		tObjData& tmpObj = *gp_PlayerHungTarget;

		// つるに掴まったので、重力処理とキー入力移動は中断
		return;
	}

	if( g_nPlayerNum == NAME_TICK && CheckHitFlower( g_tPlayerCollision[g_nPlayerNum] ) ){
		// プレイヤーがチック・花に触れている状態
		if( g_tPF.MoveUp ){
			// 上下速度を無くす
			g_dPlayerSpeed.y = 0.0f;
			// 上へ移動
			g_tPlayer[g_nPlayerNum].pos.y -= 2.5f;
			Adjust.y += 20.0f;
		}
		else if( GetKeyboardPress( DIK_DOWN ) ){
			// 下へ移動
			g_tPlayer[g_nPlayerNum].pos.y += 2.5f;
			Adjust.y -= 20.0f;
		}
	}
	else{
		//***** 自由落下 *****//
		ADDWITHLIMIT( g_dPlayerSpeed.y, PLAYER_SPEED_GRAVITY_Y, PLAYER_SPEED_MAX_Y );
	}

	//***** 左右・上移動 *****//
	if( !(g_tPF.MoveUp) ){
		// 上へ移動していない時
		if (g_tPF.MoveRight && !(g_tPF.MoveLeft) )
		{
			// スピードを設定
			if( g_dPlayerSpeed.x < PLAYER_SPEED_MAX_X ){
				ADDWITHLIMIT( g_dPlayerSpeed.x, PLAYER_SPEED_X, PLAYER_SPEED_MAX_X );
			}
			Adjust.x -= 50.0f;
		}
		else if (g_tPF.MoveLeft && !(g_tPF.MoveRight) )
		{
			// スピードを設定
			if( g_dPlayerSpeed.x > -PLAYER_SPEED_MAX_X ){
				ADDWITHLIMIT( g_dPlayerSpeed.x, -PLAYER_SPEED_X, -PLAYER_SPEED_MAX_X );
			}
			Adjust.x += 50.0f;
		}
	}

	// X方向の摩擦補正
	if( g_dPlayerSpeed.x > 0.0f ){
		ADDWITHLIMIT( g_dPlayerSpeed.x, -PLAYER_SPEED_RESIST_X, 0.0f );
	}
	else if( g_dPlayerSpeed.x < 0.0f ){
		ADDWITHLIMIT( g_dPlayerSpeed.x, PLAYER_SPEED_RESIST_X, 0.0f );
	}

	
	// 上方向に加速させるなら設定する
	// 終端速度のキャップ
	if( g_dPlayerSpeed.y < -PLAYER_SPEED_MAX_UP ){
		g_dPlayerSpeed.y = -PLAYER_SPEED_MAX_UP;
	}
	

	// プレイヤーの移動
	g_tPlayer[g_nPlayerNum].pos.x += g_dPlayerSpeed.x;
	g_tPlayer[g_nPlayerNum].pos.y += g_dPlayerSpeed.y;

	//***** 選択外プレイヤーの追従設定 *****//
	if( D3DXVec2LengthSq( &Adjust ) > 0.0f ){
		// 位置補正が存在する（移動した）場合のみ処理
		D3DXVECTOR2 From =	D3DXVECTOR2(g_tPlayer[!(g_nPlayerNum)].pos.x,g_tPlayer[!(g_nPlayerNum)].pos.y);
		D3DXVECTOR2 To	=	D3DXVECTOR2(g_tPlayer[g_nPlayerNum].pos.x ,g_tPlayer[g_nPlayerNum].pos.y ) + Adjust;
		SetTween(From,To,30);

		if ( !(isEnd()))	
		{
			g_tPlayer[!(g_nPlayerNum)].pos.x = GetCurrentVec2Value().x;
			g_tPlayer[!(g_nPlayerNum)].pos.y = GetCurrentVec2Value().y;
		}
	}
}

//=============================================================================
// キャラクターアニメーション
//=============================================================================
void PlayerAnimation(void)
{
	//***** 移動アニメーション *****//
	if (GetKeyboardPress(DIK_RIGHT) && !(g_tPF.AnimLeft) && !(g_tPF.AnimRight) && !(g_tPF.AnimUp))
	{
		for ( int i = 0; i < MAX_PLAYER; i++ )	SetAnimationNo(&g_tPlayer[i],ANIM_MOVE_RIGHT);
		g_tPF.AnimRight = g_tPF.MoveRight = true;
	}
	else if (GetKeyboardRelease(DIK_RIGHT) && !(g_tPF.AnimLeft) && g_tPF.AnimRight && !(g_tPF.AnimUp))
	{
		for ( int i = 0; i < MAX_PLAYER; i++ )	SetAnimationNo(&g_tPlayer[i],ANIM_MOVE_RIGHT);
		g_tPF.AnimRight = g_tPF.MoveRight = false;
	}
	else if (GetKeyboardPress(DIK_LEFT) && !(g_tPF.AnimLeft) && !(g_tPF.AnimRight) && !(g_tPF.AnimUp))
	{
		for ( int i = 0; i < MAX_PLAYER; i++ )	SetAnimationNo(&g_tPlayer[i],ANIM_MOVE_LEFT);
		g_tPF.AnimLeft = g_tPF.MoveLeft = true;
	}
	else if (GetKeyboardRelease(DIK_LEFT) && g_tPF.AnimLeft && !(g_tPF.AnimRight) && !(g_tPF.AnimUp))
	{
		for ( int i = 0; i < MAX_PLAYER; i++ )	SetAnimationNo(&g_tPlayer[i],ANIM_MOVE_LEFT);
		g_tPF.AnimLeft = g_tPF.MoveLeft = false;
	}
	else if (GetKeyboardPress(DIK_UP) && !(g_tPF.AnimLeft) && !(g_tPF.AnimRight) && !(g_tPF.AnimUp) && CheckHitFlower( g_tPlayerCollision[g_nPlayerNum] ))
	{
		for ( int i = 0; i < MAX_PLAYER; i++ )	SetAnimationNo(&g_tPlayer[i],ANIM_MOVE_UP);
		g_tPF.AnimUp = g_tPF.MoveUp = true;
	}
	else if (GetKeyboardRelease(DIK_UP) && !(g_tPF.AnimLeft) && !(g_tPF.AnimRight) && g_tPF.AnimUp)
	{
		for ( int i = 0; i < MAX_PLAYER; i++ )	SetAnimationNo(&g_tPlayer[i],ANIM_MOVE_UP);
		g_tPF.AnimUp = g_tPF.MoveUp = false;
		//g_dPlayerSpeed.y = 0.0f;
	}
}

//=============================================================================
// ギミックに対するアクション
//=============================================================================
void GimmickAction(void)
{
	// ***** バルブ ***** //
	if (GetKeyboardPress(DIK_RETURN) && CheckHitValve(g_tPlayerCollision[g_nPlayerNum]) && !(g_tPF.AnimValve)){
		g_tPF.AnimValve = true;
		SetAnimationNo(&g_tPlayer[g_nPlayerNum],ANIM_GIM_PIPE);
		SetAnimationNo(&g_tPlayer[!(g_nPlayerNum)],ANIM_GIM_PIPEWAIT);
	}
	else if (GetKeyboardPress(DIK_RETURN) && CheckHitValve(g_tPlayerCollision[g_nPlayerNum]) && g_tPF.AnimValve && !(g_tPF.ActValve)){
		if(!(IsPlayerAnimation(&g_tPlayer[g_nPlayerNum],ANIM_GIM_PIPE))) g_tPF.ActValve = true;
		SetSound(SOUND_PLAY,SE_VALVE);
	}
	else{
		g_tPF.ActValve = false;
		g_tPF.AnimValve = false;
		SetSound(SOUND_STOP,SE_VALVE);
	}


	// ***** 蜘蛛の巣 ***** //
	if (CheckHitSpiderNet(g_tPlayerCollision[g_nPlayerNum]) && g_tPF.ActSpiderNet)
	{
		SetSpiderNet();
		g_tPF.ActSpiderNet = false;
	}
	else if (CheckHitSpiderNet(g_tPlayerCollision[g_nPlayerNum]) != true && g_tPF.ActSpiderNet != true)
	{
		g_tPF.ActSpiderNet = true;
	}
}

//=============================================================================
// プレイヤーの中心座標の取得
//=============================================================================
D3DXVECTOR3 GetPlayerPos(int num)
{
	return g_tPlayer[num].pos;
}

//=============================================================================
// プレイヤーの中心座標の設定
//=============================================================================
void SetPlayerPos(int num, D3DXVECTOR3 pos)
{
	g_tPlayer[num].pos = pos;
}

//=============================================================================
// プレイヤーの１フレーム前の中心座標の取得
//=============================================================================
D3DXVECTOR3 GetPlayerBeforePos(int num)
{
	return g_dPlayerBeforePos[num];
}

//=============================================================================
// プレイヤーの移動スピードの取得
//=============================================================================
D3DXVECTOR2 GetPlayerSpeed(void)
{
	return g_dPlayerSpeed;
}
//=============================================================================
// プレイヤーの移動フラグの取得
//=============================================================================
bool GetFlagIsStop(void)
{
	return 	g_bPlayerStop;
}
//=============================================================================
// tCollisionDataの取得
//=============================================================================
tCollisionData GetPlayerCollision(int num)
{
	return g_tPlayerCollision[num];
}

//=============================================================================
// 現在選択中のプレイヤーナンバーの取得
//=============================================================================
int GetPlayerNumber(void)
{
	return g_nPlayerNum;
}

//=============================================================================
// フラグの取得
//=============================================================================
bool GetPlayerFlag(int FlagNum)
{
	switch ( FlagNum )
	{
	case ACT_VALVE:
		return g_tPF.ActValve;
	}
	return false;
}
//=============================================================================
// プレイヤーの切り替え
//=============================================================================
void ChangePlayer(void)
{
	if(g_tPF.AnimValve || g_tPF.ActValve ) return;

	g_PlayerChange = false;
	if ( GetKeyboardTrigger(DIK_SPACE) )
	{
		g_PlayerChange = true;
		int TempNum = g_nPlayerNum;
		g_nPlayerNum++;
		// MAX_PLAYERになると0へ戻す
		if ( g_nPlayerNum >= MAX_PLAYER )			
			g_nPlayerNum = 0;
		
		D3DXVECTOR3 TempPos = g_tPlayer[TempNum].pos;
		g_tPlayer[TempNum].pos = g_tPlayer[g_nPlayerNum].pos;
		g_tPlayer[g_nPlayerNum].pos = TempPos;
		
	}
}

//=============================================================================
// テクスチャアニメーション
//=============================================================================
void SetPlayerAnimation( tObjData* obj )
{
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	// アニメーション処理
	int flag = pAnim[ obj->nAnimPattern ].flag;

	// アニメーションカウント
	if ( g_tPF.MoveLeft || g_tPF.MoveRight || g_tPF.MoveUp || g_tPF.AnimValve)	obj->nAnimCount++;

	// 各アニメーションに割り当てたフレームまで行くと次のアニメーションへ
	if( obj->nAnimCount >= pAnim[ obj->nAnimPattern ].frame )
	{	
		obj->nAnimPattern++;
		obj->nAnimCount = 0;
		// 次の絵は何枚目かを決定する
		if( flag == ANIM_FLAG_STOP )
		{
			obj->bAnim = false;
		} else if( flag == ANIM_FLAG_LOOP )
		{
			obj->nAnimCount = 0;
			obj->nAnimPattern = 0;
		} 
	}
}

//=============================================================================
// アニメーション中かどうか
//=============================================================================
bool IsPlayerAnimation( tObjData* obj, int no /*=-1*/ )
{
	// どのアニメでもいいけど、アニメしているかどうかを判定
	if( no == -1 ) return obj->bAnim;

	// 指定のアニメナンバーでアニメしているかどうか
	if( obj->bAnim && obj->nAnimNo == no )
	{
		return true;
	}
	return false;
}

//=============================================================================
// テクスチャ設定
//=============================================================================
void SetTexturePlayer( tObjData* obj )
{
	// アニメデータがない場合何もしない
	if( obj->pAnim == NULL ) return;

	SetPlayerAnimation( obj );
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	// テクスチャ座標設定
	{
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		// テクスチャ座標の計算
		RECT rect = pAnim[ obj->nAnimPattern ].pos;	// 場所
		float& x = obj->sizeMax.x;	// 大きさを別名に。（長いから）
		float& y = obj->sizeMax.y;

		pVtx[0].tex = D3DXVECTOR2( rect.left / x , rect.top / y );
		pVtx[1].tex = D3DXVECTOR2( rect.right / x, rect.top / y );
		pVtx[2].tex = D3DXVECTOR2( rect.left / x , rect.bottom / y );
		pVtx[3].tex = D3DXVECTOR2( rect.right / x, rect.bottom / y );

		// アンロック
		obj->pD3DVtxBuff->Unlock();
	}
}

//=============================================================================
// プレイヤーの速度ベクトル加算
//=============================================================================
void AddPlayerSpeed( D3DXVECTOR3 speed )
{
	D3DXVECTOR2 tmpSpeed( speed.x, speed.y );
	g_dPlayerSpeed += tmpSpeed;
}

//=============================================================================
// プレイヤーの速度ベクトル設定
//=============================================================================
void SetPlayerSpeed( D3DXVECTOR3 speed )
{
	D3DXVECTOR2 tmpSpeed( speed.x, speed.y ), tmp = g_dPlayerSpeed;
	g_dPlayerSpeed = tmpSpeed;
}

//=============================================================================
// プレイヤーの重力方向速度ベクトル消去
//=============================================================================
void ErazePlayerSpeed_Gravity(void)
{
	g_dPlayerSpeed.y = 0.0f;
}

//=============================================================================
// プレイヤーが切り替わったか？
//=============================================================================
bool GetPlayerChange(void)
{
	return g_PlayerChange;
}

//=============================================================================
// 再初期化
//=============================================================================
void ReInitPlayer(void)
{
	g_tPlayer[0].pos = D3DXVECTOR3(250.0f,300.0f,0.0f);
	g_tPlayer[1].pos = D3DXVECTOR3(220.0f,300.0f,0.0f);

	// プレイヤーデータの初期設定(共通する部分)
	for ( int i = 0; i < MAX_PLAYER; i++ )
	{
		// あたり判定用データの初期化
		CreateCollision(&g_tPlayerCollision[i],g_tPlayer[i].pos,
						COLLISION_TYPE_CIRCLE,
						(float)PLAYER_SIZE_X/2,(float)PLAYER_SIZE_Y/2);

		// プレイヤーデータ
		g_tPlayer[i].rot = D3DXVECTOR3(0.0f,0.0f,0.0f);
		g_tPlayer[i].fScale = 1.0f;
		g_tPlayer[i].size = D3DXVECTOR2((float)PLAYER_SIZE_X,(float)PLAYER_SIZE_Y);
		g_tPlayer[i].bExist = true;	
		g_dPlayerBeforePos[i] = D3DXVECTOR3(0.0f,0.0f,0.0f);
	}

	// フラグの初期化処理
	g_tPF.MoveLeft = g_tPF.MoveRight = g_tPF.MoveUp = g_tPF.ActValve =
	g_tPF.AnimLeft = g_tPF.AnimRight = g_tPF.AnimUp = g_tPF.AnimValve =
	g_tPF.HungVine = false;

	// 各変数の初期化

	g_bPlayerStop = false;

	g_nPlayerNum = 0;
	g_dPlayerSpeed = D3DXVECTOR2(0.0f,0.0f);
}

#ifdef _DEBUG
//=============================================================================
// プレイヤー情報の表示
//=============================================================================
void DrawPlayerInfo(void)
{
	RECT rect = { 10, 20, SCREEN_WIDTH, SCREEN_HEIGHT };
	TCHAR str[256];

	if ( g_nPlayerNum == NAME_TICK )		wsprintf( str, _T("PlayerName : TICK"));
	else if ( g_nPlayerNum == NAME_TACK )	wsprintf( str, _T("PlayerName : TACK"));

	// テキスト描画
	g_pD3DXFont2->DrawText( NULL, str, -1, &rect, DT_LEFT, D3DCOLOR_ARGB( 0xff, 0x00, 0x00, 0x00 ) );
}
#endif
