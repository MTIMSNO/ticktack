//----------------------------------------
// ライティング
// 2013.11.24
// 作成：fujimura
//----------------------------------------

#ifndef ___CAMERA_H___
#define ___CAMERA_H___

#include "main.h"
#include "mesh.h"

// プロトタイプ宣言
HRESULT InitCamera( LPDIRECT3DDEVICE9 pDevice );
void UpdateCamera( LPDIRECT3DDEVICE9 pDevice );

#endif