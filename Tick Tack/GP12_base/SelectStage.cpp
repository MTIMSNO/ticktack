//===================================================================================
//
// ステージ選択
//
//===================================================================================

#include "SelectStage.h"
#include "Fade.h"
#include "Player.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
void ChangeStage(void);
void SetSelectAnimation(tObjData* obj);
void SetTextureSelect(tObjData* obj);
void SelectNumberMove(void);
void SetPlayerMove(int state);

//-----------------------------------------------------------------------------------
// 列挙型定義
//-----------------------------------------------------------------------------------

enum SELECTSTAGE
{
	STYPE_PLAYER01,
	STYPE_PLAYER02,
	STYPE_FLOOR,
	STYPE_NAMBER,
};

enum NUMBERMOVE
{
	MOVE_NONE,
	MOVE_LEFT,
	MOVE_RIGHT,
};

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

tSelectStage		tStage;		

// TEXTURE
TCHAR*				SelectFileName[] = {
	_T("tick.png"),
	_T("tack.png"),
	_T("StageSelect.png"),
	_T("StageSelect.png"),
};

int					NumberMove;
bool				NumberChange;
bool				g_bPlayerAnim;

// タイトルから遷移してきたときに初期位置まで移動する
bool				g_bSetPlayerPos;
// 12からさらに右へ押した時の演出よう
bool				g_bSetEndPlayerPos;

#ifdef _DEBUG
LPD3DXFONT				g_pSelectInfo = NULL;			// フォントへのポインタ
#endif

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
void InitSelectStage(LPDIRECT3DDEVICE9 pDevice)
{
	tStage.MainStageNum = GetData(DATA_SELECTMAIN);
	tStage.SubStageNum = GetData(DATA_SELECTSUB);

	// タイプ設定
	for(int i = 0; i < MAX_SELECTOBJ; i++)
	{
		tStage.tSelectObject[i].bExist = true;
		tStage.tSelectObject[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y+100.0f,0.0f);
		tStage.tSelectObject[i].rot = D3DXVECTOR3(0.0f,0.0f,0.0f);
		tStage.tSelectObject[i].sizeMax = c_sizeSelect00;
		tStage.tSelectObject[i].pAnim = c_animSelect00;
		tStage.tSelectObject[i].fScale = 1.0f;
		SetAnimationNo(&tStage.tSelectObject[i],i);


		if(i <= 11)
		{
			tStage.tSelectObject[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X + (450*i),(float)SCREEN_CENTER_Y,0.0f);
			tStage.tSelectObject[i].nType = STYPE_NAMBER;	
			tStage.tSelectObject[i].size = D3DXVECTOR2(400,300);
		}
		else if(i == 12)
		{
			tStage.tSelectObject[i].nType = STYPE_PLAYER01;
			tStage.tSelectObject[i].pos = D3DXVECTOR3((float)-100.0f,(float)SCREEN_CENTER_Y+100.0f,0.0f);
			tStage.tSelectObject[i].sizeMax = c_sizePlayer00;
			tStage.tSelectObject[i].size = D3DXVECTOR2(75.0f,75.0f);
			tStage.tSelectObject[i].pAnim = c_animPlayer00;
			SetAnimationNo(&tStage.tSelectObject[i],ANIM_MOVE_RIGHT);
		}
		else if(i == 13)
		{
			tStage.tSelectObject[i].nType = STYPE_PLAYER02;
			tStage.tSelectObject[i].pos = D3DXVECTOR3((float)-150.0f,(float)SCREEN_CENTER_Y+100.0f,0.0f);
			tStage.tSelectObject[i].sizeMax = c_sizePlayer01;
			tStage.tSelectObject[i].size = D3DXVECTOR2(75.0f,75.0f);
			tStage.tSelectObject[i].pAnim = c_animPlayer01;
			SetAnimationNo(&tStage.tSelectObject[i],ANIM_MOVE_RIGHT);
		}

		MakePolygon(pDevice,&tStage.tSelectObject[i],SelectFileName[tStage.tSelectObject[i].nType]);
	}

	NumberMove = MOVE_NONE;
	NumberChange = false;
	g_bPlayerAnim = false;
	g_bSetPlayerPos = true;
	g_bSetEndPlayerPos = false;

	#ifdef _DEBUG
	// 情報表示用フォントを設定
	D3DXCreateFont( pDevice, 18, 0, 0, 0, FALSE, SHIFTJIS_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Terminal"), &g_pSelectInfo );
	#endif
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateSelectStage(void)
{
	if(g_bSetPlayerPos != false)	SetPlayerMove(MOVE_RIGHT);
	else if (g_bSetEndPlayerPos != false) SetPlayerMove(1);
	else
	{
		ChangeStage();
		SelectNumberMove();

		// ステージ決定
		if ( GetKeyboardTrigger(DIK_RETURN) && NumberChange != true)
		{
			// SE再生
			SetSound(SOUND_PLAY,SE_DECISION);
			// フェード設定
			SetFade(FADE_OUT,60,D3DXCOLOR(0.0f,0.0f,0.0f,1.0f),0.0f);
			// ステージの設定
			PreStartStage(tStage.MainStageNum,tStage.SubStageNum);

			ReInitPlayer();
			HideSpiderNet();
		}
		else if( GetFadeOutComplete() ) 
		{
			// フェード設定
			SetFade(FADE_IN,60,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f),0.0f);
			// データの設定
			SetData(DATA_SELECTMAIN,tStage.MainStageNum);
			SetData(DATA_SELECTSUB,tStage.SubStageNum);
			SetData(DATA_MAINTRANSITION,STATE_GAME);
		}
	}

	for(int i = 0; i < MAX_SELECTOBJ; i++)
	{
		// ポリゴンの座標セット
		SetVertexPolygon(&tStage.tSelectObject[i]);
		SetTextureSelect(&tStage.tSelectObject[i]);
	}
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawSelectStage(LPDIRECT3DDEVICE9 pDevice)
{
	for(int i = 0; i < MAX_SELECTOBJ; i++)
	{
		// 頂点バッファをデバイスのデータストリームにバインド
		pDevice->SetStreamSource( 0, tStage.tSelectObject[i].pD3DVtxBuff, 0, sizeof(VERTEX_2D) );
		// 頂点フォーマットの設定
		pDevice->SetFVF( FVF_VERTEX_2D );
		// テクスチャの設定
		pDevice->SetTexture( 0, tStage.tSelectObject[i].pD3DTexture );
		// ポリゴンの描画
		pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
	}
}

//-----------------------------------------------------------------------------------
// ステージ切り替え
//-----------------------------------------------------------------------------------
void ChangeStage(void)
{
	if( NumberChange != false ) return;

	if ( GetKeyboardPress(DIK_RIGHT) && NumberMove == MOVE_NONE )
	{
		SetSound(SOUND_PLAY,SE_DECISION);
		tStage.SubStageNum++;
		NumberMove = MOVE_RIGHT;
		NumberChange = true;
		SetAnimationNo(&tStage.tSelectObject[12],ANIM_MOVE_RIGHT);
		SetAnimationNo(&tStage.tSelectObject[13],ANIM_MOVE_RIGHT);
		if ( tStage.SubStageNum > 3 )
		{
			tStage.MainStageNum++;
			tStage.SubStageNum = 1;
			if ( tStage.MainStageNum > 4 )
			{
				tStage.MainStageNum = 1;
				tStage.SubStageNum = 1;
				NumberMove = MOVE_NONE;
				NumberChange = false;
				g_bSetEndPlayerPos = true;
				g_bPlayerAnim = true;
			}
		}
	}
	else if (GetKeyboardPress(DIK_LEFT) && NumberMove == MOVE_NONE)
	{
		SetSound(SOUND_PLAY,SE_DECISION);
		tStage.SubStageNum--;
		NumberMove = MOVE_LEFT;
		NumberChange = true;
		SetAnimationNo(&tStage.tSelectObject[12],ANIM_MOVE_LEFT);
		SetAnimationNo(&tStage.tSelectObject[13],ANIM_MOVE_LEFT);
		if ( tStage.SubStageNum < 1 )
		{
			tStage.MainStageNum--;
			tStage.SubStageNum = 3;
			if ( tStage.MainStageNum < 1 )
			{
				tStage.MainStageNum = 1;
				tStage.SubStageNum = 1;
				NumberChange = false;
				SetAnimationNo(&tStage.tSelectObject[12],ANIM_MOVE_RIGHT);
				SetAnimationNo(&tStage.tSelectObject[13],ANIM_MOVE_RIGHT);
				NumberMove = MOVE_NONE;
			}
		}
	}
}

//-----------------------------------------------------------------------------------
// ナンバーの移動
//-----------------------------------------------------------------------------------
void SelectNumberMove(void)
{
	static int count = 0;
	switch(NumberMove)
	{
	case MOVE_NONE:
		break;

	case MOVE_RIGHT:
		for(int i = 0; i < MAX_SELECTOBJ-2; i++)
		{
			tStage.tSelectObject[i].pos.x -= (float)450/60;
		}
		count++;
		break;

	case MOVE_LEFT:
		for(int i = 0; i < MAX_SELECTOBJ-2; i++)
		{
			tStage.tSelectObject[i].pos.x += (float)450/60;
		}
		count++;
		break;
	}

	if(count >= 60)
	{
		count = 0;
		NumberMove = MOVE_NONE;
		NumberChange = false;
		for(int i = 0; i < MAX_SELECTOBJ-2; i++)
		{
			tStage.tSelectObject[i].nAnimCount = 0;
			tStage.tSelectObject[i].nAnimPattern = 0;
			SetAnimationNo(&tStage.tSelectObject[12],ANIM_MOVE_UP);
			SetAnimationNo(&tStage.tSelectObject[13],ANIM_MOVE_UP);
		}
	}
}

//-----------------------------------------------------------------------------------
// テクスチャ設定
//-----------------------------------------------------------------------------------
void SetTextureSelect(tObjData* obj)
{
	// アニメデータがない場合何もしない
	if( obj->pAnim == NULL ) return;

	SetSelectAnimation( obj );
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	// テクスチャ座標設定
	{
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		// テクスチャ座標の計算
		RECT rect = pAnim[ obj->nAnimPattern ].pos;	// 場所
		float& x = obj->sizeMax.x;	// 大きさを別名に。（長いから）
		float& y = obj->sizeMax.y;

		pVtx[0].tex = D3DXVECTOR2( rect.left / x , rect.top / y );
		pVtx[1].tex = D3DXVECTOR2( rect.right / x, rect.top / y );
		pVtx[2].tex = D3DXVECTOR2( rect.left / x , rect.bottom / y );
		pVtx[3].tex = D3DXVECTOR2( rect.right / x, rect.bottom / y );

		// アンロック
		obj->pD3DVtxBuff->Unlock();
	}
}

//-----------------------------------------------------------------------------------
// テクスチャアニメーション
//-----------------------------------------------------------------------------------
void SetSelectAnimation(tObjData* obj)
{
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	// アニメーション処理
	int flag = pAnim[ obj->nAnimPattern ].flag;

	// アニメーションカウント
	if (NumberMove != MOVE_NONE || g_bPlayerAnim != false)	obj->nAnimCount++;

	// 各アニメーションに割り当てたフレームまで行くと次のアニメーションへ
	if( obj->nAnimCount >= pAnim[ obj->nAnimPattern ].frame )
	{	
		obj->nAnimPattern++;
		obj->nAnimCount = 0;
		// 次の絵は何枚目かを決定する
		if( flag == ANIM_FLAG_STOP )
		{
			obj->bAnim = false;
		} else if( flag == ANIM_FLAG_LOOP )
		{
			obj->nAnimCount = 0;
			obj->nAnimPattern = 0;
		} 
	}
}

//-----------------------------------------------------------------------------------
// プレイヤーの移動
//-----------------------------------------------------------------------------------
void SetPlayerMove(int state)
{
	switch(state)
	{
	case 1:
		if(g_bSetEndPlayerPos != true ) return;

		if(tStage.tSelectObject[12].pos.x <= SCREEN_WIDTH+100.0f)
		{
			g_bPlayerAnim = true;
			if(g_bSetEndPlayerPos && GetFadeOutComplete() != true )
					SetFade(FADE_OUT,60,D3DXCOLOR(0.0f,0.0f,0.0f,1.0f),0.0f);

			for(int i = 12; i < MAX_SELECTOBJ; i++)
			{
				tStage.tSelectObject[i].pos.x += 5.0f;
				SetTextureSelect(&tStage.tSelectObject[i]);
			}
		}
		else
		{
			for(int i = 0; i < MAX_SELECTOBJ; i++)
			{
				if(i <= 11)
					tStage.tSelectObject[i].pos = D3DXVECTOR3((float)SCREEN_CENTER_X + (450*i),(float)SCREEN_CENTER_Y,0.0f);
				else if(i == 12)
				{
					tStage.tSelectObject[i].pos = D3DXVECTOR3((float)-100.0f,(float)SCREEN_CENTER_Y+100.0f,0.0f);
					SetAnimationNo(&tStage.tSelectObject[i],ANIM_MOVE_RIGHT);
				}
				else if(i == 13)
				{
					tStage.tSelectObject[i].pos = D3DXVECTOR3((float)-150.0f,(float)SCREEN_CENTER_Y+100.0f,0.0f);
					SetAnimationNo(&tStage.tSelectObject[i],ANIM_MOVE_RIGHT);
				}
			}

			if(g_bSetEndPlayerPos && GetFadeOutComplete() )
			{
				SetFade(FADE_IN,30,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f),0.0f);
				g_bSetEndPlayerPos = false;
				g_bSetPlayerPos = true;
				g_bPlayerAnim = false;
			}
		}
		break;

	case 2:
		if(g_bSetPlayerPos != true ) return;

		if(tStage.tSelectObject[12].pos.x <= SCREEN_CENTER_X+25.0f)
		{
			g_bPlayerAnim = true;
			for(int i = 12; i < MAX_SELECTOBJ; i++)
			{
				tStage.tSelectObject[i].pos.x += 5.0f;
				SetTextureSelect(&tStage.tSelectObject[i]);
			}
		}
		else
		{
			for(int i = 12; i < MAX_SELECTOBJ; i++)
			{
				SetAnimationNo(&tStage.tSelectObject[i],ANIM_MOVE_UP);
			}
			// // // // // 
			g_bSetPlayerPos = false;
			g_bPlayerAnim = false;
		}
		break;

	
	}
}

#ifdef _DEBUG
//-----------------------------------------------------------------------------------
// ステージ選択情報
//-----------------------------------------------------------------------------------
void DrawSelectStageInfo(void)
{
	RECT rect = { SCREEN_CENTER_X/2, 30, SCREEN_WIDTH, SCREEN_HEIGHT };
	TCHAR str[256];

	wsprintf( str, _T("MainStage : %d \nSubStage : %d"),tStage.MainStageNum,tStage.SubStageNum);

	// テキスト描画
	g_pSelectInfo->DrawText( NULL, str, -1, &rect, DT_LEFT, D3DCOLOR_ARGB( 0xff, 0xff, 0x00, 0x00 ) );
}
#endif