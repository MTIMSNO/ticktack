#include "BlockVine.h"
#include "Player.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define BLOCKVINE_SIZE_X		(STAGE_CELL_WIDTH * 1.5f)
#define BLOCKVINE_SIZE_Y		(STAGE_CELL_HEIGHT * 3.0f)
#define BLOCKVINE_SPEED_ACCEL	(0.001f)
#define BLOCKVINE_SPEED_MAX		(0.06f)

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PreStartBlockVine(void);
void UpdateBlockVine(void);
TCHAR* GetBlockVine_TableFileName(void);

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*		g_Stage_BlockVine[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];
float			g_Stage_BlockVine_speed[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];

//=============================================================================
// 初期化処理
//=============================================================================
void InitBlockVine(void){
	LinkStage_CellArray( g_Stage_BlockVine, STAGE_LAYER_VINE );

	LinkStage_PreStartFunc( PreStartBlockVine, STAGE_LAYER_VINE );

	LinkStage_UpdateFunc( UpdateBlockVine, STAGE_LAYER_VINE );

	LinkStage_TableFunc( GetBlockVine_TableFileName, STAGE_LAYER_VINE );

	for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
		for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
			tObjData&		tmpObj = g_Stage_BlockVine[i][j]->obj;
			tCollisionData&	tmpCol = g_Stage_BlockVine[i][j]->collision;

			// サイズを更新
			tmpObj.size = D3DXVECTOR2( BLOCKVINE_SIZE_X, BLOCKVINE_SIZE_Y);
			CreateCollision(
				&tmpCol,
				tmpObj.pos, COLLISION_TYPE_BOX,
				tmpObj.size.x/4, tmpObj.size.y, 0.0f	// 細いモノなので、とりあえず横の判定は狭くしておく
				);

			// 操作起点を画像中央→つる根元の部分に（回転を違和感なく行う）
			SetPolygonHanddle( &tmpObj, POLYGON_HANDLE_MIDDLETOP );
			SetCollisionHandle( &tmpCol, COLLISION_HANDLE_MIDDLETOP );
		}
	}
}

//=============================================================================
// ステージ開始時の処理
//=============================================================================
void PreStartBlockVine(void){
	for(int i=0; i<STAGE_CELL_ROW_NUM; i++){
		for(int j=0; j<STAGE_CELL_COLUMN_NUM; j++){
			// 揺れる速度の初期化
			g_Stage_BlockVine_speed[i][j] = BLOCKVINE_SPEED_MAX;

			// 角度を初期化
			g_Stage_BlockVine[i][j]->obj.rot.z = 0.0f;
			SetVertexPolygon( &g_Stage_BlockVine[i][j]->obj );
			SetCollisionRotAxisNormalize( &g_Stage_BlockVine[i][j]->collision, g_Stage_BlockVine[i][j]->obj.rot );
		}
	}
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockVine(void){
	int tmpPNum = GetPlayerNumber();
	if( tmpPNum != NAME_TACK ){
		// チックへ切り替わった瞬間
		if( GetPlayerChange() ){
		}

		// タックでなければ処理はしない
		return;
	}

	// タックへ切り替わった瞬間
	if( GetPlayerChange() ){
	}

	RECT rect;

	// 可視範囲のブロック
	GetStageRect_Disp( &rect );
	for(int i=rect.top; i<rect.bottom; i++){
		for(int j=rect.left; j<rect.right; j++){
			if( g_Stage_BlockVine[i][j]->type == 0 ){
				// 何もないマスは飛ばす
				continue;
			}
			float& tmpSpeed = g_Stage_BlockVine_speed[i][j];

			// 現在速度に従って揺らす
			g_Stage_BlockVine[i][j]->obj.rot.z += tmpSpeed;
			SetVertexPolygon( &g_Stage_BlockVine[i][j]->obj );
			SetCollisionRotAxisNormalize( &g_Stage_BlockVine[i][j]->collision, g_Stage_BlockVine[i][j]->obj.rot );

			// 現在の角度に応じて加速させる
			if( g_Stage_BlockVine[i][j]->obj.rot.z >= 0.0f ){
				ADDWITHLIMIT( tmpSpeed, -BLOCKVINE_SPEED_ACCEL, -BLOCKVINE_SPEED_MAX);
			}
			else{
				ADDWITHLIMIT( tmpSpeed, BLOCKVINE_SPEED_ACCEL, BLOCKVINE_SPEED_MAX);
			}
		}
	}
}

//=============================================================================
// 配置データファイル名取得
//=============================================================================
TCHAR* GetBlockVine_TableFileName(void){
	return BLOCKVINE_CELLTABLE_FILENAME;
}

//=============================================================================
// つるとプレイヤーの判定チェック
//=============================================================================
bool CheckHitBlockVine( tObjData** data ){
	RECT	rect;
	const tCollisionData Col_P = GetPlayerCollision( GetPlayerNumber() );
	// つるの当たり判定がセル範囲より大きいので、プレイヤー判定内のマスのみでは不十分
	// 取得範囲を大きめにしておく
	GetStageRect_Disp( &rect );

	for(int i=rect.top; i<rect.bottom; i++){
		for(int j=rect.left; j<rect.right; j++){
			if( g_Stage_BlockVine[i][j]->type == 0 ){
				continue;
			}

			if( CheckCollision( Col_P, g_Stage_BlockVine[i][j]->collision ) ){
				// 当たっているつるを掴まり対象オブジェとして登録する
				*data = &g_Stage_BlockVine[i][j]->obj;
				return true;
			}
		}
	}

	return false;
}

//=============================================================================
// 掴まり処理を始めた位置から見て、一番近い掴まれる位置に設定
//=============================================================================
void AdjustPos_OnHungVine( float* length, D3DXVECTOR3* pos_P, tObjData& target ){
	D3DXVECTOR3
		Vec_ToPlayer = *pos_P - target.pos,
		Vec_OnVine = D3DXVECTOR3( sinf(target.rot.z), cosf(target.rot.z), 0.0f );

	// プレイヤー位置からつる上へ垂線を下ろした、つる上の点までの長さ
	*length = D3DXVec3Dot( &Vec_ToPlayer, &Vec_OnVine ) / D3DXVec3Length( &Vec_OnVine );

	// 算出した長さに応じて位置を設定する
	(*pos_P).x = target.pos.x + *length * sinf(target.rot.z) * target.fScale;
	(*pos_P).y = target.pos.y + *length * cosf(target.rot.z) * target.fScale;
}