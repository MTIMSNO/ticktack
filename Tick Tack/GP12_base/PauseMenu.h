//===================================================================================
//
// ポーズ画面
//
//===================================================================================

#include "main.h"
#include "Sound.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

#define TEXTURE_PAUSEMENU			_T("PauseMenu.png")

#define MAX_PAUSEMENU				(16)

#define MAX_PAUSE_BG_SIZE_X			(900)
#define MAX_PAUSE_BG_SIZE_Y			(700)

#define MAX_PAUSE_FRA_SIZE_X		(100)
#define MAX_PAUSE_FRA_SIZE_Y		(100)

#define MAX_PAUSE_MENU_X			(200)
#define MAX_PAUSE_MENU_Y			(200)

#define MAX_PAUSE_VALVE_X			(60)
#define MAX_PAUSE_VALVE_Y			(60)


#define START_VALVE					(SCREEN_CENTER_X-MAX_PAUSE_VALVE_X*2.5f)

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
HRESULT InitPauseMenu(LPDIRECT3DDEVICE9 pDevice);
void UninitPauseMenu(void);
void UpdatePauseMenu(void);
void DrawPauseMenu(LPDIRECT3DDEVICE9 pDevice);
bool GetPauseFlag(void);
