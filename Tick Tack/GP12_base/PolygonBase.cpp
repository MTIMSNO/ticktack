//=======================================================================================
//
//	2Dポリゴンベース
//
// 作成者：fujimura koichi	作成月：2013.11
//=======================================================================================
#include "PolygonBase.h"
#include "polygonAnim.h"
#include "TextureBase.h"

// 設定されたデータからポリゴン作成
HRESULT MakePolygon( LPDIRECT3DDEVICE9 pDevice, tObjData* obj, TCHAR* tex /*= NULL*/ )
{
	// オブジェクトの頂点バッファを生成
	if( FAILED( pDevice->CreateVertexBuffer( sizeof(VERTEX_2D)*NUM_VERTEX,	// 頂点データ用に確保するバッファサイズ（バイト単位）
												D3DUSAGE_WRITEONLY,			// 頂点バッファの使用法
												FVF_VERTEX_2D,				// 使用する頂点フォーマット
												D3DPOOL_MANAGED,			// リソースのバッファを保持するメモリクラスを指定
												&obj->pD3DVtxBuff,			// 頂点バッファインターフェースへのポインタ
												NULL ) ) )					// NULL固定
	{
		return E_FAIL;
	}

	{	// 頂点バッファの中身を埋める
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );
		
		// 基本値を設定
		obj->fRadius = sqrtf(obj->size.x * obj->size.x + obj->size.y * obj->size.y) / 2.0f;
		obj->fBaseAngle = atan2f(obj->size.x, obj->size.y);

		// 頂点座標の設定
		pVtx[0].vtx.x = obj->pos.x - sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[0].vtx.y = obj->pos.y - cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[0].vtx.z = 0.0f;
		pVtx[1].vtx.x = obj->pos.x + sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[1].vtx.y = obj->pos.y - cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[1].vtx.z = 0.0f;
		pVtx[2].vtx.x = obj->pos.x - sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[2].vtx.y = obj->pos.y + cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[2].vtx.z = 0.0f;
		pVtx[3].vtx.x = obj->pos.x + sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[3].vtx.y = obj->pos.y + cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[3].vtx.z = 0.0f;

		// rhwの設定
		pVtx[0].rhw =
		pVtx[1].rhw =
		pVtx[2].rhw =
		pVtx[3].rhw = 1.0f;

		// 反射光の設定
		pVtx[0].diffuse = 
		pVtx[1].diffuse = 
		pVtx[2].diffuse = 
		pVtx[3].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		// テクスチャ座標の設定
		pVtx[0].tex = D3DXVECTOR2( 0.0f, 0.0f );
		pVtx[1].tex = D3DXVECTOR2( 1.0f, 0.0f );
		pVtx[2].tex = D3DXVECTOR2( 0.0f, 1.0f );
		pVtx[3].tex = D3DXVECTOR2( 1.0f, 1.0f );

		// 頂点データをアンロックする
		obj->pD3DVtxBuff->Unlock();
	}

	if( tex == NULL ){
		obj->pD3DTexture = NULL;
	}
	else
	{
		// 画像ファイルの探索
		obj->pD3DTexture = GetTexturePoint( tex );
		/*// テクスチャの読み込み
		D3DXCreateTextureFromFile( pDevice,		// デバイスのポインタ
			tex,								// ファイルの名前
			&obj->pD3DTexture );				// 読み込むメモリのポインタ*/
	}

	// 初期化
	obj->nAnimCount = 0;
	obj->nAnimPattern = 0;
	obj->bAnim = true;
	obj->bExist = true;
	obj->fAlpha = 1.0f;
	obj->nHandle = POLYGON_HANDLE_CENTER;

	return S_OK;
}

// 頂点データの変更
void SetVertexPolygon( tObjData* obj )
{
	VERTEX_2D* pVtx;

	// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
	obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

	// 頂点座標の設定
	switch( obj->nHandle ){
	case POLYGON_HANDLE_CENTER:
		pVtx[0].vtx.x = obj->pos.x - sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[0].vtx.y = obj->pos.y - cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[0].vtx.z = 0.0f;
		pVtx[1].vtx.x = obj->pos.x + sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[1].vtx.y = obj->pos.y - cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[1].vtx.z = 0.0f;
		pVtx[2].vtx.x = obj->pos.x - sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[2].vtx.y = obj->pos.y + cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[2].vtx.z = 0.0f;
		pVtx[3].vtx.x = obj->pos.x + sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[3].vtx.y = obj->pos.y + cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[3].vtx.z = 0.0f;
		break;
	case POLYGON_HANDLE_MIDDLETOP:
		pVtx[0].vtx.x = obj->pos.x - (obj->size.x / 2.0f) * cosf(obj->rot.z) * obj->fScale;
		pVtx[0].vtx.y = obj->pos.y + (obj->size.x / 2.0f) * sinf(obj->rot.z) * obj->fScale;
		pVtx[0].vtx.z = 0.0f;
		pVtx[1].vtx.x = obj->pos.x + (obj->size.x / 2.0f) * cosf(obj->rot.z) * obj->fScale;
		pVtx[1].vtx.y = obj->pos.y - (obj->size.x / 2.0f) * sinf(obj->rot.z) * obj->fScale;
		pVtx[1].vtx.z = 0.0f;
		pVtx[2].vtx.x = pVtx[0].vtx.x + obj->size.y * sinf(obj->rot.z) * obj->fScale;
		pVtx[2].vtx.y = pVtx[0].vtx.y + obj->size.y * cosf(obj->rot.z) * obj->fScale;
		pVtx[2].vtx.z = 0.0f;
		pVtx[3].vtx.x = pVtx[1].vtx.x + obj->size.y * sinf(obj->rot.z) * obj->fScale;
		pVtx[3].vtx.y = pVtx[1].vtx.y + obj->size.y * cosf(obj->rot.z) * obj->fScale;
		pVtx[3].vtx.z = 0.0f;
		break;
	default:
		break;
	}

	// 表示ずれ対策（これを行っても、テクスチャサイズの問題でずれる可能性はある）
	for( int i = 0; i <= 3; i++ )
	{
		pVtx[i].vtx.x -= 0.5f;
		pVtx[i].vtx.y -= 0.5f;
		D3DXCOLOR temp = pVtx[i].diffuse;
		temp.a = obj->fAlpha;
		pVtx[i].diffuse = temp;
	}

	// 頂点データをアンロックする
	obj->pD3DVtxBuff->Unlock();
}

// 後片付け(名前が変なのは元々UninitPolygonがあったため)
void UninitPolygonAndTexture( tObjData* obj )
{
	/*if( obj->pD3DTexture != NULL )
	{	// テクスチャの開放
		obj->pD3DTexture->Release();
		obj->pD3DTexture = NULL;
	}*/

	if( obj->pD3DVtxBuff != NULL )
	{	// 頂点バッファの開放
		obj->pD3DVtxBuff->Release();
		obj->pD3DVtxBuff = NULL;
	}
}

// テクスチャを変更
//HRESULT ChangeTexture( LPDIRECT3DDEVICE9 pDevice, tObjData* obj, TCHAR* filename )
HRESULT ChangeTexture( tObjData* obj, TCHAR* filename )
{
	//UninitPolygonAndTexture( obj );
	//MakePolygon( pDevice, obj, filename );
	obj->pD3DTexture = GetTexturePoint( filename );

	return S_OK;
}

// テクスチャアニメーション
void SetTextureAnimation( tObjData* obj )
{
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	// アニメーション処理
	int flag = pAnim[ obj->nAnimPattern ].flag;
	obj->nAnimCount++;
	if( obj->nAnimCount >= pAnim[ obj->nAnimPattern ].frame )
	{	// 次の絵は何枚目かを決定する
		if( flag == ANIM_FLAG_STOP )
		{
			obj->bAnim = false;
		} else if( flag == ANIM_FLAG_LOOP )
		{
			obj->nAnimCount = 0;
			obj->nAnimPattern = 0;
		} else {
			obj->nAnimPattern++;
		}
	}
}

// テクスチャ設定
void SetTexturePolygon( tObjData* obj )
{
	// アニメデータがない場合何もしない
	if( obj->pAnim == NULL ) return;

	SetTextureAnimation( obj );
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	// テクスチャ座標設定
	{
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		// テクスチャ座標の計算
		RECT rect = pAnim[ obj->nAnimPattern ].pos;	// 場所
		float& x = obj->sizeMax.x;	// 大きさを別名に。（長いから）
		float& y = obj->sizeMax.y;

		pVtx[0].tex = D3DXVECTOR2( rect.left / x , rect.top / y );
		pVtx[1].tex = D3DXVECTOR2( rect.right / x, rect.top / y );
		pVtx[2].tex = D3DXVECTOR2( rect.left / x , rect.bottom / y );
		pVtx[3].tex = D3DXVECTOR2( rect.right / x, rect.bottom / y );

		// アンロック
		obj->pD3DVtxBuff->Unlock();
	}

}

// アニメナンバーを変更
void SetAnimationNo( tObjData* obj, int no )
{
	obj->nAnimNo = no;
	obj->nAnimPattern = 0;
	obj->nAnimCount = 0;
	obj->bAnim = true;
}

// アニメーション中かどうか
bool IsPolygonAnimation( tObjData* obj, int no /*=-1*/ )
{
	// どのアニメでもいいけど、アニメしているかどうかを判定
	if( no == -1 ) return obj->bAnim;

	// 指定のアニメナンバーでアニメしているかどうか
	if( obj->bAnim && obj->nAnimNo == no )
	{
		return true;
	}
	return false;
}

//=============================================================================
// テクスチャアニメーション進行
//=============================================================================
void Process_PolygonTexAnimation( tObjData* obj )
{
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	if( pAnim == NULL ){
		return;
	}

	// アニメーション処理
	int flag = pAnim[ obj->nAnimPattern ].flag;

	// アニメーションカウント
	obj->nAnimCount++;

	// 各アニメーションに割り当てたフレームまで行くと次のアニメーションへ
	if( obj->nAnimCount >= pAnim[ obj->nAnimPattern ].frame )
	{	
		obj->nAnimPattern++;
		obj->nAnimCount = 0;
		// 次の絵は何枚目かを決定する
		if( flag == ANIM_FLAG_STOP )
		{
			obj->bAnim = false;
		} else if( flag == ANIM_FLAG_LOOP )
		{
			obj->nAnimCount = 0;
			obj->nAnimPattern = 0;
		} 
	}

	SetPolygonTextureRect( *obj, pAnim[obj->nAnimPattern].pos );
}

//=============================================================================
//　表示するテクスチャー範囲設定
//=============================================================================
void SetPolygonTextureRect( tObjData& obj, RECT rect ){
	if( obj.pD3DTexture == NULL ){
		// テクスチャが未設定の場合は中断
		return;
	}

	// テクスチャの縦横幅を取得
	// ※２のべき乗制約が掛かるので、テクスチャサイズもそれに合わせて作ると安全
	D3DSURFACE_DESC	desc;
	obj.pD3DTexture->GetLevelDesc( 0, &desc );
	float x = (float)desc.Width;
	float y = (float)desc.Height;

	// 頂点へテクスチャ座標の反映
	{
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj.pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		pVtx[0].tex = D3DXVECTOR2( rect.left  / x, rect.top / y );
		pVtx[1].tex = D3DXVECTOR2( rect.right / x, rect.top / y );
		pVtx[2].tex = D3DXVECTOR2( rect.left  / x, rect.bottom / y );
		pVtx[3].tex = D3DXVECTOR2( rect.right / x, rect.bottom / y );

		// 取得するテクスチャ座標の補正
		{
			float tmp_x = 0.5f / x, tmp_y = 0.5f / y;
			for(int i=0; i<4; i++){
				pVtx[i].tex.x += tmp_x;
				pVtx[i].tex.y += tmp_y;
			}
		}

		// 頂点データをアンロックする
		obj.pD3DVtxBuff->Unlock();
	}
}

//=============================================================================
//　各頂点の色設定
//=============================================================================
void SetPolygonDiffuse( tObjData* obj, D3DXCOLOR& color ){
	VERTEX_2D* pVtx;

	// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
	obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );
		
	// 反射光の初期化
	pVtx[0].diffuse =
	pVtx[1].diffuse =
	pVtx[2].diffuse =
	pVtx[3].diffuse = color;

	// 頂点データをアンロックする
	obj->pD3DVtxBuff->Unlock();
}

//=============================================================================
//　操作起点設定
//=============================================================================
void SetPolygonHanddle( tObjData* obj, int type ){
	obj->nHandle = type;
	// ポリゴンの位置を、更新後の操作起点に合わせて配置しなおす
	SetVertexPolygon( obj );
}

void SetTextureScroll( tObjData* obj , float speed,int state)
{
	// アニメデータがない場合何もしない
	if( obj->pAnim == NULL ) return;

	SetTextureAnimation( obj );
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	static float Ltemp;
	static float Rtemp;
	// テクスチャ座標設定
	{
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		// テクスチャ座標の計算
		RECT rect = pAnim[ obj->nAnimPattern ].pos;	// 場所
		float& x = obj->sizeMax.x;	// 大きさを別名に。（長いから）
		float& y = obj->sizeMax.y;

		switch(state)
		{
			// 左移動
		case 0:
			Ltemp += speed;
			pVtx[0].tex = D3DXVECTOR2( rect.left / x + Ltemp + 0.0f , rect.top / y );
			pVtx[1].tex = D3DXVECTOR2( rect.right / x + Ltemp + 1.0f, rect.top / y );
			pVtx[2].tex = D3DXVECTOR2( rect.left / x + Ltemp + 0.0f, rect.bottom / y );
			pVtx[3].tex = D3DXVECTOR2( rect.right / x + Ltemp + 1.0f, rect.bottom / y );
			break;

			// 右移動
		case 1:
			Rtemp += speed;
			pVtx[0].tex = D3DXVECTOR2( rect.left / x + Rtemp + 0.0f , rect.top / y );
			pVtx[1].tex = D3DXVECTOR2( rect.right / x + Rtemp + 1.0f, rect.top / y );
			pVtx[2].tex = D3DXVECTOR2( rect.left / x + Rtemp + 0.0f, rect.bottom / y );
			pVtx[3].tex = D3DXVECTOR2( rect.right / x + Rtemp + 1.0f, rect.bottom / y );
			break;
		}
		// アンロック
		obj->pD3DVtxBuff->Unlock();
	}
}


//--------------------------------------------
// ビルボード用
//--------------------------------------------


// 設定されたデータからポリゴン作成
HRESULT MakePolygon3D( LPDIRECT3DDEVICE9 pDevice, tObjData* obj, TCHAR* tex /*= NULL*/ )
{
	// オブジェクトの頂点バッファを生成
	if( FAILED( pDevice->CreateVertexBuffer( sizeof(VERTEX_3D)*NUM_VERTEX,	// 頂点データ用に確保するバッファサイズ（バイト単位）
												D3DUSAGE_WRITEONLY,			// 頂点バッファの使用法
												FVF_VERTEX_3D,				// 使用する頂点フォーマット
												D3DPOOL_MANAGED,			// リソースのバッファを保持するメモリクラスを指定
												&obj->pD3DVtxBuff,			// 頂点バッファインターフェースへのポインタ
												NULL ) ) )					// NULL固定
	{
		return E_FAIL;
	}

	{	// 頂点バッファの中身を埋める
		VERTEX_3D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );
		
		// 基本値を設定
		obj->fRadius = sqrtf(obj->size.x * obj->size.x + obj->size.y * obj->size.y) / 2.0f;
		obj->fBaseAngle = atan2f(obj->size.x, obj->size.y);

		// 頂点座標の設定
		pVtx[0].vtx.z = -obj->size.x/2.0f;
		pVtx[0].vtx.y = obj->size.y/2.0f;
		pVtx[0].vtx.x = 0.0f;
		pVtx[1].vtx.z = obj->size.x/2.0f;
		pVtx[1].vtx.y = obj->size.y/2.0f;
		pVtx[1].vtx.x = 0.0f;
		pVtx[2].vtx.z = -obj->size.x/2.0f;
		pVtx[2].vtx.y = -obj->size.y/2.0f;
		pVtx[2].vtx.x = 0.0f;
		pVtx[3].vtx.z = obj->size.x/2.0f;
		pVtx[3].vtx.y = -obj->size.y/2.0f;
		pVtx[3].vtx.x = 0.0f;

		// rhwの設定
		pVtx[0].normal =
		pVtx[1].normal =
		pVtx[2].normal =
		pVtx[3].normal = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

		// 反射光の設定
		pVtx[0].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[1].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[2].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[3].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		// テクスチャ座標の設定
		pVtx[0].tex = D3DXVECTOR2( 0.0f, 0.0f );
		pVtx[1].tex = D3DXVECTOR2( 1.0f, 0.0f );
		pVtx[2].tex = D3DXVECTOR2( 0.0f, 1.0f );
		pVtx[3].tex = D3DXVECTOR2( 1.0f, 1.0f );

		// 頂点データをアンロックする
		obj->pD3DVtxBuff->Unlock();
	}

	if( tex == NULL ){
		obj->pD3DTexture = NULL;
	}
	else
	{
		obj->pD3DTexture = GetTexturePoint( tex );
		/*// テクスチャの読み込み
		D3DXCreateTextureFromFile( pDevice,		// デバイスのポインタ
			tex,								// ファイルの名前
			&obj->pD3DTexture );				// 読み込むメモリのポインタ*/
	}

	// 初期化
	obj->nAnimCount = 0;
	obj->nAnimPattern = 0;
	obj->bAnim = true;

	return S_OK;
}

// 頂点データの変更
void SetVertexPolygon3D( tObjData* obj )
{
	VERTEX_3D* pVtx;

	// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
	obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

	// 頂点データをアンロックする
	obj->pD3DVtxBuff->Unlock();
}

// テクスチャ設定
void SetTexturePolygon3D( tObjData* obj )
{
	SetTextureAnimation( obj );
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	// テクスチャ座標設定
	{
		VERTEX_3D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		// テクスチャ座標の計算
		RECT rect = pAnim[ obj->nAnimPattern ].pos;	// 場所
		float& x = obj->sizeMax.x;	// 大きさを別名に。（長いから）
		float& y = obj->sizeMax.y;

		pVtx[0].tex = D3DXVECTOR2( rect.left / x , rect.top / y );
		pVtx[1].tex = D3DXVECTOR2( rect.right / x, rect.top / y );
		pVtx[2].tex = D3DXVECTOR2( rect.left / x , rect.bottom / y );
		pVtx[3].tex = D3DXVECTOR2( rect.right / x, rect.bottom / y );

		// アンロック
		obj->pD3DVtxBuff->Unlock();
	}

}


void SetTextureScroll3D( tObjData* obj , float speed)
{
	SetTextureAnimation( obj );
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	static float temp;
	// テクスチャ座標設定
	{
		VERTEX_3D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		// テクスチャ座標の計算
		RECT rect = pAnim[ obj->nAnimPattern ].pos;	// 場所
		float& x = obj->sizeMax.x;	// 大きさを別名に。（長いから）
		float& y = obj->sizeMax.y;

		temp += speed;

		pVtx[0].tex = D3DXVECTOR2( rect.left / x + temp  , rect.top / y );
		pVtx[1].tex = D3DXVECTOR2( rect.right / x + temp , rect.top / y );
		pVtx[2].tex = D3DXVECTOR2( rect.left / x + temp , rect.bottom / y );
		pVtx[3].tex = D3DXVECTOR2( rect.right / x + temp , rect.bottom / y );

		// アンロック
		obj->pD3DVtxBuff->Unlock();
	}
}
