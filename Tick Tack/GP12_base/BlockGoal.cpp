#include "BlockGoal.h"
#include "BlockCheckPoint.h"
#include "Player.h"
#include "Fade.h"
#include "Data.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
void PreStartBlockGoal(void);
void UpdateBlockGoal(void);
TCHAR* GetBlockGoal_TableFileName(void);
void CheckHitGoal(const tCollisionData& DATA);

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************
tStageCell*		g_Stage_BlockGoal[STAGE_CELL_ROW_NUM][STAGE_CELL_COLUMN_NUM];
bool			g_bGoalFadeFlag;
bool			g_bHitGoal;

//=============================================================================
// 初期化処理
//=============================================================================
void InitBlockGoal(void){
	LinkStage_CellArray( g_Stage_BlockGoal, STAGE_LAYER_GOAL );

	LinkStage_PreStartFunc( PreStartBlockGoal, STAGE_LAYER_GOAL );

	LinkStage_UpdateFunc( UpdateBlockGoal, STAGE_LAYER_GOAL );

	LinkStage_TableFunc( GetBlockGoal_TableFileName, STAGE_LAYER_GOAL );

	g_bGoalFadeFlag = g_bHitGoal = false;
}

//=============================================================================
// ステージ開始時の処理
//=============================================================================
void PreStartBlockGoal(void){
	// ステージ開始時のキャラクター位置を設定する
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBlockGoal(void){
	// ゴールとのあたり判定
	if ( !(g_bHitGoal) && !(g_bGoalFadeFlag) )
	CheckHitGoal(GetPlayerCollision(GetPlayerNumber()));
}

//=============================================================================
// 配置データファイル名取得
//=============================================================================
TCHAR* GetBlockGoal_TableFileName(void){
	return BLOCKGOAL_CELLTABLE_FILENAME;
}

//=============================================================================
// あたり判定
//=============================================================================
void CheckHitGoal(const tCollisionData& DATA)
{
	for ( int i = 0; i < STAGE_CELL_ROW_NUM; i++ )
	{
		for ( int j = 0; j < STAGE_CELL_COLUMN_NUM; j++ )
		{
			if ( g_Stage_BlockGoal[i][j]->type != GOAL_FLAG )	continue;

			if ( CheckCollision(DATA,g_Stage_BlockGoal[i][j]->collision ) && GetManipulatedValve() == GetMaxValve() )
			{
				g_bGoalFadeFlag = true;
				g_bHitGoal = true;
			}
		}
	}

}

bool GetGoalFlag(){

	return g_bHitGoal;
}

void ReInitGoal(void)
{
	g_bGoalFadeFlag = false;
	g_bHitGoal = false;
}