#include "CollisionBase.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
//#define _COLLISION_3D_

#define POS_ABH(data)	GetCollisionPos_AdjustByHandle(data)

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
bool CheckCollision_BC( const tCollisionData& BOX, const tCollisionData& CIRCLE );
void CalcSpanSq_OBBtoPoint( float* data, const tCollisionData& BOX, const D3DXVECTOR3& pos );
bool CheckCollision_BB( const tCollisionData& BOX1, const tCollisionData& BOX2 );
float CalcCollisionLength_OnSeparateAxis( const D3DXVECTOR3* SepAxis, const D3DXVECTOR3* vec1, const D3DXVECTOR3* vec2, const D3DXVECTOR3* vec3 );

D3DXVECTOR3 GetCollisionPos_AdjustByHandle( const tCollisionData& data );

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************


//=============================================================================
//当たり判定チェック
//=============================================================================
bool CheckCollision( const tCollisionData& DATA1, const tCollisionData& DATA2 ){
	// 円-円判定
	if( DATA1.type == COLLISION_TYPE_CIRCLE && DATA2.type == COLLISION_TYPE_CIRCLE ){
		//D3DXVECTOR2 vec_span = *(DATA1.pos) - *(DATA2.pos);
		D3DXVECTOR2 vec_span = POS_ABH(DATA1) - POS_ABH(DATA2);
		float SpanSq = D3DXVec2LengthSq( &vec_span ), RadiusSq = powf( ( DATA1.radius + DATA2.radius ), 2 );

		if( SpanSq < RadiusSq ){
			return true;
		}
	}
	// 箱-円判定
	else if( DATA1.type == COLLISION_TYPE_BOX && DATA2.type == COLLISION_TYPE_CIRCLE ){
		return CheckCollision_BC( DATA1, DATA2 );
	}
	else if( DATA1.type == COLLISION_TYPE_CIRCLE && DATA2.type == COLLISION_TYPE_BOX ){
		return CheckCollision_BC( DATA2, DATA1 );
	}
	// 箱-箱判定
	else if( DATA1.type == COLLISION_TYPE_BOX && DATA2.type == COLLISION_TYPE_BOX ){
		return CheckCollision_BB( DATA1, DATA2 );
	}

	return false;
}

//=============================================================================
// 箱-円　判定
//=============================================================================
bool CheckCollision_BC( const tCollisionData& BOX, const tCollisionData& CIRCLE ){
	float spanSq, radiusSq = powf( CIRCLE.radius, 2 );
	//CalcSpanSq_OBBtoPoint( &spanSq, BOX, *CIRCLE.pos );
	CalcSpanSq_OBBtoPoint( &spanSq, BOX, POS_ABH(CIRCLE) );

	return ( spanSq < radiusSq )? true: false;
}
//=============================================================================
// OBBから点への距離取得(二乗値)
//=============================================================================
void CalcSpanSq_OBBtoPoint( float* data, const tCollisionData& BOX, const D3DXVECTOR3& pos ){
	float tmpNum, length;
	//D3DXVECTOR3 Vec_toPoint = pos - *BOX.pos, spanVec(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 Vec_toPoint = pos - POS_ABH(BOX), spanVec(0.0f, 0.0f, 0.0f);

	// OBB各軸ベクトルに対して、境界線から対象点までの距離ベクトルを加算
	for(int i=0; i<3; i++){
		if( ( length = BOX.length[i] ) <= 0 ){
			continue;
		}

		// チェック中のOBB軸線分上から、対象点が外れていたら加算する
		//tmpNum = fabs( D3DXVec3Dot( &tmpVec, &BOX.axis[i] ) / length );
		tmpNum = fabs( GetPosNum_OBBtoPoint( Vec_toPoint, BOX.axis[i], length ) );
		if( tmpNum > 1 ){
			spanVec += ( 1 - tmpNum ) * length * BOX.axis[i];
		}
	}

	// 加算した三軸ベクトルの長さを反映
	*data = D3DXVec3LengthSq( &spanVec );
}
//=============================================================================
// チェック中のOBB軸線分上から、対象点の相対位置を示す係数取得
//=============================================================================
float GetPosNum_OBBtoPoint( const D3DXVECTOR3& Vec_toPoint, const D3DXVECTOR3& axis, const float length ){
	return D3DXVec3Dot( &Vec_toPoint, &axis ) / length;
}

//=============================================================================
// 箱-箱　判定
//=============================================================================
bool CheckCollision_BB( const tCollisionData& BOX1, const tCollisionData& BOX2 ){
	// OBB-OBBの判定
	const D3DXVECTOR3& A0 = BOX1.axis[0]; const D3DXVECTOR3& LA0 = A0 * BOX1.length.x;
	const D3DXVECTOR3& A1 = BOX1.axis[1]; const D3DXVECTOR3& LA1 = A1 * BOX1.length.y;
	const D3DXVECTOR3& A2 = BOX1.axis[2]; const D3DXVECTOR3& LA2 = A2 * BOX1.length.z;
	const D3DXVECTOR3& B0 = BOX2.axis[0]; const D3DXVECTOR3& LB0 = B0 * BOX2.length.x;
	const D3DXVECTOR3& B1 = BOX2.axis[1]; const D3DXVECTOR3& LB1 = B1 * BOX2.length.y;
	const D3DXVECTOR3& B2 = BOX2.axis[2]; const D3DXVECTOR3& LB2 = B2 * BOX2.length.z;
	//D3DXVECTOR3 Vec_AtoB = *BOX1.pos - *BOX2.pos;
	D3DXVECTOR3 Vec_AtoB = POS_ABH(BOX1) - POS_ABH(BOX2);
	float span, length;

	// OBB1の分離軸三本について判定を行う
	// A0
	span = fabs( D3DXVec3Dot( &A0, &Vec_AtoB ) );
	length = BOX1.length.x + CalcCollisionLength_OnSeparateAxis( &A0, &LB0, &LB1, &LB2 );
	if( length < span ){
		return false;
	}
	// A1
	span = fabs( D3DXVec3Dot( &A1, &Vec_AtoB ) );
	length = BOX1.length.y + CalcCollisionLength_OnSeparateAxis( &A1, &LB0, &LB1, &LB2 );
	if( length < span ){
		return false;
	}
#ifdef _COLLISION_3D_
	// A2
	span = fabs( D3DXVec3Dot( &A2, &Vec_AtoB ) );
	length = BOX1.length.z + CalcCollisionLength_OnSeparateAxis( &A2, &LB0, &LB1, &LB2 );
	if( length < span ){
		return false;
	}
#endif

	// OBB2の分離軸三本について判定を行う
	// B0
	span = fabs( D3DXVec3Dot( &B0, &Vec_AtoB ) );
	length = BOX2.length.x + CalcCollisionLength_OnSeparateAxis( &B0, &LA0, &LA1, &LA2 );
	if( length < span ){
		return false;
	}
	// B1
	span = fabs( D3DXVec3Dot( &B1, &Vec_AtoB ) );
	length = BOX2.length.y + CalcCollisionLength_OnSeparateAxis( &B1, &LA0, &LA1, &LA2 );
	if( length < span ){
		return false;
	}
#ifdef _COLLISION_3D_
	// B2
	span = fabs( D3DXVec3Dot( &B2, &Vec_AtoB ) );
	length = BOX2.length.z + CalcCollisionLength_OnSeparateAxis( &B2, &LA0, &LA1, &LA2 );
	if( length < span ){
		return false;
	}

	// OBB1及びOBB2それぞれの軸三本と垂直な分離軸について判定を行う
	// 組み合わせが「3 * 3」で計９本
	D3DXVECTOR3 Cross;
	// C00
	D3DXVec3Cross( &Cross, &A0, &B0 );
	span = fabs( D3DXVec3Dot( &Cross, &Vec_AtoB ) );
	length = CalcCollisionLength_OnSeparateAxis( &Cross, &LA1, &LA2 ) + CalcCollisionLength_OnSeparateAxis( &Cross, &LB1, &LB2 );
	if( length < span ){
		return false;
	}
	
	// C01
	D3DXVec3Cross( &Cross, &A0, &B1 );
	span = fabs( D3DXVec3Dot( &Cross, &Vec_AtoB ) );
	length = CalcCollisionLength_OnSeparateAxis( &Cross, &LA1, &LA2 ) + CalcCollisionLength_OnSeparateAxis( &Cross, &LB0, &LB2 );
	if( length < span ){
		return false;
	}
	
	// C02
	D3DXVec3Cross( &Cross, &A0, &B2 );
	span = fabs( D3DXVec3Dot( &Cross, &Vec_AtoB ) );
	length = CalcCollisionLength_OnSeparateAxis( &Cross, &LA1, &LA2 ) + CalcCollisionLength_OnSeparateAxis( &Cross, &LB0, &LB1 );
	if( length < span ){
		return false;
	}
	
	// C10
	D3DXVec3Cross( &Cross, &A1, &B0 );
	span = fabs( D3DXVec3Dot( &Cross, &Vec_AtoB ) );
	length = CalcCollisionLength_OnSeparateAxis( &Cross, &LA0, &LA2 ) + CalcCollisionLength_OnSeparateAxis( &Cross, &LB1, &LB2 );
	if( length < span ){
		return false;
	}
	
	// C11
	D3DXVec3Cross( &Cross, &A1, &B1 );
	span = fabs( D3DXVec3Dot( &Cross, &Vec_AtoB ) );
	length = CalcCollisionLength_OnSeparateAxis( &Cross, &LA0, &LA2 ) + CalcCollisionLength_OnSeparateAxis( &Cross, &LB0, &LB2 );
	if( length < span ){
		return false;
	}
	
	// C12
	D3DXVec3Cross( &Cross, &A1, &B2 );
	span = fabs( D3DXVec3Dot( &Cross, &Vec_AtoB ) );
	length = CalcCollisionLength_OnSeparateAxis( &Cross, &LA0, &LA2 ) + CalcCollisionLength_OnSeparateAxis( &Cross, &LB0, &LB1 );
	if( length < span ){
		return false;
	}
	
	// C20
	D3DXVec3Cross( &Cross, &A2, &B0 );
	span = fabs( D3DXVec3Dot( &Cross, &Vec_AtoB ) );
	length = CalcCollisionLength_OnSeparateAxis( &Cross, &LA0, &LA1 ) + CalcCollisionLength_OnSeparateAxis( &Cross, &LB1, &LB2 );
	if( length < span ){
		return false;
	}
	
	// C21
	D3DXVec3Cross( &Cross, &A2, &B1 );
	span = fabs( D3DXVec3Dot( &Cross, &Vec_AtoB ) );
	length = CalcCollisionLength_OnSeparateAxis( &Cross, &LA0, &LA1 ) + CalcCollisionLength_OnSeparateAxis( &Cross, &LB0, &LB2 );
	if( length < span ){
		return false;
	}
	
	// C22
	D3DXVec3Cross( &Cross, &A2, &B2 );
	span = fabs( D3DXVec3Dot( &Cross, &Vec_AtoB ) );
	length = CalcCollisionLength_OnSeparateAxis( &Cross, &LA0, &LA1 ) + CalcCollisionLength_OnSeparateAxis( &Cross, &LB0, &LB1 );
	if( length < span ){
		return false;
	}
#endif

	return true;
}

//=============================================================================
//　分離軸への投影成分長の算出
//=============================================================================
float CalcCollisionLength_OnSeparateAxis( const D3DXVECTOR3* SepAxis, const D3DXVECTOR3* vec1, const D3DXVECTOR3* vec2, const D3DXVECTOR3* vec3 ){
	return fabs( D3DXVec3Dot( vec1, SepAxis ) )
		+ fabs( D3DXVec3Dot( vec2, SepAxis ) )
		+ ( vec3? fabs( D3DXVec3Dot( vec3, SepAxis ) ):0 );
}

//=============================================================================
// 当たり判定作成
//=============================================================================
void CreateCollision( tCollisionData* data, const D3DXVECTOR3& pos, int type, float data1, float data2, float data3 ){
	data->pos = ( &pos == NULL )? NULL: &pos;
	data->handle = COLLISION_HANDLE_CENTER;
	data->axis[0] = D3DXVECTOR3( 1.0f, 0.0f, 0.0f );
	data->axis[1] = D3DXVECTOR3( 0.0f, 1.0f, 0.0f );
	data->axis[2] = D3DXVECTOR3( 0.0f, 0.0f, 1.0f );

	switch( data->type = type ){
	case COLLISION_TYPE_CIRCLE:
		// 円判定の場合は先頭データを半径として反映する
		// ※後ろ二つのデータは無視
		data->radius = data1;
		data->length.x = data1;
		data->length.y = data1;
		data->length.z = data1;
		break;
	case COLLISION_TYPE_BOX:
		// 箱判定の場合は、各データを三軸成分として反映
		// ※箱を覆う最小球の半径を算出して保持する
		data->length.x = data1;
		data->length.y = data2;
		data->length.z = data3;
		data->radius = D3DXVec3Length( &data->length );
		break;
	default:
		break;
	}
}

//=============================================================================
//　OBB各軸ベクトルの回転反映と正規化
//=============================================================================
void SetCollisionRotAxisNormalize( tCollisionData* data, const D3DXVECTOR3& rot ){
	// 回転後のXYZ軸ベクトルを取得する
	// ※行列成分直だと煩雑になりすぎるので、ヘルパー関数経由
	D3DXMATRIXA16	matRot;
	D3DXMatrixRotationYawPitchRoll( &matRot, rot.y, rot.x, rot.z );

	// 回転後の各軸ベクトル設定
	D3DXVec3Normalize( &data->axis[0], &D3DXVECTOR3( matRot._11, matRot._12, matRot._13 ) );
	D3DXVec3Normalize( &data->axis[1], &D3DXVECTOR3( matRot._21, matRot._22, matRot._23 ) );
	D3DXVec3Normalize( &data->axis[2], &D3DXVECTOR3( matRot._31, matRot._32, matRot._33 ) );
}

//=============================================================================
//　操作起点設定
//=============================================================================
void SetCollisionHandle( tCollisionData* data, int type ){
	data->handle = type;
}

//=============================================================================
//　操作起点に応じて補正した中心位置を取得
//=============================================================================
D3DXVECTOR3 GetCollisionPos_AdjustByHandle( const tCollisionData& data ){
	D3DXVECTOR3	tmpPos = *data.pos;

	switch( data.handle ){
	case COLLISION_HANDLE_MIDDLETOP:
		// 2Dと3DでY軸方向が違う為、2Dの場合はY軸成分補正を逆方向にする
		tmpPos.x -= data.length.y * data.axis[1].x;
		tmpPos.y += data.length.y * data.axis[1].y;
		break;
	default:
		break;
	}

	return tmpPos;
}